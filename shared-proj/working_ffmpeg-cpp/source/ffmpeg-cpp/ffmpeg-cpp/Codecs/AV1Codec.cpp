// Adi: added

#include "AV1Codec.h"

namespace ffmpegcpp
{

	AV1Codec::AV1Codec()
		: VideoCodec("AV1")
	{

	}

	void AV1Codec::SetCrf(int crf)
	{
		SetOption("crf", crf);
	}
}