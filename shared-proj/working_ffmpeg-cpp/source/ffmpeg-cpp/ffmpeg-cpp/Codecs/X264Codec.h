// Adi: added

#pragma once
#include "VideoCodec.h"

namespace ffmpegcpp
{

	class X264Codec : public VideoCodec
	{

	public:

		X264Codec();

		void SetPreset(const char* preset);
	};


}