// Adi: added

#include "X264Codec.h"

namespace ffmpegcpp
{

	X264Codec::X264Codec()
		: VideoCodec("libx264")
	{

	}

	void X264Codec::SetPreset(const char* preset)
	{
		SetOption("preset", preset);
	}
}