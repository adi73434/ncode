
#pragma once

#include "stdafx.h"
#include <sciter/sciter-x.h>
#include <sciter/sciter-x-behavior.h>
#include <sciter/sciter-x-threads.h>
#include <sciter/sciter-x-video-api.h>

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
}

#include <string>

void SaveFrame(AVFrame *pFrame, int width, int height, int iFrame)
{
	FILE *pFile;
	char szFilename[32];
	int y;

	// Open file
	sprintf(szFilename, "frame%d.ppm", iFrame);
	pFile = fopen(szFilename, "wb");
	if (pFile == NULL)
		return;

	// Write header
	fprintf(pFile, "P6\n%d %d\n255\n", width, height);

	// Write pixel data
	for (y = 0; y < height; y++)
		fwrite(pFrame->data[0] + y * pFrame->linesize[0], 1, width * 3, pFile);

	// Close file
	fclose(pFile);
}

/*
	BEHAVIOR: video_generated_stream
		- provides synthetic video frames.
		- this code is here solely for the demo purposes - how
			to connect your own video frame stream with the rendering site

	COMMENTS:
		 <video style="behavior:video-generator video" />
	SAMPLE:
		 See: samples/video/video-generator-behavior.htm
	*/

struct custom_video_stream : public sciter::event_handler
{
	sciter::om::hasset<sciter::video_destination> rendering_site;
	// ctor
	custom_video_stream() {}
	virtual ~custom_video_stream() {}

	virtual bool subscription(HELEMENT he, UINT &event_groups)
	{
		event_groups = HANDLE_BEHAVIOR_EVENT; // we only handle VIDEO_BIND_RQ here
		return true;
	}

	virtual void attached(HELEMENT he)
	{
		he = he;
	}

	virtual void detached(HELEMENT he) { asset_release(); }
	virtual bool on_event(HELEMENT he, HELEMENT target, BEHAVIOR_EVENTS type, UINT_PTR reason)
	{
		if (type != VIDEO_BIND_RQ)
			return false;
		// we handle only VIDEO_BIND_RQ requests here

		//printf("VIDEO_BIND_RQ %d\n",reason);

		if (!reason)
			return true; // first phase, consume the event to mark as we will provide frames

		rendering_site = (sciter::video_destination *)reason;
		sciter::om::hasset<sciter::fragmented_video_destination> fsite;

		if (rendering_site->asset_get_interface(FRAGMENTED_VIDEO_DESTINATION_INAME, fsite.target()))
		{
			sciter::thread(generation_thread, fsite);
		}

		return true;
	}

	static void generation_thread(sciter::fragmented_video_destination *dst)
	{
		// Create sciter rendering site thing
		sciter::om::hasset<sciter::fragmented_video_destination> rendering_site = dst;

		// -----------------------------------------------------------------
		// Loading video and trying to feed it into Sciter's rendering site
		// ? means idfk what it does/how it does the thing it does
		// -----------------------------------------------------------------
		AVFormatContext *pFormatCtx = NULL;
		int i;
		int videoStream;
		AVCodecContext *pCodecCtxOrig = NULL;
		AVCodecContext *pCodecCtx = NULL;
		AVCodec *pCodec = NULL;
		AVFrame *pFrame = NULL;
		AVFrame *pFrameRGB = NULL;
		AVPacket packet;
		int frameFinished;
		float aspect_ratio;
		struct SwsContext *sws_ctx = NULL;

		int numBytes;
		uint8_t *buffer = NULL;

		std::string fileDir = "testVideo.mp4";

		// Register all formats and codecs; makes all codecs/muxers etc. supported
		// Can call them individually if want to
		av_register_all();

		// Open video
		if (avformat_open_input(&pFormatCtx, fileDir.c_str(), NULL, NULL) != 0)
		{
			// error here; couldn't open file
			return;
		}

		// Retrieve stream information
		if (avformat_find_stream_info(pFormatCtx, NULL) < 0)
		{
			// error here; couldn't find stream info
			return;
		}

		// ? Find the first video stream
		videoStream = -1;
		for (i = 0; i < pFormatCtx->nb_streams; i++)
			if (pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
			{
				videoStream = i;
				break;
			}
		if (videoStream == -1)
		{
			throw std::runtime_error("Didn't find supported stream");
			return; // Didn't find a video stream
		}

		// Get a pointer to the codec context for the video stream
		pCodecCtxOrig = pFormatCtx->streams[videoStream]->codec;
		// Find the decoder for the video stream
		pCodec = avcodec_find_decoder(pCodecCtxOrig->codec_id);
		if (pCodec == NULL)
		{
			fprintf(stderr, "Unsupported codec\n");
			// error here; unsupported codec
			return;
		}

		// Allocate codec context with matching codec
		pCodecCtx = avcodec_alloc_context3(pCodec);
		// Copy AVCodecContext from pCodecCtxOrig to pCodecCtx
		// Supposedly this is deprecated; see documentation with F12
		// TODO: Fix deprecated
		if (avcodec_copy_context(pCodecCtx, pCodecCtxOrig) != 0)
		{
			fprintf(stderr, "Couldn't copy codec context");
			// error here; couldn't copy codec context
			return;
		}

		// Open codec with provided context and codec
		if (avcodec_open2(pCodecCtx, pCodec, NULL) < 0)
		{
			// error here
			return;
		}

		// Allocate video frame.
		// ? I think this holds the frame data
		pFrame = av_frame_alloc();
		pFrameRGB = av_frame_alloc();
		if (pFrameRGB == NULL)
			throw std::runtime_error("aaaaaaaaaaa");

		// ? Initialize SWS context for software scaling
		// Convert to RGB24 format
		sws_ctx = sws_getContext(pCodecCtx->width,
								 pCodecCtx->height,
								 pCodecCtx->pix_fmt,
								 pCodecCtx->width,
								 pCodecCtx->height,
								 AV_PIX_FMT_RGB24,
								 SWS_BILINEAR,
								 NULL,
								 NULL,
								 NULL);

		// -----------------------------------------------------------------
		// Set up pFrameRGB
		// -----------------------------------------------------------------

		// Determine required buffer size and allocate buffer
		numBytes = avpicture_get_size(AV_PIX_FMT_RGB24, pCodecCtx->width,
									  pCodecCtx->height);
		buffer = (uint8_t *)av_malloc(numBytes * sizeof(uint8_t));

		// Assign appropriate parts of buffer to image planes in pFrameRGB
		// Note that pFrameRGB is an AVFrame, but AVFrame is a superset
		// of AVPicture
		avpicture_fill((AVPicture *)pFrameRGB, buffer, AV_PIX_FMT_RGB24,
					   pCodecCtx->width, pCodecCtx->height);

		// -----------------------------------------------------------------
		// SECTION Rendering
		// Read frames and save first five frames to disk
		// Packet passed as reference; it'll store frame data
		// -----------------------------------------------------------------
		rendering_site->start_streaming(pCodecCtx->width, pCodecCtx->height, sciter::COLOR_SPACE_RGB24);
		i = 0;
		while (av_read_frame(pFormatCtx, &packet) >= 0)
		{
			// ? Check if this is a packet from the video stream
			if (packet.stream_index == videoStream)
			{
				// Decode video frame
				// avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);
				avcodec_send_packet(pCodecCtx, &packet);

				// Check a video frame was read; should be non-zero if good
				if (avcodec_receive_frame(pCodecCtx, pFrame))
				{

					// AVPicture pict;
					// avpicture_alloc(&pict, AV_PIX_FMT_RGB24, pFrame->height, pFrame->height);

					// sws_scale(sws_ctx, (uint8_t const *const *)pFrame->data,
					// 		  pFrame->linesize, 0, pCodecCtx->height, pict.data, pict.linesize);

					sws_scale(sws_ctx, (uint8_t const *const *)pFrame->data,
							  pFrame->linesize, 0, pCodecCtx->height, pFrameRGB->data, pFrameRGB->linesize);

					// sws_scale(sws_ctx, (uint8_t const *const *)pFrame->data,
					// 		  pFrame->linesize, 0, pCodecCtx->height, pFrame->data, pFrame->linesize);

					// const BYTE *wtf = reinterpret_cast<BYTE*>(pFrame->data);

					// rendering_site->render_frame((const BYTE*)pFrame->data, pFrame->pkt_size);
					// Render the frame to Sciter's rendering site
					rendering_site->render_frame((const BYTE *)pFrameRGB->data, pFrameRGB->pkt_size);
					// if (++i <= 5)
					SaveFrame(pFrameRGB, pCodecCtx->width, pCodecCtx->height, i);
				}
			}
		}

		// Free the packet that was allocated by av_read_frame
		av_free_packet(&packet);
		// av_packet_unref(&packet);

		// -----------------------------------------------------------------
		// !SECTION
		// -----------------------------------------------------------------

		// Free the YUV frame
		av_frame_free(&pFrame);

		// Close the codec
		avcodec_close(pCodecCtx);
		avcodec_close(pCodecCtxOrig);

		// Close the video file
		avformat_close_input(&pFormatCtx);

		// return;

		// unsigned int figure[FRAGMENT_WIDTH*FRAGMENT_HEIGHT];// = {0xFFFF00FF};

		// auto generate_fill_color = [&]() {
		//   unsigned color =
		//     0xff000000 |
		//     ((unsigned(rand()) & 0xff) << 16) |
		//     ((unsigned(rand()) & 0xff) << 8) |
		//     ((unsigned(rand()) & 0xff) << 0);
		//   for (int i = 0; i < FRAGMENT_WIDTH * FRAGMENT_HEIGHT; ++i)
		//     figure[i] = color;
		// };

		// generate_fill_color();

		// int xpos = 0;
		// int ypos = 0;
		// int stepx = +1;
		// int stepy = +1;

		// while (rendering_site->is_alive())
		// {
		//   sciter::sync::sleep(40); // simulate 24 FPS rate

		//   xpos += stepx;
		//   if (xpos < 0) { xpos = 0; stepx = -stepx; generate_fill_color(); }
		//   if (xpos >= VIDEO_WIDTH - FRAGMENT_WIDTH) { xpos = VIDEO_WIDTH - FRAGMENT_WIDTH; stepx = -stepx; generate_fill_color(); }

		//   ypos += stepy;
		//   if (ypos < 0) { ypos = 0; stepy = -stepy; generate_fill_color(); }
		//   if (ypos >= VIDEO_HEIGHT - FRAGMENT_HEIGHT) { ypos = VIDEO_HEIGHT - FRAGMENT_HEIGHT; stepy = -stepy; generate_fill_color(); }

		//   rendering_site->render_frame_part((const unsigned char*)figure, sizeof(figure), xpos, ypos, FRAGMENT_WIDTH, FRAGMENT_HEIGHT);
		// }
	}
};

struct custom_video_stream_factory : public sciter::behavior_factory
{

	custom_video_stream_factory() : sciter::behavior_factory("custom-video-generator")
	{
	}

	// the only behavior_factory method:
	virtual sciter::event_handler *create(HELEMENT he)
	{
		return new custom_video_stream();
	}
};

// instantiating and attaching it to the global list
custom_video_stream_factory custom_video_stream_factory_instance;
