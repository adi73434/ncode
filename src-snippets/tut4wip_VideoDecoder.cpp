

#include "tut4wip_VideoDecoder.h"

sciter::om::hasset<sciter::video_destination> sciter_rendering_site;

VideoDecoder::VideoState *global_video_state;

void *frame_refresh_dataptr;

// This mutex/condition/bool provide a way for the video renderer
// to wait until the next frame should display
std::atomic<bool> frame_refresh_do;
std::mutex frame_refresh_mtx;
std::condition_variable_any frame_refresh_cond;

// This mutex/cond ensure packet_queue_get/packet_queue_put don't
// have race conditions
std::mutex packetqueue_mutex;
std::condition_variable_any packetqueue_cond;

//
std::mutex videostate_mutex;
std::condition_variable_any videostate_cond;

void VideoDecoder::packet_queue_init(PacketQueue *q)
{
	memset(q, 0, sizeof(PacketQueue));
	// q->mutex = SDL_CreateMutex();
	// q->cond = SDL_CreateCond();
	// std::unique_lock<std::mutex> mtx_lock(q->mutex_packetsomething);
}
/**
 * @brief 
 * 
 * @param param_pq Pointer to a PacketQueue
 * @param param_pkt 
 * @return int 
 */
int VideoDecoder::packet_queue_put(PacketQueue *param_pq, AVPacket *param_pkt)
{
	AVPacketList *pkt_list;
	if (av_dup_packet(param_pkt) < 0)
	{
		_WINDEBUGOUT("ERROR8\n")
	}

	pkt_list = (AVPacketList *)av_malloc(sizeof(AVPacketList));
	if (!pkt_list)
	{
		_WINDEBUGOUT("ERROR9\n");
	}
	pkt_list->pkt = *param_pkt;
	pkt_list->next = NULL;

	// SDL_LockMutex(q->mutex);
	// q->mutex_something.lock();
	std::unique_lock<std::mutex> mtx_lock(packetqueue_mutex);

	if (!param_pq->last_pkt)
	{
		param_pq->first_pkt = pkt_list;
	}
	else
	{
		param_pq->last_pkt->next = pkt_list;
	}
	param_pq->last_pkt = pkt_list;
	param_pq->nb_packets++;
	_WINDEBUGOUT("Put packet size: " << pkt_list->pkt.size << std::endl);
	param_pq->size += pkt_list->pkt.size;
	// SQL_CondSignal(q->cond)
	mtx_lock.unlock();

	// SDL_UnlockMutex(q->mutex);
	packetqueue_cond.notify_all();
	return 0;
}

int VideoDecoder::packet_queue_get(PacketQueue *param_pq, AVPacket *param_pkt, int block)
{
	AVPacketList *pkt_list;
	int ret;

	// SDL_LockMutex(q->mutex);
	// q->mutex_something.lock();

	std::unique_lock<std::mutex> mtx_lock(packetqueue_mutex);

	for (;;)
	{
		if (global_video_state->misc_quit)
		{
			ret = -1;
			break;
		}

		pkt_list = param_pq->first_pkt;
		if (pkt_list && param_pq->nb_packets > 0)
		{
			param_pq->last_pkt = NULL;
			param_pq->nb_packets--;
			_WINDEBUGOUT("Get packet size: " << pkt_list->pkt.size << std::endl);
			param_pq->size -= pkt_list->pkt.size;
			*param_pkt = pkt_list->pkt;
			av_free(pkt_list);
			ret = 1;
			break;
		}
		else if (!block)
		{
			ret = 0;
			break;
		}
		else
		{
			// SDL_CondWait(q->cond_packetsomething, q->mutex)
			packetqueue_cond.wait(mtx_lock);
		}
	}
	// SDL_UnlockMutex(q->mutex);
	//mutex_packetsomething.unlock();
	//cond_packetsomething.notify_all();
	return ret;
}

uint32_t VideoDecoder::refresh_timer_cb(void *opaque)
{
	std::unique_lock<std::mutex> mtx_lock(frame_refresh_mtx);
	frame_refresh_do = true;
	frame_refresh_dataptr = opaque;
	frame_refresh_cond.notify_all();
	return 0; /* 0 means stop timer */
}

void VideoDecoder::schedule_refresh(VideoState *vs, int delay)
{
	// FIXME: Accurate delay
	Sleep(delay);
	refresh_timer_cb(vs);
	// SDL_AddTimer(delay, sdl_refresh_timer_cb, is);
}

void VideoDecoder::video_display(VideoState *vs)
{
	// SDL_Rect rect;
	VideoPicture *vp;
	float aspect_ratio;
	int w, h, x, y;
	int i;

	vp = &vs->v_vp[vs->v_vp_rindex];

	// Convert whatever the source video was to a AV_PIX_FMT_RGB24 frame

	// Render the frame to Sciter's rendering site and update last rendered time to play back at correct fps
	sciter_rendering_site->render_frame((const BYTE *)vp->frame_data.data[0], frame_buffer_size);

	// vp = &vs->v_vp[vs->v_vp_rindex];
	// if (vp->bmp)
	// {
	// 	if (vs->v_ctx_codec->sample_aspect_ratio.num == 0)
	// 	{
	// 		aspect_ratio = 0;
	// 	}
	// 	else
	// 	{
	// 		aspect_ratio = av_q2d(vs->v_ctx_codec->sample_aspect_ratio) * vs->v_ctx_codec->width / vs->v_ctx_codec->height;
	// 	}

	// 	if (aspect_ratio <= 0.0)
	// 	{
	// 		aspect_ratio = (float)vs->v_ctx_codec->width / (float)vs->v_ctx_codec->height;
	// 		h = screen->h;
	// 		w = ((int)rint(h * aspect_ratio)) & -3;
	// 		if (w > screen->w)
	// 		{
	// 			w = screen->w;
	// 			h = ((int)rint(w / aspect_ratio)) & -3;
	// 		}
	// 		x = (screen->w - w) / 2;
	// 		y = (screen->h - h) / 2;

	// 		rect.x = x;
	// 		rect.y = y;
	// 		rect.w = w;
	// 		rect.h = h;

	// 		// SDL_LockMutex(screen_mutex);
	// 		// SDL_DisplayYUVOverlay(vp->bpm, &rect);
	// 		// SDL_UnlockMutex(screen_mutex);

	// 		screen_mutex.lock();
	// 		// ???? render to sciter ????
	// 		screen_mutex.unlock();
	// 	}
	// }
}

void VideoDecoder::video_refresh_timer(void *userdata)
{
	VideoState *vs = (VideoState *)userdata;
	VideoPicture *vp;

	if (vs->v_stream)
	{
		if (vs->v_vp_size == 0)
		{
			schedule_refresh(vs, 1);
		}
		else
		{
			vp = &vs->v_vp[vs->v_vp_rindex];
			// Now, normally here goes a ton of code
			// about timing, etc. we're just going to
			// guess at a delay for now. You can
			// increase and decrease this value and hard code
			// the timing - but I don't suggest that ;)
			// We'll learn how to do it for real later.
			schedule_refresh(vs, 40);

			// show the picture
			video_display(vs);

			// update queue for next picture
			if (++vs->v_vp_rindex == VIDEO_PICTURE_QUEUE_SIZE)
			{
				vs->v_vp_rindex = 0;
			}

			// SDL_LockMutex(vs->mutex_something);
			std::unique_lock<std::mutex> mtx_lock(videostate_mutex);
			vs->v_vp_size--;
			// SDL_CondSignal(vs->mutex_conditionorsomething);
			mtx_lock.unlock();
			// SDL_UnlockMutex(vs->mutex_something);
			videostate_cond.notify_all();
		}
	}
	else
	{
		schedule_refresh(vs, 100);
	}
}

// void VideoDecoder::alloc_picture(void *userdata)
// {
// 	VideoState *vs = (VideoState *)userdata;
// 	// VideoPicture *vp;
// 	AVFrame *v_frame;
// 	AVFrame *v_frame_rgb;

// 	// vp = &vs->v_vp[vs->v_vp_rindex];
// 	// if (vp->bpm)
// 	// {
// 	// 	// we already have one make another, bigger/smaller
// 	// 	SDL_FreeYUVOverlay(vp->bmp);
// 	// }

// 	// // Allocate a place to put our YUV image on that screen
// 	// // SDL_LockMutex(screen_mutex);
// 	// // screen_mutex.lock();
// 	// std::unique_lock<std::mutex> mtx_lock(screen_mutex);

// 	// vp->bmp = SDL_CreateYUVOverlay(vs->v_ctx_codec->width,
// 	// 							   vs->v_ctx_codec->height,
// 	// 							   SDL_YV12_OVERLAY,
// 	// 							   screen);

// 	v_frame = av_frame_alloc();
// 	v_frame_rgb = av_frame_alloc();

// 	// SDL_UnlockMutex(screen_mutex);
// 	// mtx_lock.unlock();

// 	vp->width = vs->v_ctx_codec->width;
// 	vp->height = vs->v_ctx_codec->height;
// 	vp->allocated = 1;
// }

int VideoDecoder::queue_picture(VideoState *param_vs, AVFrame *param_frame)
{
	VideoPicture *vp;
	AVFrame *v_frame_rgb;
	int dst_pix_fmt;
	uint8_t *v_frame_rgb_buffer = NULL;

	// wait until we have space for a new pic
	// SDL_LockMutex(vs->mutex_something);
	std::unique_lock<std::mutex> mtx_lock(videostate_mutex);
	while (param_vs->v_vp_size >= VIDEO_PICTURE_QUEUE_SIZE && !param_vs->misc_quit)
	{
		_WINDEBUGOUT("queue picture waiting\n");
		// SDL_CondWait(vs->mutexconditionorsomething, vs->mutex_something);
		// vs->cond_something.wait(mtx_lock, [&] { return; });
		videostate_cond.wait(mtx_lock);
	}
	// SDL_UnlockMutex(vs->mutex_something);
	mtx_lock.unlock();

	if (param_vs->misc_quit)
		return -1;

	// windex is set to 0 initially
	vp = &param_vs->v_vp[param_vs->v_vp_windex];

	// Allocate or resize the buffer
	// if (!vp->bmp ||
	// 	vp->width != vs->v_ctx_codec->width ||
	// 	vp->height != vs->v_ctx_codec->height)
	// {
	// 	// SDL_Event event

	// 	vp->allocated = 0;
	// 	alloc_picture(vs);
	// 	if (vs->misc_quit)
	// 	{
	// 		return -1;
	// 	}
	// }

	v_frame_rgb = av_frame_alloc();

	// Assign appropriate parts of buffer to image planes in v_frame_rgb
	// Note that v_frame_rgb is an AVFrame, but AVFrame is a superset
	// of AVPicture
	avpicture_fill((AVPicture *)v_frame_rgb, v_frame_rgb_buffer, AV_PIX_FMT_RGB24,
				   param_vs->v_ctx_codec->width, param_vs->v_ctx_codec->height);

	// We have a place to put our picture on the queue

	// if (vp->bmp)
	// {
	// SDL_LockYUVOverlay(vp->bmp);

	// dst_pix_fmt = AV_PIX_FMT_YUV420P;

	// pict.data[0] = vp->bmp->pixels[0];
	// pict.data[1] = vp->bmp->pixels[2];
	// pict.data[2] = vp->bmp->pixels[1];

	// pict.linesize[0] = vp->bmp->pitches[0];
	// pict.linesize[1] = vp->bmp->pitches[2];
	// pict.linesize[2] = vp->bmp->pitches[1];

	// Convert the image into YUV format that SDL uses
	// sws_scale(vs->sws_ctx, (uint8_t const *const *)qp_frame->data,
	// 		  qp_frame->linesize, 0, vs->v_ctx_codec->height,
	// 		  pict.data, pict.linesize);
	sws_scale(param_vs->sws_ctx, (uint8_t const *const *)param_frame->data,
			  param_frame->linesize, 0, param_vs->v_ctx_codec->height, v_frame_rgb->data, v_frame_rgb->linesize);

	// SDL_UnlockYUVOverlay(vp->bmp);

	// Now we inform our display thread that we have a pic ready
	if (++param_vs->v_vp_rindex == VIDEO_PICTURE_QUEUE_SIZE)
	{
		param_vs->v_vp_windex = 0;
	}
	// SDL_LockMutex(vs->mutex_something);
	// std::unique_lock<std::mutex> mtx_lock(vs->mutex_something);
	mtx_lock.lock();
	param_vs->v_vp_size++;
	// SDL_UnlockMutex(vs->mutex_something);
	mtx_lock.unlock();
	// }
	return 0;
}

int VideoDecoder::video_thread(void *param)
{
	VideoState *vs = (VideoState *)param;
	AVPacket vt_pkt1;
	AVPacket *vt_packet = &vt_pkt1;
	int vt_frame_finished;
	AVFrame *vt_frame;

	vt_frame = av_frame_alloc();

	for (;;)
	{
		if (packet_queue_get(&vs->v_pq, vt_packet, 1) < 0)
		{
			// stopped getting packets
			break;
		}

		// Decode video frame
		avcodec_decode_video2(vs->v_ctx_codec, vt_frame, &vt_frame_finished, vt_packet);

		// Did we get a video frame?
		if (vt_frame_finished)
		{
			// Determine required buffer size and allocate buffer
			if (queue_picture(vs, vt_frame) < 0)
			{
				 break;
			}
		}

		av_free_packet(vt_packet);
	}

	av_frame_free(&vt_frame);
	return 0;
}

// FIXME: Not all control paths return a value
/**
 * @brief Finds the decoder, allocates the context of the codec within the function,
 * and copies the context from the stream in param_vs->g_ctx_format to the local
 * context defined in the function.
 * 
 * It then opens the codec using the copied context.
 * 
 * Finally, if the stream is a video:
 * it copies the stream index for the video to the v_stream_index;
 * it copies the stream to v_stream;
 * it copies the context used to open the video to v_ctx_codec;
 * it initialises the packet queue;
 * it spawns a video_thread on thread_video_tid, passing it param_vs as an argument;
 * it detaches thread_video_tid;
 * it allocates an SwsContext using the v_ctx_codec parameters, and RGB24, on sws_ctx;
 * it calculates the frame_buffer_size according to RGB24 and the v_ctx_codec->height/width;
 * and finally it starts the sciter_rendering_site stream, passing it the height/width and RGB24 args.
 * 
 * If the stream is audio, it does nothing currently as that is not a requirement for this project.
 * 
 * @param vs pointer to the VideoState
 * @param stream_index Integer which is passed to target the correct stream
 * from the potential list of audio/video streams in the file.
 * @return int 
 */
int VideoDecoder::stream_component_open(VideoState *param_vs, int param_stream_index)
{
	AVFormatContext *sco_ctx_format = param_vs->g_ctx_format;
	AVCodecContext *sco_ctx_codec;
	AVCodec *sco_codec;

	if (param_stream_index < 0 || param_stream_index >= sco_ctx_format->nb_streams)
	{
		_WINDEBUGOUT("ERROR1\n");
	}

	sco_codec = avcodec_find_decoder(sco_ctx_format->streams[param_stream_index]->codec->codec_id);
	if (!sco_codec)
	{
		_WINDEBUGOUT("ERROR2 Unsupported codec\n");
		return -1;
	}

	sco_ctx_codec = avcodec_alloc_context3(sco_codec);
	if (avcodec_copy_context(sco_ctx_codec, sco_ctx_format->streams[param_stream_index]->codec) != 0)
	{
		_WINDEBUGOUT("ERROR3 Couldn't copy codec context\n");
	}

	if (sco_ctx_codec->codec_type == AVMEDIA_TYPE_AUDIO)
	{
				// NOTE: Not doing audio
	}

	if (avcodec_open2(sco_ctx_codec, sco_codec, NULL) < 0)
	{
		_WINDEBUGOUT("ERROR4 Unsupported codec\n");
	}

	switch (sco_ctx_codec->codec_type)
	{
	case AVMEDIA_TYPE_AUDIO:
		// NOTE: Not doing audio
		break;
	case AVMEDIA_TYPE_VIDEO:
		param_vs->v_stream_index = param_stream_index;
		param_vs->v_stream = sco_ctx_format->streams[param_stream_index];
		param_vs->v_ctx_codec = sco_ctx_codec;

		packet_queue_init(&param_vs->v_pq);

		param_vs->thread_video_tid = std::thread(&VideoDecoder::video_thread, this, param_vs);
		param_vs->thread_video_tid.detach();

		param_vs->sws_ctx = sws_getContext(param_vs->v_ctx_codec->width,
										   param_vs->v_ctx_codec->height,
										   param_vs->v_ctx_codec->pix_fmt,
										   param_vs->v_ctx_codec->width,
										   param_vs->v_ctx_codec->height,
										   AV_PIX_FMT_RGB24,
										   SWS_BILINEAR, NULL, NULL, NULL);

		frame_buffer_size = av_image_get_buffer_size(AV_PIX_FMT_RGB24, param_vs->v_ctx_codec->width, param_vs->v_ctx_codec->height, 1);
		sciter_rendering_site->start_streaming(param_vs->v_ctx_codec->width, param_vs->v_ctx_codec->height, sciter::COLOR_SPACE_RGB24);
		break;
	default:
		break;
	}
}

/**
 * @brief This thread opens the video, finds the codec and stream info,
 * and notable triggers stream_component_open.
 * 
 * It then loops, reading a frame and passing the VideoState's PacketQueue
 * to packet_queue_put so that the decoded packet can be queued.
 * 
 * @param param 
 */
void VideoDecoder::decode_thread(void *param)
{
	VideoState *vs = (VideoState *)param;
	AVFormatContext *dt_ctx_format = NULL;
	AVPacket dt_pkt1;
	AVPacket *dt_packet = &dt_pkt1;

	int video_index = -1;
	int audio_index = -1;
	int i;

	vs->v_stream_index = -1;

	global_video_state = vs;

	std::string in_file_str = Conversions::sciter_value_to_std_string(vs->misc_filename);
	std::string file_dir = "d.mp4";

	if (avformat_open_input(&dt_ctx_format, file_dir.c_str(), NULL, NULL) != 0)
	{
		_WINDEBUGOUT("Error5 Couldn't open file\n");
		return;
	}

	vs->g_ctx_format = dt_ctx_format;

	// Retrieve stream information
	if (avformat_find_stream_info(dt_ctx_format, NULL) < 0)
	{
		_WINDEBUGOUT("Error6 Couldn't find stream information\n");
		return;
	}

	// Dump information about file onto standard error
	// av_dump_format(dt_ctx_format, 0, vs->misc_filename, 0);

	// Find the first video and audio stream
	// These are done so that the correct stream can be targeted hereafter
	for (i = 0; i < dt_ctx_format->nb_streams; i++)
	{
		if (dt_ctx_format->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO && video_index < 0)
		{
			video_index = i;
		}
		// -----------------------------------------------------------------
		// NOTE: Audio
		// if (dt_ctx_format->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO && audio_index < 0)
		// {
		// 	audio_index = i;
		// }
		// -----------------------------------------------------------------
	}

	// Open the respective steams
	if (video_index >= 0)
	{
		stream_component_open(vs, video_index);
	}

	// ---------------------------------------------------------------------
	// NOTE: Audio
	// if (audio_index >= 0 )
	// {
	// 	stream_component_open(vs, audio_index);
	// }
	// ---------------------------------------------------------------------

	// ---------------------------------------------------------------------
	// NOTE: Audio
	// if (vs->v_stream_counter < 0 || vs->vs_astream_int < 0)
	// ---------------------------------------------------------------------
	if (vs->v_stream_index < 0)
	{
		_WINDEBUGOUT("ERROR7 Couldn't open codecs\n", vs->misc_filename);
		// goto fail;
	}

	// ---------------------------------------------------------------------
	// Main decode loop
	// ---------------------------------------------------------------------
	for (;;)
	{
		if (vs->misc_quit)
		{
			break;
		}

		// -----------------------------------------------------------------
		// NOTE: Audio
		// if (vs->a_pq.size > MAX_AUDIOQ_SIZE || vs->v_pq.size > MAX_VIDEOQ_SIZE )
		// -----------------------------------------------------------------
		// Seek stuff goes here
		if (vs->v_pq.size > MAX_VIDEOQ_SIZE)
		{
			// SDL_Delay(10);
			Sleep(10);
			// continue;
		}

		// if (vs->v_pq.nb_packets > MAX_VIDEOQ_NUM_PACKETS)
		if (vs->v_pq.nb_packets > 5)
		{
			// SDL_Delay(10);
			Sleep(10);
			// continue;
		}

		if (av_read_frame(vs->g_ctx_format, dt_packet) < 0)
		{
			if (vs->g_ctx_format->pb->error == 0)
			{
				// SDL_Delay(100); /* no error; wait for user input */
				Sleep(100);
				// continue;
			}
			else
			{
				break;
			}
		}

		// Check packet is from video stream
		if (dt_packet->stream_index == vs->v_stream_index)
		{
			packet_queue_put(&vs->v_pq, dt_packet);
		}
		// NOTE: Not doing audio
		// else if (dt_packet->stream_index == vs->a_stream_counter)
		// {
		// 	packet_queue_put(&vs->a_pq, dt_packet);
		// }
		else
		{
			av_free_packet(dt_packet);
		}
	}

	// All done - wait for it
	while (!vs->misc_quit)
	{
		Sleep(100);
		// SDL_Delay(100);
	}

	// fail:
	// if (1)
	// {
	// 	SDL_Event event;
	// 	event.type = FF_QUIT_EVENT;
	// 	event.user.data1 = is;
	// 	SDL_PushEvent(&event);
	// }
	return;
}

/**
 * @brief This function is the parent of the VideoState passed around
 * throughout the other functions.
 * 
 * This VideoState stores the PacketQueue used for specifying a packet
 * and the next packet, recursively, using AVPacktList.
 * 
 * It also spawns a separate decode_thread, passing it this context and the
 * VideoState.
 * 
 * Lastly, this thread waits on frame_refresh_cond to receive a signal
 * to trigger the next video frame refresh.
 * 
 * @param fsite This is a sciter video destination parameter used for the sciter
 * video rendering site.
 */
void VideoDecoder::initialise_video(sciter::om::hasset<sciter::video_destination> fsite)
{

	// NOTE: Discrepancy: Single pointer in tutorial
	VideoState *vs = NULL;

	vs = (VideoState *)av_mallocz(sizeof(VideoState));

	sciter_rendering_site = fsite;

	// Register all formats and codecs; makes all codecs/muxers etc. supported
	// Can call them individually if want to
	av_register_all();

	// Copy the params.filenameFromUi to misc_filename
	// av_strlcpy(vs->misc_filename,
	// 		   Conversions::sciter_value_to_const_char_pointer(params.video_filename),
	// 		   sizeof(vs->misc_filename));

	// vs->misc_filename = params.video_filename;
	vs->misc_filename = "b.mp4";

	schedule_refresh(vs, 40);

	// Create thread for decoding
	vs->thread_parse_tid = std::thread(&VideoDecoder::decode_thread, this, vs);

	// if(!vs->thread_parse_tid)
	// {
	// av_free(vs);
	// return;
	// }

	for (;;)
	{
		std::unique_lock<std::mutex> mtx_lock(frame_refresh_mtx);
		frame_refresh_cond.wait(mtx_lock);
		if (frame_refresh_do)
		{
			video_refresh_timer(frame_refresh_dataptr);
		}
	}

	// for (;;)
	// {
	// 	SDL_WaitEvent(&event);
	// 	switch (event.type)
	// 	{
	// 	case FF_QUIT_EVENT:
	// 	case SDL_QUIT:
	// 		vs->misc_quit = 1;
	// 		SDL_Quit;
	// 		return;
	// 		break;
	// 	case FF_REFRESH_EVENT:
	// 		video_refresh_timer(event.user.data1);
	// 		break;
	// 	default:
	// 		break;
	// 	}
	// }
}
