
#pragma once

#include "Globals.h"
#include "stdafx.h"

#include <Windows.h>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <sciter/sciter-x.h>
#include <sciter/sciter-x-behavior.h>
#include <sciter/sciter-x-threads.h>
#include <sciter/sciter-x-video-api.h>

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
}

#include "Conversions.h"

#define DBOUT(s)                               \
	{                                          \
		std::wostringstream os_;               \
		os_ << s;                              \
		OutputDebugStringW(os_.str().c_str()); \
	}

/*
	BEHAVIOR: video_generated_stream
		- provides synthetic video frames.
		- this code is here solely for the demo purposes - how
			to connect your own video frame stream with the rendering site

	COMMENTS:
		 <video style="behavior:video-generator video" />
	SAMPLE:
		 See: samples/video/video-generator-behavior.htm
	*/

struct custom_video_stream : public sciter::event_handler
{

	static void SaveFrame(AVFrame *v_frame, int width, int height, int iFrame)
	{
		FILE *pFile;
		char szFilename[32];
		int y;

		sprintf(szFilename, "frame%d.ppm", iFrame);
		pFile = fopen(szFilename, "wb");
		if (pFile == NULL)
			return;

		fprintf(pFile, "P6\n%d %d\n255\n", width, height);

		for (y = 0; y < height; y++)
			fwrite(v_frame->data[0] + y * v_frame->linesize[0], 1, width * 3, pFile);

		fclose(pFile);
	}

	sciter::om::hasset<sciter::video_destination> rendering_site;
	custom_video_stream() {}
	virtual ~custom_video_stream() {}

	virtual bool subscription(HELEMENT he, UINT &event_groups)
	{
		return true;
	}

	virtual void attached(HELEMENT he)
	{
		he = he;
	}

	virtual void detached(HELEMENT he) { asset_release(); }
	virtual bool on_event(HELEMENT he, HELEMENT target, BEHAVIOR_EVENTS type, UINT_PTR reason)
	{
		if (type != VIDEO_BIND_RQ)
			return false;

		if (!reason)

			rendering_site = (sciter::video_destination *)reason;
		sciter::om::hasset<sciter::fragmented_video_destination> fsite;

		if (rendering_site->asset_get_interface(FRAGMENTED_VIDEO_DESTINATION_INAME, fsite.target()))
		{
			sciter::thread(generation_thread, fsite);
		}

		return true;
	}

	static void generation_thread(sciter::fragmented_video_destination *dst)
	{
		sciter::om::hasset<sciter::fragmented_video_destination> rendering_site = dst;
		AVFormatContext *v_ctx_format = NULL;
		AVCodecContext *v_ctx_codec_orig = NULL;
		AVCodecContext *v_ctx_codec = NULL;
		AVCodec *v_codec = NULL;
		AVFrame *v_frame = NULL;
		AVFrame *v_frame_rgb = NULL;
		uint8_t *v_frame_rgb_buffer = NULL;
		AVPacket v_pkt;
		int frame_finished;
		struct SwsContext *sws_ctx = NULL;
		int i;
		int video_stream;
		bool first_frame = true;
		double frame_rate;
		double frame_duration;
		std::chrono::milliseconds last_rendered_time;
		std::string file_dir = "a.mp4";

		av_register_all();

		if (avformat_open_input(&v_ctx_format, file_dir.c_str(), NULL, NULL) != 0)
		{
			return;
		}

		if (avformat_find_stream_info(v_ctx_format, NULL) < 0)
		{
			return;
		}

		video_stream = -1;
		for (i = 0; i < v_ctx_format->nb_streams; i++)
			if (v_ctx_format->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
			{
				video_stream = i;
				frame_rate = (double)v_ctx_format->streams[i]->r_frame_rate.num / (double)v_ctx_format->streams[i]->r_frame_rate.den;
				frame_duration = 1000.0 / frame_rate;
				break;
			}
		if (video_stream == -1)
		{
			throw std::runtime_error("Didn't find supported stream");
		}

		v_ctx_codec_orig = v_ctx_format->streams[video_stream]->codec;
		v_codec = avcodec_find_decoder(v_ctx_codec_orig->codec_id);
		if (v_codec == NULL)
		{
			fprintf(stderr, "Unsupported codec\n");
			return;
		}

		v_ctx_codec = avcodec_alloc_context3(v_codec);
		if (avcodec_copy_context(v_ctx_codec, v_ctx_codec_orig) != 0)
		{
			fprintf(stderr, "Couldn't copy codec context");
			return;
		}

		if (avcodec_open2(v_ctx_codec, v_codec, NULL) < 0)
		{
			return;
		}

		sws_ctx = sws_getContext(v_ctx_codec->width,
								 v_ctx_codec->height,
								 v_ctx_codec->pix_fmt,
								 v_ctx_codec->width,
								 v_ctx_codec->height,
								 AV_PIX_FMT_RGB24,
								 SWS_BILINEAR,
								 NULL,
								 NULL,
								 NULL);

		int frame_buffer_size = av_image_get_buffer_size(AV_PIX_FMT_RGB24, v_ctx_codec->width, v_ctx_codec->height, 1);

		v_frame = av_frame_alloc();
		v_frame_rgb = av_frame_alloc();
		if (v_frame == NULL || v_frame_rgb == NULL)
			throw std::runtime_error("v_frame is null; could not allocate");

		v_frame_rgb_buffer = (uint8_t *)av_malloc(frame_buffer_size * sizeof(uint8_t));

		avpicture_fill((AVPicture *)v_frame_rgb, v_frame_rgb_buffer, AV_PIX_FMT_RGB24,
					   v_ctx_codec->width, v_ctx_codec->height);

		std::vector<BYTE> frame_buffer_queue;
		rendering_site->start_streaming(v_ctx_codec->width, v_ctx_codec->height, sciter::COLOR_SPACE_RGB24);
		i = 0;
		while (av_read_frame(v_ctx_format, &v_pkt) >= 0)
		{
			if (v_pkt.stream_index == video_stream)
			{

				int avcodec_ret = avcodec_send_packet(v_ctx_codec, &v_pkt);

				if (avcodec_ret < 0 || avcodec_ret == AVERROR(EAGAIN) || avcodec_ret == AVERROR_EOF)
				{
					std::cout << "avcodec_send_packet: " << avcodec_ret << std::endl;
					break;
				}

				while (avcodec_ret >= 0)
				{
					avcodec_ret = avcodec_receive_frame(v_ctx_codec, v_frame);
					if (avcodec_ret == AVERROR(EAGAIN) || avcodec_ret == AVERROR_EOF)
					{
						break;
					}
					if (!first_frame)
					{
						std::chrono::milliseconds current_epoch = Conversions::get_ms_since_epoch();
						int actual_delay = current_epoch.count() - last_rendered_time.count();
						DBOUT("\n The value of current_epoch is" << current_epoch.count() << "\n");
						DBOUT("\n The value of last_rendered_time is" << last_rendered_time.count() << "\n");
						DBOUT("\n The value of actual_delay is" << actual_delay << "\n");
						DBOUT("\n The value of frame_duration is" << frame_duration << "\n\n\n\n");

						Sleep(actual_delay);
					}
					else
					{
						first_frame = false;
					}
					sws_scale(sws_ctx, (uint8_t const *const *)v_frame->data,
							  v_frame->linesize, 0, v_ctx_codec->height, v_frame_rgb->data, v_frame_rgb->linesize);


					// ---------------------------------------------------------
					// SECTION Snippet
					// ---------------------------------------------------------
					BYTE f_data;
					memcpy(&f_data, (BYTE *)v_frame_rgb->data[0], frame_buffer_size);
					frame_buffer_queue.emplace_back(f_data);
					// ---------------------------------------------------------
					// !SECTION Snippet
					// ---------------------------------------------------------

					rendering_site->render_frame((const BYTE *)v_frame_rgb->data[0], frame_buffer_size);
					last_rendered_time = Conversions::get_ms_since_epoch();
				}
			}
			av_packet_unref(&v_pkt);
		}
		
		// ---------------------------------------------------------------------
		// SECTION Snippet
		// ---------------------------------------------------------------------
		DBOUT("\n Playing" << 1 << "\n");

		for (int j = 0; j < frame_buffer_queue.size(); j++)
		{
			rendering_site->render_frame((const BYTE *)frame_buffer_queue[j], frame_buffer_size);
		}
		// ---------------------------------------------------------------------
		// !SECTION Snippet
		// ---------------------------------------------------------------------

		av_frame_free(&v_frame);

		avcodec_close(v_ctx_codec);
		avcodec_close(v_ctx_codec_orig);

		avformat_close_input(&v_ctx_format);
	}
};

struct custom_video_stream_factory : public sciter::behavior_factory
{

	custom_video_stream_factory() : sciter::behavior_factory("custom-video-generator")
	{
	}

	virtual sciter::event_handler *create(HELEMENT he)
	{
		return new custom_video_stream();
	}
};

custom_video_stream_factory custom_video_stream_factory_instance;
