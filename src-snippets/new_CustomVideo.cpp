
#pragma once

#include "Globals.h"
#include "stdafx.h"

#include <Windows.h>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include <mutex>

#include <sciter/sciter-x.h>
#include <sciter/sciter-x-behavior.h>
#include <sciter/sciter-x-threads.h>
#include <sciter/sciter-x-video-api.h>

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <libavutil/avstring.h>
}

#include "Conversions.h"

/*
	BEHAVIOR: video_generated_stream
		- provides synthetic video frames.
		- this code is here solely for the demo purposes - how
			to connect your own video frame stream with the rendering site

	COMMENTS:
		 <video style="behavior:video-generator video" />
	SAMPLE:
		 See: samples/video/video-generator-behavior.htm
	*/

struct custom_video_stream : public sciter::event_handler
{
	static void SaveFrame(AVFrame *vs_frame, int width, int height, int iFrame)
	{
		FILE *pFile;
		char szFilename[32];
		int y;

		// Open file
		sprintf(szFilename, "frame%d.ppm", iFrame);
		pFile = fopen(szFilename, "wb");
		if (pFile == NULL)
			return;

		// Write header
		fprintf(pFile, "P6\n%d %d\n255\n", width, height);

		// Write pixel data
		for (y = 0; y < height; y++)
			fwrite(vs_frame->data[0] + y * vs_frame->linesize[0], 1, width * 3, pFile);

		// Close file
		fclose(pFile);
	}

	sciter::om::hasset<sciter::video_destination> rendering_site;
	// ctor
	custom_video_stream() {}
	virtual ~custom_video_stream() {}

	virtual bool subscription(HELEMENT he, UINT &event_groups)
	{
		event_groups = HANDLE_BEHAVIOR_EVENT; // we only handle VIDEO_BIND_RQ here
		return true;
	}

	virtual void attached(HELEMENT he)
	{
		he = he;
	}

	virtual void detached(HELEMENT he) { asset_release(); }

	struct sciter_gen_params
	{
		sciter::om::hasset<sciter::fragmented_video_destination> fsite;
		sciter::value filenameFromUi;
	};

	virtual bool on_event(HELEMENT he, HELEMENT target, BEHAVIOR_EVENTS type, UINT_PTR reason)
	{
		if (type != VIDEO_BIND_RQ)
			return false;
		// we handle only VIDEO_BIND_RQ requests here

		//printf("VIDEO_BIND_RQ %d\n",reason);

		if (!reason)
			return true; // first phase, consume the event to mark as we will provide frames

		rendering_site = (sciter::video_destination *)reason;
		sciter::om::hasset<sciter::fragmented_video_destination> fsite;

		if (rendering_site->asset_get_interface(FRAGMENTED_VIDEO_DESTINATION_INAME, fsite.target()))
		{
			sciter_gen_params args;
			args.fsite = fsite;
			// TODO: Get the misc_filename, probably from the attached method?
			args.filenameFromUi = "d.mp4";

			sciter::thread(generation_thread, args);
		}

		return true;
	}

// -----------------------------------------------------------------------------
// SECTION AV Processing
// -----------------------------------------------------------------------------
#define MAX_VIDEOQ_SIZE (5 * 256 * 1024)
#define VIDEO_PICTURE_QUEUE_SIZE 1

	typedef struct PacketQueue
	{
		AVPacketList *first_pkt, *last_pkt;
		int nb_packets;
		int size;
		// ????????????????????????
		// Sciter doesn't have this
		// SDL_mutex *mutex;
		// SDL_cond *cond;
		std::mutex mutex_something;
	} PacketQueue;

	typedef struct VideoPicture
	{
		// SDL_Overlay *bmp;
		int width, height; /* source height & width */
		int allocated;
	} VideoPicture;

	typedef struct VideoState
	{
		// General
		AVFormatContext *g_ctx_format;
		struct SwsContext *sws_ctx;

		// ---------------------------------------------------------------------
		// Video
		// ---------------------------------------------------------------------
		int v_stream_counter;
		AVStream *v_stream;
		AVCodecContext *v_ctx_codec;
		PacketQueue v_pq;
		VideoPicture v_vp[VIDEO_PICTURE_QUEUE_SIZE];
		int v_vp_size;
		int v_vp_rindex;
		int v_vp_windex;

		// ---------------------------------------------------------------------
		// Audio
		// ---------------------------------------------------------------------
		int a_stream_counter;
		AVStream *a_stream;
		AVCodecContext *a_ctx_codec;
		PacketQueue a_pq;
		AVFrame a_frame;
		AVPacket a_pkt;
		// uint8_t a_buf[(AVCODEC_MAX_AUDIO_FRAME_SIZE * 3) / 2];
		unsigned int a_buf_size;
		unsigned int a_buf_index;
		uint8_t *a_pkt_data;
		int a_pkt_size;

		// ---------------------------------------------------------------------
		// Threads and Mutexes
		// ---------------------------------------------------------------------
		std::thread thread_parse_tid;
		std::thread thread_video_tid;
		std::mutex mutex_something;

		uint8_t *vs_frame_rgb_buffer;
		int i;
		int vs_frame_finished;
		bool vs_first_frame = true;
		double vs_frame_rate;
		double vs_frame_duration;

		// ---------------------------------------------------------------------
		// Misc
		// ---------------------------------------------------------------------
		char misc_filename[1024];
		int misc_quit;
	} VideoState;

	// SDL_Surface     *screen;
	// SDL_mutex       *screen_mutex;
	std::mutex screen_mutex;

	static VideoState *global_video_state;

	void packet_queue_init(PacketQueue *q)
	{
		// memset(q, 0, sizeof(PacketQueue));
		// q->mutex = SDL_CreateMutex();
		// q->cond = SDL_CreateCond();
	}

	int packet_queue_put(PacketQueue *q, AVPacket *pqp_pkt)
	{
		AVPacketList *pqp_pkt1;
		if (av_dup_packet(pqp_pkt) < 0)
		{
			_WINDEBUGOUT("ERROR8\n")
		}

		pqp_pkt1 = (AVPacketList *)av_malloc(sizeof(AVPacketList));
		if (!pqp_pkt1)
		{
			_WINDEBUGOUT("ERROR9\n");
		}
		pqp_pkt1->pkt = *pqp_pkt;
		pqp_pkt1->next = NULL;

		// SDL_LockMutex(q->mutex);
		q->mutex_something.lock();

		if (!q->last_pkt)
		{
			q->first_pkt = pqp_pkt1;
		}
		else
		{
			q->last_pkt->next = pqp_pkt1;
		}
		q->last_pkt = pqp_pkt1;
		q->nb_packets++;
		q->size += pqp_pkt1->pkt.size;
		// SQL_CondSignal(q->cond)

		// SDL_UnlockMutex(q->mutex);
		q->mutex_something.unlock();
		return 0;
	}

	static int packet_queue_get(PacketQueue *q, AVPacket *pqg_pkt, int block)
	{
		AVPacketList *pqg_pkt1;
		int ret;

		// SDL_LockMutex(q->mutex);
		q->mutex_something.lock();

		for (;;)
		{
			if (global_video_state->misc_quit)
			{
				ret = -1;
				break;
			}

			pqg_pkt1 = q->first_pkt;
			if (pqg_pkt1)
			{
				q->last_pkt = NULL;
				q->nb_packets--;
				q->size -= pqg_pkt1->pkt.size;
				*pqg_pkt = pqg_pkt1->pkt;
				av_free(pqg_pkt1);
				ret = 1;
				break;
			}
			else if (!block)
			{
				ret = 0;
				break;
			}
			else
			{
				// SDL_CondWait(q->cond, q->mutex)
			}
		}
		// SDL_UnlockMutex(q->mutex);
		q->mutex_something.unlock();
		return ret;
	}

	static void schedule_refresh(VideoState *vs, int delay)
	{
		// SDL_AddTimer(delay, sdl_refresh_timer_cb, is);
	}

	void video_display(VideoState *vs)
	{
		// SDL_Rect rect;
		VideoPicture *vp;
		float aspect_ratio;
		int w, h, x, y;
		int i;

		vp = &vs->v_pq[vs->v_vp_rindex];
		if (vp->bmp)
		{
			if (vs->v_ctx_codec->sample_aspect_ratio.num == 0)
			{
				aspect_ratio = 0;
			}
			else
			{
				aspect_ratio = av_q2d(vs->v_ctx_codec->sample_aspect_ratio) * vs->v_ctx_codec->width / vs->v_ctx_codec->height;
			}

			if (aspect_ratio <= 0.0)
			{
				aspect_ratio = (float)vs->v_ctx_codec->width / (float)vs->v_ctx_codec->height;
				h = screen->h;
				w = ((int)rint(h * aspect_ratio)) & -3;
				if (w > screen->w)
				{
					w = screen->w;
					h = ((int)rint(w / aspect_ratio)) & -3;
				}
				x = (screen->w - w) / 2;
				y = (screen->h - h) / 2;

				rect.x = x;
				rect.y = y;
				rect.w = w;
				rect.h = h;

				// SDL_LockMutex(screen_mutex);
				// SDL_DisplayYUVOverlay(vp->bpm, &rect);
				// SDL_UnlockMutex(screen_mutex);

				screen_mutex.lock();
				// ???? render to sciter ????
				screen_mutex.unlock();
			}
		}
	}

	void video_refresh_timer(void *userdata)
	{
		VideoState *vs = (VideoState *)userdata;
		VideoPicture *vp;

		if (vs->v_stream)
		{
			if (vs->v_vp_size == 0)
			{
				schedule_refresh(vs, 1);
			}
			else
			{
				vp = &vs->v_vp[vs->v_vp_rindex];
				// Now, normally here goes a ton of code
				// about timing, etc. we're just going to
				// guess at a delay for now. You can
				// increase and decrease this value and hard code
				// the timing - but I don't suggest that ;)
				// We'll learn how to do it for real later.
				schedule_refresh(vs, 40);

				// show the picture
				video_display(vs);

				// update queue for next picture
				if (++vs->v_vp_rindex == VIDEO_PICTURE_QUEUE_SIZE)
				{
					vs->v_vp_rindex = 0;
				}

				// SDL_LockMutex(vs->mutex_something);
				vs->mutex_something.lock();
				vs->v_vp_size--;
				// SDL_CondSignal(vs->mutex_conditionorsomething);
				// SDL_UnlockMutex(vs->mutex_something);
				vs->mutex_something.unlock();
			}
		}
		else
		{
			schedule_refresh(vs, 100);
		}
	}

	void alloc_picture(void *userdata)
	{
		VideoState *vs = (VideoState *)userdata;
		VideoPicture *vp;

		vp = &vs->v_vp[vs->v_vp_rindex];
		if (vp->bpm)
		{
			// we already have one make another, bigger/smaller
			SDL_FreeYUVOverlay(vp->bmp);
		}

		// Allocate a place to put our YUV image on that screen
		// SDL_LockMutex(screen_mutex);
		screen_mutex.lock();
		vp->bmp = SDL_CreateYUVOverlay(vs->v_ctx_codec->width,
									   vs->v_ctx_codec->height,
									   SDL_YV12_OVERLAY,
									   screen);
		// SDL_UnlockMutex(screen_mutex);
		screen_mutex.unlock();

		vp->width = vs->v_ctx_codec->width;
		vp->height = vs->v_ctx_codec->height;
		vp->allocated = 1;
	}

	int queue_picture(VideoState *vs, AVFrame *qp_frame)
	{
		VideoPicture *vp;
		int dst_pix_fmt;
		AVPicture pict;

		// wait until we have space for a new pic
		// SDL_LockMutex(vs->mutex_something);
		vs->mutex_something.lock();
		while (vs->v_vp_size >= VIDEO_PICTURE_QUEUE_SIZE && !vs->misc_quit)
		{
			// SDL_CondWait(vs->mutexconditionorsomething, vs->mutex_something);
		}
		// SDL_UnlockMutex(vs->mutex_something);
		vs->mutex_something.unlock();

		if (vs->misc_quit)
			return -1;

		// windex is set to 0 initially
		vp = &vs->v_vp[vs->v_vp_windex];

		// Allocate or resize the buffer
		if (!vp->bmp ||
			vp->width != vs->v_ctx_codec->width ||
			vp->height != vs->v_ctx_codec->height)
		{
			// SDL_Event event

			vp->allocated = 0;
			alloc_picture(vs);
			if (vs->misc_quit)
			{
				return -1;
			}
		}


		// We have a place to put our picture on the queue

		if (vp->bmp)
		{
			SDL_LockYUVOverlay(vp->bmp);

			dst_pix_fmt = AV_PIX_FMT_YUV420P;

			pict.data[0] = vp->bmp->pixels[0];
			pict.data[1] = vp->bmp->pixels[2];
			pict.data[2] = vp->bmp->pixels[1];

			pict.linesize[0] = vp->bmp->pitches[0];
			pict.linesize[1] = vp->bmp->pitches[2];
			pict.linesize[2] = vp->bmp->pitches[1];

			// Convert the image into YUV format that SDL uses
			sws_scale(vs->sws_ctx, (uint8_t const *const *)qp_frame->data,
					  qp_frame->linesize, 0, vs->v_ctx_codec->height,
					  pict.data, pict.linesize);

			// SDL_UnlockYUVOverlay(vp->bmp);

			// Now we inform our display thread that we have a pic ready
			if (++vs->v_vp_rindex == VIDEO_PICTURE_QUEUE_SIZE)
			{
				vs->v_vp_windex = 0;
			}
			// SDL_LockMutex(vs->mutex_something);
			vs->mutex_something.lock();
			vs->v_vp_size++;
			// SDL_UnlockMutex(vs->mutex_something);
			vs->mutex_something.unlock();
		}
		return 0;
	}

	int video_thread(void *param)
	{
		VideoState *vs = (VideoState *)param;
		AVPacket vt_pkt1;
		AVPacket *vt_packet = &vt_pkt1;
		int vt_frame_finished;
		AVFrame *vt_frame;

		vt_frame = av_frame_alloc();

		for (;;)
		{
			if (packet_queue_get(&vs->v_pq, vt_packet, 1) < 0)
			{
				// stopped getting packets
				break;
			}

			// Decode video frame
			avcodec_decode_video2(vs->v_ctx_codec, vt_frame, &vt_frame_finished, vt_packet);

			// Did we get a video frame?
			if (vt_frame_finished)
			{
				if (queue_picture(vs, vt_frame) < 0)
				{
					break;
				}
			}

			av_free_packet(vt_packet);
		}

		av_frame_free(&vt_frame);
		return 0;
	}

	int stream_component_open(VideoState *vs, int stream_index)
	{
		AVFormatContext *sco_ctx_format = vs->g_ctx_format;
		AVCodecContext *sco_ctx_codec;
		AVCodec *sco_codec;

		if (stream_index < 0 || stream_index >= sco_ctx_format->nb_streams)
		{
			_WINDEBUGOUT("ERROR1\n");
		}

		sco_codec = avcodec_find_decoder(sco_ctx_format->streams[stream_index]->codec->codec_id);
		if (!sco_codec)
		{
			_WINDEBUGOUT("ERROR2 Unsupported codec\n");
		}

		sco_ctx_codec = avcodec_alloc_context3(sco_codec);
		if (avcodec_copy_context(sco_ctx_codec, sco_ctx_format->streams[stream_index]->codec) != 0)
		{
			_WINDEBUGOUT("ERROR3 Couldn't copy codec context\n");
		}

		if (sco_ctx_codec->codec_type == AVMEDIA_TYPE_AUDIO)
		{
			// -----------------------------------------------------------------
			// I don't think I'll be handling audio lol
			// I don't even know how I'd do that with Sciter;
			// I'm already trying to figure out how to translate this tutorial
			// from SDL to Sciter rendering stuff
			// -----------------------------------------------------------------
		}

		if (avcodec_open2(sco_ctx_codec, sco_codec, NULL) < 0)
		{
			_WINDEBUGOUT("ERROR4 Unsupported codec\n");
		}

		switch (sco_ctx_codec->codec_type)
		{
		case AVMEDIA_TYPE_AUDIO:
			// Again
			// Not gonna bother with audio
			break;
		case AVMEDIA_TYPE_VIDEO:
			vs->v_stream_counter = stream_index;
			vs->v_stream = sco_ctx_format->streams[stream_index];
			vs->v_ctx_codec = sco_ctx_codec;

			packet_queue_init(*vs->v_vp);
			vs->thread_video_tid = std::thread(&custom_video_stream::video_thread, vs);
			vs->sws_ctx = sws_getContext(vs->av_stream->codec->width,
										 vs->av_stream->codec->height,
										 vs->av_stream->codec->pix_fmt,
										 vs->av_stream->codec->width,
										 vs->av_stream->codec->height,
										 AV_PIX_FMT_YUV420P,
										 SWS_BILINEAR, NULL, NULL, NULL);
			break;
		default:
			break;
		}
	}

	void decode_thread(void *param)
	{
		VideoState *vs = (VideoState *)param;
		AVFormatContext *dt_ctx_format;
		AVPacket dt_pkt1;
		AVPacket *dt_packet = &dt_pkt1;

		int video_index = -1;
		int audio_index = -1;
		int i;

		vs->v_stream_counter = -1;

		global_video_state = vs;

		if (avformat_open_input(&dt_ctx_format, vs->misc_filename, NULL, NULL) != 0)
		{
			_WINDEBUGOUT("Error5 Couldn't open file\n");
			return;
		}

		vs->g_ctx_format = dt_ctx_format;

		// Retrieve stream information
		if (avformat_find_stream_info(dt_ctx_format, NULL) < 0)
		{
			_WINDEBUGOUT("Error6 Couldn't find stream information\n");
			return;
		}

		// Dump information about file onto standard error
		av_dump_format(dt_ctx_format, 0, vs->misc_filename, 0);

		// Find the first video and audio stream
		// These are done so that the correct stream can be targeted hereafter
		for (i = 0; i < dt_ctx_format->nb_streams; i++)
		{
			if (dt_ctx_format->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO && video_index < 0)
			{
				video_index = i;
			}
			// -----------------------------------------------------------------
			// NOTE: Audio
			// if (dt_ctx_format->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO && audio_index < 0)
			// {
			// 	audio_index = i;
			// }
			// -----------------------------------------------------------------
		}

		// Open the respective steams
		if (video_index >= 0)
		{
			stream_component_open(vs, video_index);
		}

		// ---------------------------------------------------------------------
		// NOTE: Audio
		// if (audio_index >= 0 )
		// {
		// 	stream_component_open(vs, audio_index);
		// }
		// ---------------------------------------------------------------------

		// ---------------------------------------------------------------------
		// NOTE: Audio
		// if (vs->v_stream_counter < 0 || vs->vs_astream_int < 0)
		// ---------------------------------------------------------------------
		if (vs->v_stream_counter < 0)
		{
			_WINDEBUGOUT("ERROR7 Couldn't open codecs\n", vs->misc_filename);
			// goto fail;
		}

		// ---------------------------------------------------------------------
		// Main decode loop
		// ---------------------------------------------------------------------
		for (;;)
		{
			if (vs->misc_quit)
			{
				break;
			}

			// -----------------------------------------------------------------
			// NOTE: Audio
			// if (vs->a_pq.size > MAX_AUDIOQ_SIZE || vs->v_pq.size > MAX_VIDEOQ_SIZE )
			// -----------------------------------------------------------------
			// Seek stuff goes here
			if (vs->v_pq.size > MAX_VIDEOQ_SIZE)
			{
				// SDL_Delay(10);
				// continue;
			}

			if (av_read_frame(vs->g_ctx_format, dt_packet) < 0)
			{
				if (vs->g_ctx_format->pb->error == 0)
				{
					// SDL_Delay(100); /* no error; wait for user input */
					// continue;
				}
				else
				{
					break;
				}
			}

			// All done - wait for it
			while (!vs->misc_quit)
			{
				// SDL_Delay(100);
			}
		}

		// fail:
		// if (1)
		// {
		// 	SDL_Event event;
		// 	event.type = FF_QUIT_EVENT;
		// 	event.user.data1 = is;
		// 	SDL_PushEvent(&event);
		// }
		return;
	}

	// -------------------------------------------------------------------------
	// !SECTION AV Processing
	// -------------------------------------------------------------------------

	static void generation_thread(sciter_gen_params params)
	{
		// Create sciter rendering site thing
		sciter::om::hasset<sciter::fragmented_video_destination> rendering_site = params.fsite;

		// NOTE: Discrepancy: Single pointer in tutorial
		VideoState *vs = NULL;

		vs = (VideoState *)av_mallocz(sizeof(VideoState));

		// Register all formats and codecs; makes all codecs/muxers etc. supported
		// Can call them individually if want to
		av_register_all();

		// Copy the params.filenameFromUi to misc_filename
		av_strlcpy(vs->misc_filename,
				   Conversions::sciter_value_to_const_char_pointer(params.filenameFromUi),
				   sizeof(vs->misc_filename));

		schedule_refresh(vs, 40);

		// Create thread for decoding
		vs->thread_parse_tid = std::thread(&custom_video_stream::decode_thread, vs);
		// if(!vs->thread_parse_tid)
		// {
		// av_free(vs);
		// return;
		// }

		// for (;;)
		// {
		// 	SDL_WaitEvent(&event);
		// 	switch (event.type)
		// 	{
		// 	case FF_QUIT_EVENT:
		// 	case SDL_QUIT:
		// 		vs->misc_quit = 1;
		// 		SDL_Quit;
		// 		return;
		// 		break;
		// 	case FF_REFRESH_EVENT:
		// 		video_refresh_timer(event.user.data1);
		// 		break;
		// 	default:
		// 		break;
		// 	}
		// }

		return;

		// NOTE: See file CustomVideoBackup.cpp
		// The stuff from here was extracted into there so I can reference it
		// more easily without looking at the git commit history
	}
};

struct custom_video_stream_factory : public sciter::behavior_factory
{

	custom_video_stream_factory() : sciter::behavior_factory("custom-video-generator")
	{
	}

	// the only behavior_factory method:
	virtual sciter::event_handler *create(HELEMENT he)
	{
		return new custom_video_stream();
	}
};

// instantiating and attaching it to the global list
custom_video_stream_factory custom_video_stream_factory_instance;
