#pragma once

#include "Globals.h"
#include "Conversions.h"

// #include "config.h"
#include <inttypes.h>
#include <math.h>
#include <limits.h>
#include <signal.h>
#include <stdint.h>
#include <thread>
#include <mutex>
#include <condition_variable>


extern "C"
{
#include "libavutil/avstring.h"
#include "libavutil/eval.h"
#include "libavutil/mathematics.h"
#include "libavutil/pixdesc.h"
#include "libavutil/imgutils.h"
#include "libavutil/dict.h"
#include "libavutil/parseutils.h"
#include "libavutil/samplefmt.h"
#include "libavutil/avassert.h"
#include "libavutil/time.h"
#include "libavutil/bprint.h"
#include "libavformat/avformat.h"
#include "libavdevice/avdevice.h"
#include "libswscale/swscale.h"
#include "libavutil/opt.h"
#include "libavcodec/avfft.h"
#include "libswresample/swresample.h"
}

#include <assert.h>


#include <sciter/sciter-x.h>
#include <sciter/sciter-x-behavior.h>
#include <sciter/sciter-x-threads.h>
#include <sciter/sciter-x-video-api.h>

// class VideoDecoder
// {
// public:
// };