#pragma once

#include "Globals.h"
#include "Conversions.h"

#include <Windows.h>
#include <iostream>
#include <sstream>
#include <string>
#include <queue>
#include <thread>
#include <mutex>
#include <shared_mutex>
#include <condition_variable>
#include <array>
#include <time.h>
#include <chrono>


#include <sciter/sciter-x.h>
#include <sciter/sciter-x-behavior.h>
#include <sciter/sciter-x-threads.h>
#include <sciter/sciter-x-video-api.h>


extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <libavutil/avstring.h>
}

#define MAX_VIDEOQ_SIZE (5 * 256 * 1024)
#define MAX_VIDEOQ_NUM_PACKETS 512
#define VIDEO_PICTURE_QUEUE_SIZE 1

#define _WINDEBUGOUT(s)							\
{												\
	std::wostringstream os_;					\
	os_ << s;									\
	OutputDebugStringW(os_.str().c_str());		\
}

class VideoDecoder
{
public:
	int frame_buffer_size;



	typedef struct SciterParams
	{
		sciter::om::hasset<sciter::video_destination> fsite;
		sciter::value video_filename;
	} SciterParams;

	typedef struct PacketQueue
	{
		AVPacketList *first_pkt, *last_pkt;
		int nb_packets;
		int size;
		// ????????????????????????
		// Sciter doesn't have this
		// SDL_mutex *mutex;
		// SDL_cond *cond;
		std::shared_mutex mutex_packetsomething;
		std::condition_variable_any cond_packetsomething;
	} PacketQueue;

	typedef struct VideoPicture
	{
		AVFrame frame_data;
		// SDL_Overlay *bmp;
		int width, height; /* source height & width */
		int allocated;
	} VideoPicture;

	typedef struct VideoState
	{
		// General
		AVFormatContext *g_ctx_format;
		struct SwsContext *sws_ctx;

		// ---------------------------------------------------------------------
		// Video
		// ---------------------------------------------------------------------
		int v_stream_index;
		AVStream *v_stream;
		AVCodecContext *v_ctx_codec;
		VideoDecoder::PacketQueue v_pq;
		VideoDecoder::VideoPicture v_vp[VIDEO_PICTURE_QUEUE_SIZE];
		int v_vp_size;
		int v_vp_rindex;
		int v_vp_windex;

		// ---------------------------------------------------------------------
		// Audio
		// ---------------------------------------------------------------------
		int a_stream_counter;
		AVStream *a_stream;
		AVCodecContext *a_ctx_codec;
		VideoDecoder::PacketQueue a_pq;
		AVFrame a_frame;
		AVPacket a_pkt;
		// uint8_t a_buf[(AVCODEC_MAX_AUDIO_FRAME_SIZE * 3) / 2];
		unsigned int a_buf_size;
		unsigned int a_buf_index;
		uint8_t *a_pkt_data;
		int a_pkt_size;

		// ---------------------------------------------------------------------
		// Threads and Mutexes
		// ---------------------------------------------------------------------
		std::thread thread_parse_tid;
		std::thread thread_video_tid;
		std::mutex mutex_something;
		std::condition_variable cond_something;

		uint8_t *vs_frame_rgb_buffer;
		int i;
		int vs_frame_finished;
		bool vs_first_frame = true;
		double vs_frame_rate;
		double vs_frame_duration;

		// ---------------------------------------------------------------------
		// Misc
		// ---------------------------------------------------------------------
		// char misc_filename[1024];
		sciter::value misc_filename;
		int misc_quit;
	} VideoState;

	// SDL_Surface     *screen;
	// SDL_mutex       *screen_mutex;
	std::mutex screen_mutex;

	void packet_queue_init(VideoDecoder::PacketQueue *q);

	int packet_queue_put(VideoDecoder::PacketQueue *q, AVPacket *pqp_pkt);

	int packet_queue_get(VideoDecoder::PacketQueue *q, AVPacket *pqg_pkt, int block);

	uint32_t refresh_timer_cb(void *opaque);

	void schedule_refresh(VideoDecoder::VideoState *vs, int delay);

	void video_display(VideoDecoder::VideoState *vs);

	void video_refresh_timer(void *userdata);

	// void alloc_picture(void *userdata);

	int queue_picture(VideoDecoder::VideoState *vs, AVFrame *qp_frame);

	int video_thread(void *param);

	int stream_component_open(VideoDecoder::VideoState *vs, int stream_index);

	void decode_thread(void *param);

	// void initialise_video(SciterParams params);
	void VideoDecoder::initialise_video(sciter::om::hasset<sciter::video_destination> fsite);
};