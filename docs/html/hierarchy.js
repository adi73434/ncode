var hierarchy =
[
    [ "sciter::behavior_factory", null, [
      [ "custom_video_stream_factory", "structcustom__video__stream__factory.html", null ],
      [ "custom_video_stream_factory", "structcustom__video__stream__factory.html", null ]
    ] ],
    [ "Conversions", "class_conversions.html", null ],
    [ "sciter::event_handler", null, [
      [ "HandleEvents", "class_handle_events.html", null ],
      [ "custom_video_stream", "structcustom__video__stream.html", null ],
      [ "custom_video_stream", "structcustom__video__stream.html", null ]
    ] ],
    [ "FrameQueue", "struct_frame_queue.html", null ],
    [ "VideoDecoder::FrameQueue", "struct_video_decoder_1_1_frame_queue.html", null ],
    [ "VideoStreamer::FrameQueue", "struct_video_streamer_1_1_frame_queue.html", null ],
    [ "Globals", "class_globals.html", null ],
    [ "VideoStreamer::PacketQueue", "struct_video_streamer_1_1_packet_queue.html", null ],
    [ "render_params", "structrender__params.html", null ],
    [ "VideoDecoder", "class_video_decoder.html", null ],
    [ "VideoStreamer", "class_video_streamer.html", null ],
    [ "sciter::window", null, [
      [ "MainFrame", "class_main_frame.html", null ]
    ] ]
];