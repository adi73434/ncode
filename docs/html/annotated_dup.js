var annotated_dup =
[
    [ "Conversions", "class_conversions.html", "class_conversions" ],
    [ "custom_video_stream", "structcustom__video__stream.html", "structcustom__video__stream" ],
    [ "custom_video_stream_factory", "structcustom__video__stream__factory.html", "structcustom__video__stream__factory" ],
    [ "FrameQueue", "struct_frame_queue.html", "struct_frame_queue" ],
    [ "Globals", "class_globals.html", "class_globals" ],
    [ "HandleEvents", "class_handle_events.html", "class_handle_events" ],
    [ "MainFrame", "class_main_frame.html", "class_main_frame" ],
    [ "render_params", "structrender__params.html", "structrender__params" ],
    [ "VideoDecoder", "class_video_decoder.html", "class_video_decoder" ],
    [ "VideoStreamer", "class_video_streamer.html", "class_video_streamer" ]
];