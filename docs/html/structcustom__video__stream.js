var structcustom__video__stream =
[
    [ "custom_video_stream", "structcustom__video__stream.html#a0b42e76f04f5b18e04375722c887a6b8", null ],
    [ "~custom_video_stream", "structcustom__video__stream.html#a17b41c11a420f35eabaf17c4736539c3", null ],
    [ "custom_video_stream", "structcustom__video__stream.html#a0b42e76f04f5b18e04375722c887a6b8", null ],
    [ "~custom_video_stream", "structcustom__video__stream.html#a17b41c11a420f35eabaf17c4736539c3", null ],
    [ "attached", "structcustom__video__stream.html#aa425742de2ca5f85fd72c4ddb216735f", null ],
    [ "attached", "structcustom__video__stream.html#aa425742de2ca5f85fd72c4ddb216735f", null ],
    [ "detached", "structcustom__video__stream.html#a24cc63a0a5a68febc73c561f0598b77f", null ],
    [ "detached", "structcustom__video__stream.html#a24cc63a0a5a68febc73c561f0598b77f", null ],
    [ "generation_thread", "structcustom__video__stream.html#ac35f3e54e31ad17a0d7b9e86abba5b7a", null ],
    [ "on_event", "structcustom__video__stream.html#a20d921207c222f1f4cc74109c2d9fd75", null ],
    [ "on_event", "structcustom__video__stream.html#a20d921207c222f1f4cc74109c2d9fd75", null ],
    [ "render_thread", "structcustom__video__stream.html#a63ee97b8e5f02907acf180d0a722bfe5", null ],
    [ "SaveFrame", "structcustom__video__stream.html#a77a857e5dcdd2e079458f9e6a8153876", null ],
    [ "subscription", "structcustom__video__stream.html#a40e7948eb3f66e64948fcb5b9a8beae0", null ],
    [ "subscription", "structcustom__video__stream.html#a40e7948eb3f66e64948fcb5b9a8beae0", null ],
    [ "rendering_site", "structcustom__video__stream.html#ac5c67a0fbab4ddb24b70b25a9811f488", null ]
];