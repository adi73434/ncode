var class_conversions =
[
    [ "decode_url", "class_conversions.html#aec1bda43b95085934ddc77537cc6aab4", null ],
    [ "encode_url", "class_conversions.html#a526ebac9909e28d2f1cbbbd577013866", null ],
    [ "file_name_from_file_path", "class_conversions.html#a15d82c64dfbac2fca16e7de5611ef111", null ],
    [ "file_name_no_extension_from_file_path", "class_conversions.html#a013d2e8af70a61ec34801b22f65d5b33", null ],
    [ "get_current_time_point", "class_conversions.html#aff5e0b76a4b1c8deddb902e47fa6d573", null ],
    [ "get_mins_since_epoch", "class_conversions.html#aa58ccfa4a0babe938c42c167885a06f0", null ],
    [ "get_ms_since_epoch", "class_conversions.html#a16b68c89cf07c611b87f56de83ae052b", null ],
    [ "pop_front", "class_conversions.html#a9b974ae14367896c12f431d541306b0e", null ],
    [ "sciter_string_to_std_string", "class_conversions.html#a0e92515db81fe0131fe241494085fd4a", null ],
    [ "sciter_value_to_const_char_pointer", "class_conversions.html#ad0ce1975e8775b2521cabfd7c820866a", null ],
    [ "sciter_value_to_int", "class_conversions.html#a88c123f1ff8f2630ac8badd5100c7693", null ],
    [ "sciter_value_to_sciter_string", "class_conversions.html#a054a3fcf4c587f86acfb6ecd046db8e2", null ],
    [ "sciter_value_to_std_string", "class_conversions.html#a02b90ee996d9370c3ef1db57b15d4598", null ],
    [ "strip_sciter_file_prefix", "class_conversions.html#a48858b01fd836e54e86e1ad04b99490c", null ]
];