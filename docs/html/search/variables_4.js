var searchData=
[
  ['file_5fdir_239',['file_dir',['../class_video_decoder.html#a5aa9afecfa5c42be23d958caa041f97d',1,'VideoDecoder::file_dir()'],['../class_video_streamer.html#ac1f14342b6281667a4347d0ccc24bd90',1,'VideoStreamer::file_dir()']]],
  ['frame_5fdecode_5ffinished_240',['frame_decode_finished',['../class_video_decoder.html#a5d0ce407c35a0ac433ba0d19ac180080',1,'VideoDecoder']]],
  ['frame_5fduration_241',['frame_duration',['../class_video_decoder.html#aacfc04050815c0c69bdc5b6ca6ad9364',1,'VideoDecoder::frame_duration()'],['../class_video_streamer.html#ae02707c5ed0d3ea8aff46ec321e45753',1,'VideoStreamer::frame_duration()']]],
  ['frame_5frate_242',['frame_rate',['../class_video_decoder.html#a11fab2203b0bccc911587af9d31caf95',1,'VideoDecoder::frame_rate()'],['../class_video_streamer.html#adb782b24c9787f4568a3d3f2ca563e6e',1,'VideoStreamer::frame_rate()']]],
  ['frame_5fskip_5fframe_5fcount_5ftolerance_243',['frame_skip_frame_count_tolerance',['../class_video_streamer.html#a485dbfb80065cd88e1e7462712cabc44',1,'VideoStreamer']]],
  ['frame_5fskip_5fignore_5fmultiplier_244',['frame_skip_ignore_multiplier',['../class_video_streamer.html#a194223d95e160be853b5b6dca3aba76c',1,'VideoStreamer']]]
];
