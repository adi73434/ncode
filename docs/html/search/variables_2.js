var searchData=
[
  ['decode_5fclock_5fstart_226',['decode_clock_start',['../class_video_decoder.html#a25a159f6f7de67c467c5fe23311f7641',1,'VideoDecoder']]],
  ['decode_5ftime_5favg_227',['decode_time_avg',['../class_video_decoder.html#a8e508847702a69272f475936ea5f6ade',1,'VideoDecoder']]],
  ['decoder_5fis_5frunning_228',['decoder_is_running',['../class_globals.html#a0738a30b850fd3e3b6267902f6ef1982',1,'Globals']]],
  ['decoder_5fret_229',['decoder_ret',['../class_video_streamer.html#ac4efea8478022b09646aa91c20d7f486',1,'VideoStreamer']]],
  ['decoder_5fstill_5frunning_230',['decoder_still_running',['../class_video_streamer.html#abb9ae1c6a7e08668f5117825c6b9f6b9',1,'VideoStreamer']]],
  ['decoding_5felapsed_5ftotal_231',['decoding_elapsed_total',['../class_video_decoder.html#a2c20181e8235f27940df9585e92882a2',1,'VideoDecoder']]],
  ['desired_5fencoder_5fname_232',['desired_encoder_name',['../class_video_streamer.html#adee2085caf6c27a83e98d5907edbce95',1,'VideoStreamer']]],
  ['desired_5fquality_233',['desired_quality',['../class_video_streamer.html#a61c3e6331c863cfc04036189b83d132c',1,'VideoStreamer']]]
];
