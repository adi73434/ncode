var searchData=
[
  ['read_5fspeedtest_5fresult_5fthread_83',['read_speedtest_result_thread',['../class_handle_events.html#a88e01f3dfd3b74047ef81829e8474303',1,'HandleEvents']]],
  ['redecoder_5fframe_5fqueue_84',['redecoder_frame_queue',['../class_video_streamer.html#a2012f2df9bd02e6d38a0d786a1835c47',1,'VideoStreamer']]],
  ['redecoder_5fret_85',['redecoder_ret',['../class_video_streamer.html#aff4501097253e62e20744e9f17bd5e3b',1,'VideoStreamer']]],
  ['redecoder_5fstill_5frunning_86',['redecoder_still_running',['../class_video_streamer.html#a845ec696506dfeca78149fb8a6687923',1,'VideoStreamer']]],
  ['render_5fparams_87',['render_params',['../structrender__params.html',1,'']]],
  ['renderer_5fkeep_5fwaiting_5ffor_5fdecoder_88',['renderer_keep_waiting_for_decoder',['../class_video_decoder.html#aaa5e687194126c68d27e1ccd3dd871ea',1,'VideoDecoder']]],
  ['renderer_5fstill_5frunning_89',['renderer_still_running',['../class_video_streamer.html#a9bb5667937db5de73a74038c87136fdf',1,'VideoStreamer']]],
  ['rendering_5fsite_90',['rendering_site',['../class_video_decoder.html#a4c6e9f1fd51335aef28d636b14ddf2e4',1,'VideoDecoder::rendering_site()'],['../class_video_streamer.html#abf2d51a7bd4f1e44af0d21c74cbe0bc0',1,'VideoStreamer::rendering_site()']]],
  ['rgb24_5fframe_5fbuffer_5fsize_91',['rgb24_frame_buffer_size',['../class_video_decoder.html#a75369529770f77b6be890bc9ad4d4aa2',1,'VideoDecoder::rgb24_frame_buffer_size()'],['../class_video_streamer.html#a02acf9c5b64f0cefffe933573352270a',1,'VideoStreamer::rgb24_frame_buffer_size()']]]
];
