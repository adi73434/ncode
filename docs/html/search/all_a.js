var searchData=
[
  ['mainframe_71',['MainFrame',['../class_main_frame.html',1,'']]],
  ['make_5fexit_5fsafe_72',['make_exit_safe',['../class_handle_events.html#a2452d2631b005895d4b06e07aa59d438',1,'HandleEvents']]],
  ['max_5fencoder_5fbitrate_5fcap_73',['max_encoder_bitrate_cap',['../class_globals.html#a2c9318ce3b10fdf7483ad318c1536e8b',1,'Globals']]],
  ['max_5fframes_74',['max_frames',['../struct_video_decoder_1_1_frame_queue.html#a03c431f710c1dcbfdd7a1db420ef23dc',1,'VideoDecoder::FrameQueue::max_frames()'],['../struct_video_streamer_1_1_frame_queue.html#a738bddaf5eba59100fc0fd33a14add56',1,'VideoStreamer::FrameQueue::max_frames()']]],
  ['max_5fpackets_75',['max_packets',['../struct_video_streamer_1_1_packet_queue.html#a30f5b624fa0b0ca844c2219b98bf0c1b',1,'VideoStreamer::PacketQueue']]]
];
