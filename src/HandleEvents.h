// This is an open source non-commercial project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#pragma once

#include <string>
#include <vector>
#include <thread>
#include <mutex>
#include <atomic>

#include <json/json.h>

#pragma warning(push, 0)
#include <sciter/sciter-x.h>
#include <sciter/sciter-x-behavior.h>
#include <sciter/sciter-x-threads.h>
#include <sciter/sciter-x-video-api.h>
#pragma warning(pop)

class HandleEvents : public sciter::event_handler
{
	HWND _hwnd;

public:
	//HandleEvents()
	//{
	//}

	//~HandleEvents();

	// -------------------------------------------------------------------------
	// SECTION Instantiating/configuring stuff via TiScript
	// -------------------------------------------------------------------------
	/**
	 * @brief Set the global video directory and concatenation info, which is stored in Globals class,
	 * to whatever was received from the UI.
	 * This is done to make syncing the file names in TiScript and in C++ a bit easier. It's also a little
	 * clearer imo when this is set via a call from main.tis.
	 *
	 * @return sciter::value
	 */
	sciter::value set_global_video_file_concat_values(sciter::value video_file_dir, sciter::value video_file_encoded_affix, sciter::value video_file_extension);

	/**
	 * @brief Set the API location in the Globals class based on what was passed from the UI
	 *
	 * @param ui_api_address
	 * @param ui_api_port
	 * @return sciter::value
	 */
	sciter::value set_global_api_location(sciter::value ui_api_address, sciter::value ui_api_port);

	/**
	 * @brief Set the details pertaining to the video and stream arrays which will be used for
	 * generating the JSON data to submit to the server, and wherever else the positions need to be targeted
	 *
	 * @param videos_settings
	 * @param streams_settings
	 * @param video_array_2d
	 * @return sciter::value
	 */
	sciter::value set_3d_video_and_stream_array_details(sciter::value videos_settings,
														sciter::value streams_settings,
														sciter::value video_array_2d);
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------

	// -------------------------------------------------------------------------
	// SECTION Decoding and Playback
	// -------------------------------------------------------------------------
	bool try_toggle_pause_video;
	// bool video_is_paused;

	// struct play_video_thread_params
	// {
	// 	sciter::value ui_file_dest;
	// 	sciter::value ui_callback;
	// };

	/**
	 * @brief Handle the creation of a VideoDecoder object with the correct video file. This manually triggers
	 * the start() method of VideoDecoder on a separate thread. It also loops while keep_decoder_alive is true
	 * so that the code after that, which closes the VideoDecoder, doesn't run until specified. That loop also
	 * handles some events (which will probably be added with time) such as pausing.
	 *
	 * @param params
	 */
	void play_video_thread(std::string file_dest, sciter::value ui_callback);

	/**
	 * @brief This is the same as play_video_thread, but here we create a live encoder and decoder combo,
	 * rather than just a from-file decoder. This also takes a quality setting from the UI.
	 * 
	 * @param file_dest
	 * @param encoder_name
	 * @param desired_quality
	 * @param ui_callback
	 */
	void play_video_stream_thread(std::string file_dest,
								  std::string encoder_name,
								  std::string desired_quality,
								  sciter::value ui_callback);

	/**
	 * @brief Calls play_video_thread on a detached thread with the concatenated file name if a video is not already running.
	 * It also provides it with a ui_callback to call a UI function if something goes wrong.
	 *
	 * @param ui_play_type
	 * @param ui_file_dest
	 * @param ui_encoder_name
	 * @param ui_desired_quality
	 * @param ui_callback
	 * @return sciter::value
	 */
	sciter::value play_video(sciter::value ui_play_type,
							 sciter::value ui_file_dest,
							 sciter::value ui_encoder_name,
							 sciter::value ui_desired_quality,
							 sciter::value ui_callback);

	/**
	 * @brief This sets keep_decoder_alive to false, causing the loop in play_video_thread to stop which then
	 * informs the VideoDecoder object to stop.
	 *
	 * @note This waits until decoder_is_running is false
	 *
	 */
	sciter::value stop_video();

	/**
	 * @brief Set try_toggle_pause_video to true for the loop in play_video_thread to handle
	 *
	 * @return sciter::value
	 */
	sciter::value toggle_pause_video();
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------

	// -------------------------------------------------------------------------
	// SECTION Encoding
	// -------------------------------------------------------------------------

	/**
	 * @brief 2D vector used for the queue of video/codec combos to encode
	 * @note Once in the second vector, 0th item is ui_file_to_encode and 1st item is ui_encoder_tu_use
	 */
	std::vector<std::vector<std::string>> encoder_queue;

	/**
	 * @brief This is used to provide a callback to the UI to draw a progress bar
	 *
	 */
	sciter::value encode_file_callback;

	/**
	 * @brief Whether or not to keep watching the queue thread and trying to encode files.
	 * This is mostly a pedantry thing just to have an exit condition.
	 *
	 */
	std::atomic<bool> keep_watching_queue_thread;

	/**
	 * @brief This does the actually encoding of the video file
	 *
	 * @param ui_file_to_include
	 * @return sciter::value
	 */
	void encode_file(std::string file_to_encode, std::string encoder_to_use);

	/**
	 * @brief This just adds a new item to the encoder_queue by acquiring the mutex for the queue,
	 * creating a temporary vector with the file and codec strings, and then pushing the temp vector
	 * into the queue.
	 *
	 * @param ui_video_group
	 * @param ui_video_in_group_id
	 * @param ui_file_to_encode
	 * @param ui_encoder_to_use
	 * @return sciter::value
	 */
	sciter::value cpp_enqueue_encoder(sciter::value ui_video_group, sciter::value ui_video_in_group_id, sciter::value ui_file_to_encode, sciter::value ui_encoder_to_use);

	/**
	 * @brief This just tells this object to watch the queue and run encode_file (one at a time)
	 * when there is something in the queue of files to encode. The implementation locks the mtx_encoder_queue,
	 * copies the file and codec names from the front of the encoder_queue, pops the front of the vector,
	 * unlocks the mutex, and triggers the encoder.
	 *
	 */
	void watch_encoder_queue_thread();

	/**
	 * @brief This runs watch_encoder_queue_thread() on a separate thread
	 *
	 * @return sciter::value
	 */
	sciter::value cpp_trigger_watch_encoder_queue(sciter::value ui_callback);
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------

	// -------------------------------------------------------------------------
	// SECTION Server requests
	// -------------------------------------------------------------------------
	/**
	 * @brief Submit a unique code check to the PHP using cpp-httplib.
	 *
	 * @details This does not insert anything into the database and will not mark the code as used.
	 *
	 * @param ui_user_code
	 * @return sciter::value
	 */
	sciter::value cpp_check_user_code(sciter::value ui_user_code);

	/**
	 * @brief This converts the video details 3d array, which has already been flattened to 2D by
	 * de-grouping the videos in TiScript. This then converts it to a vector.
	 * 
	 */
	std::vector<std::vector<std::string>> HandleEvents::convert_2d_ui_array_to_2d_vector(
		sciter::value file_codec_array);

	/**
	 * @brief This converts the 3D array passed into it, which is a sciter::value, into a JSON
	 * object which is formatted as follows
	 * 
	 * - streams/video data
	 * - - file
	 * - - - codec
	 * - - - - comment
	 * - - - - rattings[]
	 * - - - codec
	 * - - - - comment
	 * - - - - rattings[]
	 * - - file
	 * - - - codec
	 * - - - - comment
	 * - - - - rattings[]
	 * - - - codec
	 * - - - - comment
	 * - - - - rattings[]
	 * 
	 * @param file_codec_array 
	 * @param file_count 
	 * @param codecs_per_file 
	 * @param comment_start_index 
	 * @param content_count 
	 * @param has_quality_rating 
	 * @return Json::Value 
	 */
	Json::Value convert_3d_ui_array_to_json(
		sciter::value file_codec_array,
		int file_count,
		int codecs_per_file,
		int comment_start_index,
		int content_count,
		bool has_quality_rating);

	/**
	 * @brief This gets the std::string that is within the std::vector which represented a passed array
	 * from videoFilesAndDetails[i] in main.tis.
	 *
	 * @note This only handles the one dimensional "inner" array
	 *
	 * @param video_file_details_single
	 * @param start_pos indicates at what position in the array the first rating is
	 * @param end_pos indicates at what position in the array the last rating is
	 * @return std::string
	 */
	std::string grab_comments_from_vector(std::vector<std::string> video_file_details_single, int start_pos, int end_pos);

	/**
	 * @brief Submit the data to the PHP server using cpp-httplib
	 *
	 * @details Once the required fields are verified and the unique code is valid,
	 * this method is used to submit all of the data. This sends the data alongside
	 * the validation code so that it is inserted into the database instantly if the
	 * unique code is accepted.
	 *
	 * @param ui_submit_array expands to the following, in chronological order:
	 * - unique_code
	 * - intro_nick_name
	 * - intro_age_range
	 * - vod_2d_array_ratings
	 * - vod_comment1
	 * - vod_comment2
	 * - vod_comment3
	 * - vod_comment4
	 * - outro_employment
	 * - outro_industry
	 * - outro_tech_hobbies
	 * - outro_selected_rating1
	 * - outro_selected_rating2
	 * - outro_selected_rating3
	 * - outro_selected_rating4
	 * @param ui_callback
	 * @return sciter::value
	 */
	sciter::value submit_survey_thread(sciter::value ui_submit_array, sciter::value ui_callback, sciter::value ui_component_context);

	/**
	 * @brief This just delegates the values to submit to submit_survey_thread on a detached thread so that
	 * the GUI doesn't block.
	 *
	 * @param ui_submit_array
	 * @param ui_callback
	 * @return sciter::value
	 */
	sciter::value cpp_submit_survey(sciter::value ui_submit_array, sciter::value ui_callback, sciter::value ui_component_context);
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------

	// -------------------------------------------------------------------------
	// SECTION Speedtest
	// -------------------------------------------------------------------------

	/**
	 * @brief This loops until a speedtest result is found, storing the upload speed and marking
	 * it as read in the Global variables.
	 * 
	 */
	void read_speedtest_result_thread();

	/**
	 * @brief This triggers the Ookla Speedtest CLI tool
	 * 
	 * @return sciter::value 
	 */
	sciter::value cpp_trigger_ookla_speedtest();
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------

	// -------------------------------------------------------------------------
	// SECTION Cleanup stuff
	// -------------------------------------------------------------------------
	/**
	 * @brief This method ensures that any work being done, such as within the video decoder,
	 * is closed when a window close event is triggered. This works by changing variables that
	 * the implementations throughout (i.e. in separate threads) watch to ensure they can keep running.
	 *
	 * @note The window will not close if this method does not inform it that it can.
	 *
	 * @return sciter::value
	 */
	sciter::value make_exit_safe();

	/**
	 * @brief We're at least somewhat well mannered so this'll cleanup stuff that was saved to
	 * the user's disk. This is also triggered on app launch so that, incase the user
	 * was part way through the process and closed, they will start from fresh
	 *
	 * @return sciter::value
	 */
	sciter::value HandleEvents::cpp_cleanup_files();
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------

	// -------------------------------------------------------------------------
	// SECTION: Expose to Sciter
	// -------------------------------------------------------------------------
	SOM_PASSPORT_BEGIN(HandleEvents)
	SOM_FUNCS(
		SOM_FUNC(set_global_video_file_concat_values),
		SOM_FUNC(set_global_api_location),
		SOM_FUNC(set_3d_video_and_stream_array_details),
		SOM_FUNC(play_video),
		SOM_FUNC(toggle_pause_video),
		SOM_FUNC(cpp_enqueue_encoder),
		SOM_FUNC(cpp_trigger_watch_encoder_queue),
		SOM_FUNC(cpp_check_user_code),
		SOM_FUNC(cpp_submit_survey),
		SOM_FUNC(cpp_trigger_ookla_speedtest),
		SOM_FUNC(make_exit_safe),
		SOM_FUNC(cpp_cleanup_files));
	SOM_PASSPORT_END;
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------

private:
};
