#pragma once

// Include sciter, ignore warnings
#pragma warning(push, 0)
#include <sciter/sciter-x.h>
#include <sciter/sciter-x-window.hpp>
#pragma warning(pop)