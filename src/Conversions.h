#pragma once

#include <string>
// #include <vector>
#include <sciter/sciter-x.h>
#include <time.h>
#include <chrono>

class Conversions
{
public:

	/**
	 * @brief Get the current time point, used for sleep_until
	 * 
	 * @return std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds> 
	 */
	static std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds> get_current_time_point();

	/**
	 * @brief Get the milliseconds since epoch
	 * 
	 * @return std::chrono::milliseconds 
	 */
	static std::chrono::milliseconds get_ms_since_epoch();
	/**
	 * @brief Get the minutes since epoch
	 * 
	 * @return std::chrono::minutes 
	 */
	static std::chrono::minutes get_mins_since_epoch();

	// ---------------------------------------------------------------------
	// SECTION sciter::value ->
	// ---------------------------------------------------------------------
	/**
	 * @brief Convert a sciter::value to a sciter::string
	 * 
	 * @param inVar 
	 * @return sciter::string 
	 */
	static sciter::string sciter_value_to_sciter_string(sciter::value inVar);
	/**
	 * @brief Convert a sciter::value to an std::string
	 * 
	 * @param inVar 
	 * @return std::string 
	 */
	static std::string sciter_value_to_std_string(sciter::value inVar);
	/**
	 * @brief Convert a sciter::value to an int
	 * 
	 * @param inVar 
	 * @return int 
	 */
	static int sciter_value_to_int(sciter::value inVar);
	/**
	 * @brief Convert a sciter::value to a const char *
	 * 
	 * @param inVar 
	 * @return const char* 
	 */
	static const char *sciter_value_to_const_char_pointer(sciter::value inVar);
	// ---------------------------------------------------------------------
	// !SECTION
	// ---------------------------------------------------------------------

	// ---------------------------------------------------------------------
	// SECTION sciter::string ->
	// ---------------------------------------------------------------------
	/**
	 * @brief Convert a sciter::string to an std::string
	 * 
	 * @param inVar 
	 * @return std::string 
	 */
	static std::string sciter_string_to_std_string(sciter::string inVar);
	/**
	 * @brief Get the file name from a file path which looks like this: `file:///C:/folder/file.txt`
	 * 
	 * @param inVar 
	 * @return std::string 
	 */
	static std::string file_name_from_file_path(std::string inVar);
	/**
	 * @brief Get the file name without the extension from a file path which looks like this: `file:///C:/folder/file.txt`
	 * 
	 * @param inVar 
	 * @return std::string 
	 */
	static std::string file_name_no_extension_from_file_path(std::string inVar);

	// ---------------------------------------------------------------------
	// SECTION Sciter path stuff
	// ---------------------------------------------------------------------
	/**
	 * @brief Removes the `file://` prefix from a file path
	 * 
	 * @param inVar 
	 * @return std::string 
	 */
	static std::string strip_sciter_file_prefix(std::string inVar);
	/**
	 * @brief Encodes a URL
	 * 
	 * @param inVar 
	 * @return std::string 
	 */
	static std::string encode_url(std::string inVar);

	/**
	 * @brief Decodes a HTML URL.
	 * 
	 * @warning This does not work.
	 * 
	 * @note As this is not used, I'm likely not going to change this. Just don't use this.
	 * 
	 * @param inVar 
	 * @return std::string 
	 */
	static std::string decode_url(std::string inVar);

	// ---------------------------------------------------------------------
	// SECTION Miscellaneous
	// ---------------------------------------------------------------------
	/**
	 * @brief This pops the front of a vector by moving the front item to the back
	 * and then popping the back item.
	 * 
	 * @tparam T 
	 * @param vec 
	 */
	template <typename T>
	static void pop_front(std::vector<T> &vec)
	{
		assert(!vec.empty());
    	vec.erase(vec.begin());
	}
};