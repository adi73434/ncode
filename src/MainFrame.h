// This is an open source non-commercial project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#ifndef MY_MAINFRAME_H
#define MY_MAINFRAME_H


#include "IncludeSciter.h"

/**
 * @brief This is the main window frame. It was separated into this whilst I was creating the project,
 * and ever since then I sort of just preferred it to be like this.
 * 
 * @note This file is included in Globals.h intentionally to, if I recall correctly, be able to grab
 * the hWnd, but I've since also added those to be stored in the Globals class itself and this whole thing
 * would need cleaning up if I actually need to use hWnd anywhere but main.cpp
 * 
 */
class MainFrame : public sciter::window
{
  public:
	MainFrame() : window(SW_TITLEBAR | SW_RESIZEABLE | SW_CONTROLS | SW_MAIN | SW_ENABLE_DEBUG)
	{
	}
};



#endif