
#include "VideoStreamer.h"

#include "Globals.h"
#include "stdafx.h"

#include <Windows.h>
#include <iostream>
#include <sstream>
#include <string>

#include <array>
#include <time.h>
#include <chrono>

#define _WINDEBUGOUT(s)                        \
	{                                          \
		std::wostringstream os_;               \
		os_ << s;                              \
		OutputDebugStringW(os_.str().c_str()); \
	}

std::mutex mutex_render_frame_queue;
std::mutex mutex_encoder_frame_queue;
std::mutex mutex_source_frame_queue;

void debug_ffmpeg_error(int ret)
{
	char err[1024] = {0};
	av_strerror(ret, err, 1024);
	_WINDEBUGOUT("FFMPEG Error: ");
	_WINDEBUGOUT(err);
	_WINDEBUGOUT(std::endl);
	return;
}

VideoStreamer::VideoStreamer(sciter::video_destination *param_dst,
							 std::string param_file_dir,
							 std::string encoder_name,
							 std::string param_desired_quality,
							 int param_max_prepared_input,
							 int param_max_output,
							 sciter::value param_ui_callback)
{
	this->rendering_site = param_dst;
	this->file_dir = param_file_dir;
	this->desired_encoder_name = encoder_name;
	this->desired_quality = param_desired_quality;

	this->source_frame_queue.max_frames = param_max_prepared_input;
	this->encoder_output_packet_queue.max_packets = param_max_output;
	this->redecoder_frame_queue.max_frames = param_max_output;

	this->ui_callback = param_ui_callback;

	this->abruptly_end_all = false;
	this->v_source_ctx_format = NULL;
	this->v_source_ctx_codec_orig = NULL;
	this->v_source_ctx_codec = NULL;
	this->v_encoder_codec = NULL;
	this->v_source_frame = NULL;
};

VideoStreamer::~VideoStreamer()
{
}

void VideoStreamer::do_emulate_frame_skip(int time_dif_ms_count)
{
	_WINDEBUGOUT("Skipping frames" << std::endl);
	bool keep_skipping = true;
	// -------------------------------------------------------------------------
	// FIXME: I'm pretty sure there can be some sort of a race condition
	// here with time_rendered_frame
	// -------------------------------------------------------------------------
	if (!abruptly_end_all && redecoder_still_running && encoder_still_running)
	{
		int64_t skip_frame_pts;
		std::unique_lock<std::mutex> q_source(mutex_source_frame_queue);
		while (!source_frame_queue.v_frame_list.empty() && keep_skipping)
		{
			// -----------------------------------------------------------------
			// -----------------------------------------------------------------

			int64_t time_src = source_frame_queue.v_frame_list.front()->best_effort_timestamp;
			time_src = av_rescale_q(time_src, v_source_ctx_codec->time_base, timebase_ms);

			_WINDEBUGOUT("Newest source time: " << time_src << std::endl);
			_WINDEBUGOUT("Last render timeee: " << time_rendered_frame << std::endl);
			// -----------------------------------------------------------------
			// If the next source frame is more than 1-frame-duration behind the renderer
			// skip it
			// -----------------------------------------------------------------
			if (time_src + time_dif_ms_count > time_rendered_frame)
			{
				_WINDEBUGOUT("Skipping frames success" << std::endl);
				av_frame_free(&source_frame_queue.v_frame_list.front());
				source_frame_queue.v_frame_list.pop();
			}
			else
			{
				keep_skipping = false;
			}
		}
		q_source.unlock();
	}
}

void VideoStreamer::do_render_emulated_stream()
{
	// Get buffer size for RGB24 output frames
	// NOTE: currently outputting IYUV, so not using this
	// rgb24_frame_buffer_size = av_image_get_buffer_size(AV_PIX_FMT_RGB24, v_redecoder_ctx_codec->width, v_redecoder_ctx_codec->height, 32);

	// Get the current time. This'll be used to offset via the presentation time
	renderer_start_time = Conversions::get_current_time_point();

	v_display_frame = av_frame_alloc();
	v_display_frame_buffer = av_buffer_alloc(input_frame_buffer_size);
	av_image_fill_arrays(v_display_frame->data,
						 v_display_frame->linesize,
						 v_display_frame_buffer->data,
						 v_redecoder_ctx_codec->pix_fmt,
						 v_redecoder_ctx_codec->width,
						 v_redecoder_ctx_codec->height,
						 32);

	rendering_site->start_streaming(v_redecoder_ctx_codec->width, v_redecoder_ctx_codec->height, sciter::COLOR_SPACE_IYUV);

	while (!abruptly_end_all)
	{
		// Skip if user paused video. This also sleeps so that this loop
		// doesn't spin for no reason
		if (playback_paused_by_user)
		{
			Sleep(50);
			continue;
		}

		// ---------------------------------------------------------------------
		// If the final decoder finished and we've no more frames, end.
		// ---------------------------------------------------------------------
		// if (!redecoder_still_running && redecoder_frame_queue.v_frame_list.empty())
		// {
		// 	goto EXIT_do_render_video;
		// }

		// ---------------------------------------------------------------------
		// If no output frames, wait
		// NOTE: q_renderer mutex
		std::unique_lock<std::mutex> q_renderer(mutex_render_frame_queue);
		while (redecoder_frame_queue.v_frame_list.empty())
		{
			// If the redecoder is finished or we should end, end.
			if (!redecoder_still_running || abruptly_end_all)
			{
				goto EXIT_do_render_video;
			}
			cond_redecoder_frame_queue_too_small.wait(q_renderer);
		}
		q_renderer.unlock();
		// ---------------------------------------------------------------------

		// Convert re-decoder output. Regardless of whether the format is changing,
		// this is used. I still think just moving the av_frame reference should work for display
		// but I don't know
		sws_scale(sws_yuv_from_redecoder,
				  (uint8_t const *const *)redecoder_frame_queue.v_frame_list.front(),
				  redecoder_frame_queue.v_frame_list.front()->linesize,
				  0,
				  v_redecoder_ctx_codec->height,
				  v_display_frame->data,
				  v_display_frame->linesize);

		int64_t default_present_time = redecoder_frame_queue.v_frame_list.front()->best_effort_timestamp;

		// time_rendered_frame = av_rescale_q(default_present_time, v_redecoder_ctx_codec->time_base, timebase_ms);
		// FIXME: HACKY
		// Somehow I ended up with libx264, libx265, and libvpx-vp9 all incrementing by 34
		// which is good enough for me a this point
		time_rendered_frame = default_present_time/2;
		_WINDEBUGOUT("Present time: " << time_rendered_frame << std::endl);


		// ---------------------------------------------------------------------
		// Wipe the first packet in the redecoder queue
		// Here we cleanup the frame from do_redecode_stream
		// NOTE: q_renderer mutex
		q_renderer.lock();
		av_frame_free(&redecoder_frame_queue.v_frame_list.front());
		redecoder_frame_queue.v_frame_list.pop();
		q_renderer.unlock();

		cond_redecoder_frame_queue_too_large.notify_one();
		// ---------------------------------------------------------------------

		// Get the time point at which the frame should display
		std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds> intended_render_time = renderer_start_time + std::chrono::milliseconds(time_rendered_frame);

		std::chrono::milliseconds time_dif = intended_render_time - Conversions::get_current_time_point();
		// _WINDEBUGOUT("Sleep for" << time_dif.count() << std::endl);

		// renderer_start_time += std::chrono::microseconds((int)frame_duration * 1000);
		// std::chrono::microseconds time_dif = renderer_start_time - Conversions::get_current_time_point();
		// _WINDEBUGOUT("Time dif: " << time_dif.count() / 1000 << std::endl)
		// int64_t time_dif_ms_count = std::chrono::duration_cast<std::chrono::milliseconds>(time_dif).count();

		// ---------------------------------------------------------------------
		// NOTE: I couldn't implement the frame skip properly because the stupid time_base and timestamps
		// either weren't converting into ms properly (incrementing by 16 ms per frame at 60fps), or
		// when I did manage to get them to convert it was with av_packet_rescale_ts and that made
		// the decoder slow down for some reason???
		// ---------------------------------------------------------------------
		// Only delay if the frame shouldn't be rendered in the past... lol
		if (time_dif.count() > 0)
		{
			std::this_thread::sleep_until(intended_render_time);
		}
		// This is here so that frame skipping DOESN'T occur if things are literally at a stand
		// still, because if that's the case then nothing will play
		//else if (time_dif.count() < -(frame_duration * frame_skip_frame_count_tolerance * frame_skip_ignore_multiplier))
		//{
		//	// _WINDEBUGOUT("Too behind: not frame skipping" << std::endl);
		//	renderer_start_time += -time_dif;
		//	pts_frame_skip_offset += -time_dif.count();
		//}
		// If the rendered frame is more than the given amount of frame-durations behind,
		// we offset the renderer_start_time and the pts_frame_skip_offset so that future frames
		// will be compared with such offset. This also runs a thread which emulates the frame skip,
		// which simply removes all frames from the decode queue whose timestamp is in the past
		// relative to the last rendered frame
		else if (time_dif.count() < (-frame_duration * frame_skip_frame_count_tolerance))
		{
			renderer_start_time += -time_dif;
			pts_frame_skip_offset += -time_dif.count();
			// std::thread thr_skip_frames(&VideoStreamer::do_emulate_frame_skip, this, time_dif_ms_count);
			// thr_skip_frames.detach();
		}

		rendering_site->render_frame((const BYTE *)v_display_frame->data[0], input_frame_buffer_size);
	}

// This is done to break out of the nested while loops
EXIT_do_render_video:
	renderer_still_running = false;
	return;
}

void VideoStreamer::do_redecode_stream()
{
	int ret;

	int frame_decode_finished;
	int frame_num = 0;
	while (!abruptly_end_all)
	{
		// ---------------------------------------------------------------------
		// If output queue is too large, wait
		// NOTE: q_renderer mutex
		std::unique_lock<std::mutex> q_renderer(mutex_render_frame_queue);
		while (redecoder_frame_queue.v_frame_list.size() > redecoder_frame_queue.max_frames)
		{
			// If we should end, end.
			// This allows abruptly exiting even if the frame queue is full
			if (abruptly_end_all)
			{
				goto EXIT_do_decode_emulated_stream;
			}
			// _WINDEBUGOUT("do_redecode_stream: output full\n");
			cond_redecoder_frame_queue_too_large.wait(q_renderer);
		}
		q_renderer.unlock();

		// ---------------------------------------------------------------------
		// If no input packets, wait
		// NOTE: q_encoder mutex
		std::unique_lock<std::mutex> q_encoder(mutex_encoder_frame_queue);
		while (encoder_output_packet_queue.v_pkt_list.empty())
		{
			if (abruptly_end_all)
			{
				goto EXIT_do_decode_emulated_stream;
			}
			// _WINDEBUGOUT("do_redecode_stream: no input");
			cond_encoder_packet_queue_too_small.wait(q_encoder);
		}
		// q_encoder.unlock();
		v_redecoded_pkt = av_packet_alloc();

		// q_encoder.lock();
		av_packet_move_ref(v_redecoded_pkt, encoder_output_packet_queue.v_pkt_list.front());
		av_packet_free(&encoder_output_packet_queue.v_pkt_list.front());
		encoder_output_packet_queue.v_pkt_list.pop();
		q_encoder.unlock();
		// ---------------------------------------------------------------------
		cond_encoder_packet_queue_too_large.notify_one();

		av_packet_rescale_ts(v_redecoded_pkt, v_encoder_ctx_codec->time_base, timebase_ms);

		redecoder_ret = avcodec_send_packet(v_redecoder_ctx_codec, v_redecoded_pkt);
		av_packet_free(&v_redecoded_pkt);
		while (redecoder_ret >= 0)
		{
			v_redecoder_frame = av_frame_alloc();
			redecoder_ret = avcodec_receive_frame(v_redecoder_ctx_codec, v_redecoder_frame);

			// Try again
			if (redecoder_ret == AVERROR(EAGAIN) || redecoder_ret == AVERROR_EOF)
			{
				continue;
			}
			// else if (encoder_ret == AVERROR(AVERROR_EOF))
			// {
			// 	continue;
			// }
			else if (redecoder_ret < 0)
			{
				debug_ffmpeg_error(redecoder_ret);
				continue;
			}

			// _WINDEBUGOUT("Redecoding\n");

			// _WINDEBUGOUT("Stamp TWO: " << v_redecoder_frame->best_effort_timestamp << std::endl);

			AVFrame *enqueue_placeholder = av_frame_alloc();
			av_frame_move_ref(enqueue_placeholder, v_redecoder_frame);


			// -----------------------------------------------------------------
			// Add the re-decoded frame to the redecoder_frame_queue
			// NOTE: q_renderer mutex
			q_renderer.lock();
			// We add the frame skip PTS offset here
			// Normally this is 0, but if the encoder lags behind enough, this gets offset
			// enqueue_placeholder->pts = enqueue_placeholder->pts + pts_frame_skip_offset;
			// enqueue_placeholder->pts = enqueue_placeholder->best_effort_timestamp + pts_frame_skip_offset;
			enqueue_placeholder->pts = enqueue_placeholder->best_effort_timestamp;
			redecoder_frame_queue.v_frame_list.push(enqueue_placeholder);
			q_renderer.unlock();

			cond_redecoder_frame_queue_too_small.notify_one();
			// -----------------------------------------------------------------
		}
		// There's no cleanup to do here cause all this is doing is receiving
		// frames and then passing them into the next step
	}
EXIT_do_decode_emulated_stream:
	// Notify the renderer so that it can exit
	redecoder_still_running = false;
	cond_redecoder_frame_queue_too_small.notify_one();
	return;
}

void VideoStreamer::init_redecode_stream()
{
	int ret;

	redecoder_still_running = true;
	// -------------------------------------------------------------------------
	// Allocate context, find the decoder, then copy the context from
	// the encoder to the renderer context
	// -------------------------------------------------------------------------
	AVCodecParameters *parameters = avcodec_parameters_alloc();
	v_redecoder_codec = avcodec_find_decoder(v_encoder_ctx_codec->codec_id);
	v_redecoder_ctx_codec = avcodec_alloc_context3(v_redecoder_codec);

	avcodec_parameters_from_context(parameters, v_encoder_ctx_codec);
	avcodec_parameters_to_context(v_redecoder_ctx_codec, parameters);

	avcodec_parameters_free(&parameters);

	v_redecoder_ctx_codec->time_base = v_encoder_ctx_codec->time_base;

	ret = avcodec_open2(v_redecoder_ctx_codec, v_redecoder_codec, NULL);
	if (ret < 0)
	{
		debug_ffmpeg_error(ret);
		return;
	}

	av_opt_set_int(v_redecoder_ctx_codec, "refcounted_frames", 1, 0);

	// -------------------------------------------------------------------------
	// Allocate the frame so that the buffer can be created
	// -------------------------------------------------------------------------
	v_redecoder_frame = av_frame_alloc();

	v_redecoder_frame->format = v_redecoder_ctx_codec->pix_fmt;
	v_redecoder_frame->width = v_redecoder_ctx_codec->width;
	v_redecoder_frame->height = v_redecoder_ctx_codec->height;

	sws_yuv_from_redecoder = sws_getContext(v_redecoder_ctx_codec->width,
											v_redecoder_ctx_codec->height,
											v_redecoder_ctx_codec->pix_fmt,
											v_redecoder_ctx_codec->width,
											v_redecoder_ctx_codec->height,
											AV_PIX_FMT_YUV420P,
											SWS_BILINEAR,
											NULL,
											NULL,
											NULL);
}

void VideoStreamer::do_encode_input_file()
{
	while (!abruptly_end_all)
	{
		// Skip if user paused video. This also sleeps so that this loop
		// doesn't spin for no reason
		if (playback_paused_by_user)
		{
			Sleep(50);
			continue;
		}

		// ---------------------------------------------------------------------
		// If output queue is too large, wait
		// NOTE: q_encoder mutex
		std::unique_lock<std::mutex> q_encoder(mutex_encoder_frame_queue);
		while (encoder_output_packet_queue.v_pkt_list.size() > encoder_output_packet_queue.max_packets)
		{
			if (abruptly_end_all)
			{
				goto EXIT_do_encode_input_file;
			}
			// _WINDEBUGOUT("do_encode_input_file: output too large\n");
			cond_encoder_packet_queue_too_large.wait(q_encoder);
		}
		q_encoder.unlock();
		// ---------------------------------------------------------------------

		// ---------------------------------------------------------------------
		// If no source input, wait
		// NOTE: q_source mutex
		std::unique_lock<std::mutex> q_source(mutex_source_frame_queue);
		while (source_frame_queue.v_frame_list.empty())
		{
			// If the first decoder finished and we've no more frames, end.
			// Also allow abrupt ending
			if (!decoder_still_running || abruptly_end_all)
			{
				goto EXIT_do_encode_input_file;
			}
			// _WINDEBUGOUT("do_encode_input_file: no input\n");
			cond_source_frame_queue_too_small.wait(q_source);
		}
		// q_source.unlock();

		// Get data from the front of the source_frame_queue and pop the front
		// q_source.lock();
		av_frame_move_ref(v_encoder_frame, source_frame_queue.v_frame_list.front());
		av_frame_free(&source_frame_queue.v_frame_list.front());
		source_frame_queue.v_frame_list.pop();
		q_source.unlock();

		cond_source_frame_queue_too_large.notify_one();
		// ---------------------------------------------------------------------

		// ---------------------------------------------------------------------
		// Send frame to encoder
		// ---------------------------------------------------------------------
		encoder_ret = avcodec_send_frame(v_encoder_ctx_codec, v_encoder_frame);

		if (encoder_ret < 0)
		{
			debug_ffmpeg_error(encoder_ret);
			return;
		}

		while (encoder_ret >= 0)
		{
			v_encoder_pkt = av_packet_alloc();
			// -----------------------------------------------------------------
			// Get packet from encoder
			// -----------------------------------------------------------------
			encoder_ret = avcodec_receive_packet(v_encoder_ctx_codec, v_encoder_pkt);

			// Try again here
			if (encoder_ret == AVERROR(EAGAIN) || encoder_ret == AVERROR_EOF)
			{
				// _WINDEBUGOUT("___ENCODE___SKIP___" << std::endl);
				continue;
			}
			else if (encoder_ret < 0)
			{
				// _WINDEBUGOUT("___ENCODE___SKIP___" << std::endl);
				debug_ffmpeg_error(encoder_ret);
				continue;
			}
			// _WINDEBUGOUT("Encoding\n");

			AVPacket *new_decoded_pkt = av_packet_alloc();
			av_packet_move_ref(new_decoded_pkt, v_encoder_pkt);

			q_encoder.lock();
			encoder_output_packet_queue.v_pkt_list.push(new_decoded_pkt);
			q_encoder.unlock();
			cond_encoder_packet_queue_too_small.notify_one();

			av_packet_free(&v_encoder_pkt);
		}
		// Cleanup per-loop
		// The encoder pkt isn't wiped here because it's passed into the next step
		av_frame_unref(v_encoder_frame);
	}
EXIT_do_encode_input_file:
	// Notify the re-decoder so that it can exit
	encoder_still_running = false;
	cond_redecoder_frame_queue_too_large.notify_one();
	return;
}

void VideoStreamer::init_encode_input_file()
{
	encoder_still_running = true;

	v_encoder_codec = avcodec_find_encoder_by_name(desired_encoder_name.c_str());

	if (!v_encoder_codec)
	{
		_WINDEBUGOUT("Encoder codec not found" << std::endl);
		return;
	}

	v_encoder_ctx_codec = avcodec_alloc_context3(v_encoder_codec);

	if (!v_encoder_ctx_codec)
	{
		_WINDEBUGOUT("Encoder codec can't allocate" << std::endl);
		return;
	}

	// -------------------------------------------------------------------------
	// Set codec options
	// Doing CBR for all of them because that's what live streams typically are
	// -------------------------------------------------------------------------

	// NOTE: Without a descriptor like "B" or "M", FFMPEG defaults to Bits

	// https://trac.ffmpeg.org/wiki/Encode/H.264
	if (v_encoder_codec->id == AV_CODEC_ID_H264)
	{
		// "CBR" stuff
		// av_opt_set(v_encoder_ctx_codec, "x264-params", "nal-hrd=cbr", AV_OPT_SEARCH_CHILDREN);
		av_opt_set_int(v_encoder_ctx_codec, "b", Globals::configured_encoder_bitrate, AV_OPT_SEARCH_CHILDREN);
		av_opt_set_int(v_encoder_ctx_codec, "minrate", Globals::configured_encoder_bitrate, AV_OPT_SEARCH_CHILDREN);
		av_opt_set_int(v_encoder_ctx_codec, "maxrate", Globals::configured_encoder_bitrate, AV_OPT_SEARCH_CHILDREN);
		av_opt_set_int(v_encoder_ctx_codec, "bufsize", Globals::configured_encoder_bitrate, AV_OPT_SEARCH_CHILDREN);

		if (desired_quality == "quality-1")
		{
			av_opt_set(v_encoder_ctx_codec, "preset", "superfast", AV_OPT_SEARCH_CHILDREN);
		}
		else if (desired_quality == "quality-2")
		{
			av_opt_set(v_encoder_ctx_codec, "preset", "faster", AV_OPT_SEARCH_CHILDREN);
		}
		else if (desired_quality == "quality-3")
		{
			av_opt_set(v_encoder_ctx_codec, "preset", "medium", AV_OPT_SEARCH_CHILDREN);
		}
		else if (desired_quality == "quality-4")
		{
			av_opt_set(v_encoder_ctx_codec, "preset", "slow", AV_OPT_SEARCH_CHILDREN);
		}
	}
	// https://trac.ffmpeg.org/wiki/Encode/H.265
	else if (v_encoder_codec->id == AV_CODEC_ID_H265)
	{
		// "CBR" stuff
		// av_opt_set(v_encoder_ctx_codec, "x264-params", "nal-hrd=cbr", AV_OPT_SEARCH_CHILDREN);
		av_opt_set_int(v_encoder_ctx_codec, "b", Globals::configured_encoder_bitrate, AV_OPT_SEARCH_CHILDREN);
		av_opt_set_int(v_encoder_ctx_codec, "minrate", Globals::configured_encoder_bitrate, AV_OPT_SEARCH_CHILDREN);
		av_opt_set_int(v_encoder_ctx_codec, "maxrate", Globals::configured_encoder_bitrate, AV_OPT_SEARCH_CHILDREN);
		av_opt_set_int(v_encoder_ctx_codec, "bufsize", Globals::configured_encoder_bitrate, AV_OPT_SEARCH_CHILDREN);

		if (desired_quality == "quality-1")
		{
			av_opt_set(v_encoder_ctx_codec, "preset", "ultrafast", AV_OPT_SEARCH_CHILDREN);
		}
		else if (desired_quality == "quality-2")
		{
			av_opt_set(v_encoder_ctx_codec, "preset", "superfast", AV_OPT_SEARCH_CHILDREN);
		}
		else if (desired_quality == "quality-3")
		{
			av_opt_set(v_encoder_ctx_codec, "preset", "veryfast", AV_OPT_SEARCH_CHILDREN);
		}
		else if (desired_quality == "quality-4")
		{
			av_opt_set(v_encoder_ctx_codec, "preset", "faster", AV_OPT_SEARCH_CHILDREN);
		}
	}
	// https://trac.ffmpeg.org/wiki/Encode/VP9
	else if (v_encoder_codec->id == AV_CODEC_ID_VP9)
	{
		// ---------------------------------------------------------------------
		// NOTE: the documentation seems to be a lie, and when using deadline realtime
		// the cpu-used range is -8 to 8.
		// Then, if you ignore the minus, the "bigger the number", the faster the encoder
		// aka -8 seems to be the same as 8, and -5 seems to be the same as 5.
		// That means that, for the same bitrate, cpu-used at 8 will look worse than 1
		// ---------------------------------------------------------------------
		int ret;
		ret = av_opt_set_int(v_encoder_ctx_codec->priv_data, "b", Globals::configured_encoder_bitrate, 0);
		debug_ffmpeg_error(ret);

		ret = av_opt_set(v_encoder_ctx_codec, "deadline", "realtime", AV_OPT_SEARCH_CHILDREN);
		debug_ffmpeg_error(ret);

		if (desired_quality == "quality-1")
		{
			ret = av_opt_set_int(v_encoder_ctx_codec, "cpu-used", 8, AV_OPT_SEARCH_CHILDREN);
			debug_ffmpeg_error(ret);
		}
		else if (desired_quality == "quality-2")
		{
			ret = av_opt_set_int(v_encoder_ctx_codec, "cpu-used", 7, AV_OPT_SEARCH_CHILDREN);
			debug_ffmpeg_error(ret);
		}
		else if (desired_quality == "quality-3")
		{
			ret = av_opt_set_int(v_encoder_ctx_codec, "cpu-used", 6, AV_OPT_SEARCH_CHILDREN);
			debug_ffmpeg_error(ret);
		}
		else if (desired_quality == "quality-4")
		{
			ret = av_opt_set_int(v_encoder_ctx_codec, "cpu-used", 5, AV_OPT_SEARCH_CHILDREN);
			debug_ffmpeg_error(ret);
		}
		// Some sort of libvp9 option that should enable/improve multithreading
		ret = av_opt_set_int(v_encoder_ctx_codec, "row-mt", 1, AV_OPT_SEARCH_CHILDREN);
		debug_ffmpeg_error(ret);
	}
	// https://trac.ffmpeg.org/wiki/Encode/AV1
	else if (v_encoder_codec->id == AV_CODEC_ID_AV1)
	{
		// NOTE: This is an "average" bitrate mode
		av_opt_set_int(v_encoder_ctx_codec->priv_data, "b", Globals::configured_encoder_bitrate, 0);
		av_opt_set_int(v_encoder_ctx_codec->priv_data, "cpu-used", 8, 0);
		// Should improve MT perf
		av_opt_set_int(v_encoder_ctx_codec->priv_data, "row-mt", 1, 0);
	}

	// v_encoder_ctx_codec->bit_rate = Globals::user_upload_speed;
	v_encoder_ctx_codec->bit_rate = Globals::configured_encoder_bitrate;
	v_encoder_ctx_codec->rc_max_rate = Globals::configured_encoder_bitrate;
	v_encoder_ctx_codec->rc_min_rate = Globals::configured_encoder_bitrate;
	// v_encoder_ctx_codec->rc_buffer_size = Globals::configured_encoder_bitrate;
	v_encoder_ctx_codec->width = v_source_ctx_codec->width;
	v_encoder_ctx_codec->height = v_source_ctx_codec->height;
	v_encoder_ctx_codec->time_base = v_source_ctx_codec->time_base;
	v_encoder_ctx_codec->framerate = v_source_ctx_codec->framerate;
	v_encoder_ctx_codec->gop_size = v_source_ctx_codec->gop_size;
	v_encoder_ctx_codec->max_b_frames = 1;
	v_encoder_ctx_codec->pix_fmt = AV_PIX_FMT_YUV420P;

	av_opt_set_int(v_encoder_ctx_codec, "refcounted_frames", 1, 0);

	int asdf = avcodec_open2(v_encoder_ctx_codec, v_encoder_codec, NULL);
	if (asdf < 0)
	{
		_WINDEBUGOUT(__FILE__ << ">\n"
							  << __LINE__ << "\nCouldn't open codec\n\n"
							  << asdf);
		return;
	}

	v_encoder_frame = av_frame_alloc();

	if (!v_encoder_frame)
	{
		_WINDEBUGOUT("Couldn't allocate video frame" << std::endl);
		return;
	}

	v_encoder_frame->format = v_encoder_ctx_codec->pix_fmt;
	v_encoder_frame->width = v_encoder_ctx_codec->width;
	v_encoder_frame->height = v_encoder_ctx_codec->height;
}

void VideoStreamer::do_decode_input_file()
{
	while (av_read_frame(v_source_ctx_format, &v_source_pkt) >= 0 && !abruptly_end_all)
	{
		// ---------------------------------------------------------------------
		// If output queue is too large, wait
		// ---------------------------------------------------------------------
		std::unique_lock<std::mutex> q_source(mutex_source_frame_queue);
		while (source_frame_queue.v_frame_list.size() > source_frame_queue.max_frames)
		{
			if (abruptly_end_all)
			{
				goto EXIT_do_decode_input_file;
			}
			// _WINDEBUGOUT("do_decode_input_file: output too large\n");
			cond_source_frame_queue_too_large.wait(q_source);
		}
		q_source.unlock();

		if (v_source_pkt.stream_index == video_source_stream)
		{
			// -----------------------------------------------------------------
			// Send packet to decode
			// -----------------------------------------------------------------
			decoder_ret = avcodec_send_packet(v_source_ctx_codec, &v_source_pkt);

			if (decoder_ret < 0)
			{
				debug_ffmpeg_error(decoder_ret);
				return;
			}

			while (decoder_ret >= 0)
			{
				// -------------------------------------------------------------
				// Get the decoded frame
				// -------------------------------------------------------------
				v_source_frame = av_frame_alloc();
				decoder_ret = avcodec_receive_frame(v_source_ctx_codec, v_source_frame);

				if (decoder_ret == AVERROR(EAGAIN) || decoder_ret == AVERROR_EOF)
				{
					// Unref here cause the ref won't be moved
					av_frame_unref(v_source_frame);
					continue;
				}
				else if (decoder_ret < 0)
				{
					// Unref here cause the ref won't be moved
					av_frame_unref(v_source_frame);
					debug_ffmpeg_error(decoder_ret);
					continue;
				}

				// _WINDEBUGOUT("Decoding\n");

				// -------------------------------------------------------------
				// Add the decoded frame to the source_frame_queue
				AVFrame *enqueue_placeholder = av_frame_alloc();

				// _WINDEBUGOUT("Stamp ONE: " << v_source_frame->best_effort_timestamp << std::endl);

				// NOTE: q_source mutex
				q_source.lock();
				av_frame_move_ref(enqueue_placeholder, v_source_frame);
				source_frame_queue.v_frame_list.push(enqueue_placeholder);
				q_source.unlock();

				cond_source_frame_queue_too_small.notify_one();
				// -------------------------------------------------------------
			}
		}

		// Cleanup per-iteration
		// The frame is not wiped here because its reference is moved into the next step
		av_packet_unref(&v_source_pkt);
	}
	// Notify the encoder so that it can exit
	decoder_still_running = false;
	cond_source_frame_queue_too_small.notify_one();
	return;
EXIT_do_decode_input_file:
	decoder_still_running = false;
	cond_source_frame_queue_too_small.notify_one();
	return;
}

void VideoStreamer::init_decode_input_file()
{
	AVRational tbinput = {1, 600};
	timebase_set = tbinput;

	int ret;
	decoder_still_running = true;
	// ---------------------------------------------------------------------
	// SECTION Initial reading and setup
	// ---------------------------------------------------------------------

	// Register all formats and codecs; makes all codecs/muxers etc. supported
	// Can call them individually if want to

	_WINDEBUGOUT(__FILE__ << ">\n"
						  << __LINE__ << "\n: FILE DIR" << file_dir.c_str() << "\n\n");
	// Open video
	ret = (avformat_open_input(&v_source_ctx_format, file_dir.c_str(), NULL, NULL));
	if (ret != 0)
	{
		_WINDEBUGOUT(__FILE__ << ">\n"
							  << __LINE__ << "\n: avformat_open_input failed\n\n");
		debug_ffmpeg_error(ret);
		return;
	}

	// Retrieve stream information
	ret = avformat_find_stream_info(v_source_ctx_format, NULL);
	if (ret < 0)
	{
		_WINDEBUGOUT(__FILE__ << ">\n"
							  << __LINE__ << "\n: avformat_find_stream_info failed\n\n");
		debug_ffmpeg_error(ret);
		return;
	}

	// Find the first video stream
	video_source_stream = -1;
	for (int i = 0; i < v_source_ctx_format->nb_streams; i++)
		if (v_source_ctx_format->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			video_source_stream = i;
			// Get framerate guess value from numerator/denominator
			frame_rate = (double)v_source_ctx_format->streams[i]->r_frame_rate.num / (double)v_source_ctx_format->streams[i]->r_frame_rate.den;
			frame_duration = 1000.0 / frame_rate;
			break;
		}
	if (video_source_stream == -1)
	{
		_WINDEBUGOUT(__FILE__ << ">\n"
							  << __LINE__ << "\n: didn't find supported stream\n\n");
		return;
	}

	// Get a pointer to the codec context for the video stream
	v_source_ctx_codec_orig = v_source_ctx_format->streams[video_source_stream]->codec;
	// Find the decoder for the video stream
	v_source_codec = avcodec_find_decoder(v_source_ctx_codec_orig->codec_id);
	if (v_source_codec == NULL)
	{
		_WINDEBUGOUT(__FILE__ << ">\n"
							  << __LINE__ << "\n: Unsupported codec\n\n");
		return;
	}

	// ---------------------------------------------------------------------
	// !SECTION
	// ---------------------------------------------------------------------

	// ---------------------------------------------------------------------
	// SECTION  video context codec and sws context
	// ---------------------------------------------------------------------

	// Allocate codec context with matching codec
	v_source_ctx_codec = avcodec_alloc_context3(v_source_codec);

	// TODO: Fix deprecated
	if (avcodec_copy_context(v_source_ctx_codec, v_source_ctx_codec_orig) != 0)
	{
		_WINDEBUGOUT(__FILE__ << ">\n"
							  << __LINE__ << "\n: avcodec_copy_context failed\n\n");
		return;
	}

	v_source_ctx_codec->time_base = timebase_set;

	av_opt_set_int(v_source_ctx_codec, "refcounted_frames", 1, 0);
	// Open codec with provided context and codec
	if (avcodec_open2(v_source_ctx_codec, v_source_codec, NULL) < 0)
	{
		_WINDEBUGOUT(__FILE__ << ">\n"
							  << __LINE__ << "\n: Couldn't open codec\n\n");
		return;
	}

	// ---------------------------------------------------------------------
	// !SECTION
	// ---------------------------------------------------------------------

	// Get buffer size for (probably YUV) frames
	input_frame_buffer_size = av_image_get_buffer_size(v_source_ctx_codec->pix_fmt, v_source_ctx_codec->width, v_source_ctx_codec->height, 32);

	// Allocate video frame.
	v_source_frame = av_frame_alloc();
	if (v_source_frame == NULL)
	{
		_WINDEBUGOUT(__FILE__ << ">\n"
							  << __LINE__ << "\n: v_source_frame is null; could not allocate\n\n");
		return;
	}

	// Get the time base in ms
	AVRational tbms = {1, frame_duration};
	timebase_ms = tbms;
}

void VideoStreamer::initialise_video()
{
	frame_skip_frame_count_tolerance = 1;
	frame_skip_ignore_multiplier = 10;
	pts_frame_skip_offset = 0;
	should_emulate_frame_skip = false;
	renderer_still_running = true;

	av_register_all();
	avcodec_register_all();

	init_decode_input_file();
	init_encode_input_file();
	init_redecode_stream();

	// -------------------------------------------------------------------------
	// SECTION Decode -> encode -> decode -> render process
	// -------------------------------------------------------------------------
	std::thread thr_decode_input(&VideoStreamer::do_decode_input_file, this);
	std::thread thr_encode_input(&VideoStreamer::do_encode_input_file, this);
	std::thread thr_decode_emulated(&VideoStreamer::do_redecode_stream, this);
	std::thread thr_render_emulated(&VideoStreamer::do_render_emulated_stream, this);

	// When the renderer stops running, we notify all the potential conditions
	// which stuff could be waiting on. This is because they could, I think (?),
	// potentially wait on that condition but never be notified again.
	/*while (renderer_still_running) {
		Sleep(100);
	}

	cond_source_frame_queue_too_large.notify_one();
	cond_encoder_packet_queue_too_large.notify_one();
	cond_source_frame_queue_too_small.notify_one();
	cond_redecoder_frame_queue_too_large.notify_one();
	cond_redecoder_frame_queue_too_small.notify_one();*/

	thr_decode_input.join();
	thr_encode_input.join();
	thr_decode_emulated.join();
	thr_render_emulated.join();

	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------

	// -------------------------------------------------------------------------
	// SECTION Cleanup
	// at this point, all of the threads should've quit fine because all of the
	// queues are empty, but I don't trust myself so here this'll just clear
	// all of the queues
	// -------------------------------------------------------------------------
	while (!redecoder_frame_queue.v_frame_list.empty())
	{
		av_frame_free(&redecoder_frame_queue.v_frame_list.front());
		redecoder_frame_queue.v_frame_list.pop();
	}
	while (!source_frame_queue.v_frame_list.empty())
	{
		av_frame_free(&source_frame_queue.v_frame_list.front());
		source_frame_queue.v_frame_list.pop();
	}
	while (!encoder_output_packet_queue.v_pkt_list.empty())
	{
		av_packet_free(&encoder_output_packet_queue.v_pkt_list.front());
		encoder_output_packet_queue.v_pkt_list.pop();
	}

	// Close the video file
	avformat_close_input(&v_source_ctx_format);

	// Close the codec
	avcodec_close(v_source_ctx_codec);
	avcodec_close(v_source_ctx_codec_orig);
	avcodec_close(v_encoder_ctx_codec);
	avcodec_close(v_redecoder_ctx_codec);

	// Free frames
	av_free(v_display_frame);
	av_free(v_redecoder_frame);
	av_free(v_encoder_frame);
	av_free(v_source_frame);

	// Free the frame buffer used for the display
	av_buffer_unref(&v_display_frame_buffer);

	// Free contexts
	sws_freeContext(sws_yuv_from_redecoder);
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------
}

void VideoStreamer::start()
{
	// If the speedtest hasn't yet finished, we'll use the default
	// upload speed and mark the var here.
	if (!Globals::speedtest_upload_speed_read)
	{
		// ui_callback.call("Speedtest not yet finished, please wait");
		Globals::user_started_streamer_before_speed_read = true;
	}
	initialise_video();
	// std::thread initmainthread(&VideoStreamer::initialise_video, this);
	// initmainthread.detach();
	// initmainthread.join();
	//_WINDEBUGOUT("aaaaaaaaaa");
	// this;
}

void VideoStreamer::early_stop_all()
{
	// Make any next loops in the threads end
	abruptly_end_all = true;

	// Fake it till you make it -> tell the threads that they will no longer be getting anything
	decoder_still_running = false;
	encoder_still_running = false;
	redecoder_still_running = false;
	renderer_still_running = false;

	// Make any threads that were waiting refresh, which will cause them to find that they're
	// not supposed to keep running anymore
	cond_source_frame_queue_too_large.notify_one();
	cond_encoder_packet_queue_too_large.notify_one();
	cond_encoder_packet_queue_too_small.notify_one();
	cond_source_frame_queue_too_small.notify_one();
	cond_redecoder_frame_queue_too_large.notify_one();
	cond_redecoder_frame_queue_too_small.notify_one();
}