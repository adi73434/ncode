// This is an open source non-commercial project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#ifndef MY_GLOBALS_H
#define MY_GLOBALS_H

// #define _WINDEBUGOUT(s)							\
// {												\
// 	std::wostringstream os_;					\
// 	os_ << s;									\
// 	OutputDebugStringW(os_.str().c_str());		\
// }

#include "IncludeSciter.h"

#include "MainFrame.h"

#include <sciter/sciter-x.h>
#include <sciter/sciter-x-behavior.h>
#include <sciter/sciter-x-threads.h>
#include <sciter/sciter-x-video-api.h>

#include <thread>
#include <mutex>
#include <atomic>

#include "VideoDecoder.h"

/**
 * @brief This stores certain global variables, either because they are needed throughout or because, for some reason,
 * I thought this would aid legibility.
 * 
 */
class Globals
{
public:
	static HWND mainHwnd;
	static sciter::om::hasset<MainFrame> mainPwin;
	static int myglobalint;

	// static VideoDecoder *global_video_decoder;

	/**
	 * @brief Use this to hold the string that points to the path to the videos. This is done to make it
	 * slightly easier to change. This value is set by set_global_video_file_concat_values in HandleEvents
	 * by a call from TiScript.
	 * 
	 */
	static std::string video_file_dir;
	/**
	 * @brief This stores the encoded affix used for the video file path.
	 * 
	 * @see video_file_dir
	 */
	static std::string video_file_encoded_affix;
	/**
	 * @brief This stores the file extension used for the video files.
	 * 
	 * @see video_file_dir
	 */
	static std::string video_file_extension;

	/**
	 * @brief 0-indexed. This points to the user comment location in the third layer of the 3D array.
	 *
	 * This is initialised by main.tis by calling HandleEvents::set_3d_video_and_stream_array_details
	 * 
	 */
	static int videos_content_comment_index;

	/**
	 * @brief 1-indexed. This notes the number of video files within the video 3D array and represents
	 * the number of items in the first layer of the 3D array.
	 * 
	 * This is initialised by main.tis by calling HandleEvents::set_3d_video_and_stream_array_details
	 * 
	 */
	static int videos_file_count;

	/**
	 * @brief 1-indexed. This notes the number of codec varaints each video file will have within the
	 * second layer of the video 3D array.
	 * 
	 * This is initialised by main.tis by calling HandleEvents::set_3d_video_and_stream_array_details
	 * 
	 */
	static int videos_codec_varaints;

	/**
	 * @brief 1-indexed. This notes the number of fields each video-file combo will have in the third layer
	 * of the video 3D array. This, alongside videos_content_comment_index, will be used to get the ratings.
	 * 
	 * This is initialised by main.tis by calling HandleEvents::set_3d_video_and_stream_array_details
	 * 
	 */
	static int videos_content_count;

	/**
	 * @see videos_content_comment_index
	 * 
	 */
	static int streams_content_comment_index;

	/**
	 * @see videos_file_count
	 * 
	 */
	static int streams_file_count;

	/**
	 * @see videos_codec_varaints
	 * 
	 */
	static int streams_codec_varaints;

	/**
	 * @see videos_content_count
	 * 
	 */
	static int streams_content_count;

	/**
	 * @brief This stores the original videoFilesAndDetails array from main.tis, which was
	 * converted into videoFilesAndDetails2D and then into a 2D vector with convert_2d_ui_array_to_2d_vector.
	 * This is used for getting the video file details for deletion.
	 * 
	 * @warning This does NOT store the filled data, and it shalll never be used to reference
	 * user results; the user results are kept in TiScript, and they are only ever fetched
	 * when they are necessary via HandleEvents::convert_3d_ui_array_to_json.
	 * 
	 */
	static std::vector<std::vector<std::string>> vector_2d_original_video_details;

	/**
	 * @brief This is a user-controllable variable which will modify the quality setting of the
	 * ffmpeg encoder. If the user changes the quality setting in the UI, live_encoder_quality_setting_changed
	 * will be set to tryue and the video player will be refreshed, starting the encode/decode from the
	 * beginning and with the new setting.
	 * 
	 */
	static std::atomic<int> live_encoder_quality_setting;

	/**
	 * @brief This is just a bool to keep track of whether or not the user changed the quality setting.
	 * If this is set to true, the encoder will refresh and apply the updated live_encoder_quality_setting.
	 * 
	 */
	static std::atomic<bool> live_encoder_quality_setting_changed;

	/**
	 * @brief This keeps track of whether or not the speedtest result was read
	 * into the user_upload_speed. This will block livestreams from playing
	 * until the result was successfully read.
	 * 
	 */
	static std::atomic<bool> speedtest_upload_speed_read;

	/**
	 * @brief This keeps track of whether or not the user started a VideoStreamer before their upload
	 * speed was read. If this happens, the VideoStreamers will all use the default configured_encoder_bitrate.
	 * 
	 */
	static std::atomic<bool> user_started_streamer_before_speed_read;

	/**
	 * @brief This is the cap, in bytes, of the encoder. If the user's upload speed is larger than this,
	 * we use this value.
	 * 
	 */
	static std::atomic<long long> max_encoder_bitrate_cap;

	/**
	 * @brief Store the encoder bitrate that is being used here.
	 * 
	 */
	static std::atomic<long long> configured_encoder_bitrate;

	/**
	 * @brief Store the Sciter video rendering site as a global element so that it can be accessed
	 * anywhere throughout the program. This is done so that arbitrary events from the UI can
	 * create a VideoDecoder object with different video files and settings.
	 * 
	 */
	static sciter::video_destination *global_video_destination;

	/**
	 * @brief Store the API address which is used in cpp-httplib for connecting to the PHP
	 * backend. This might be used elsewhere, I don't know.
	 * 
	 * @warning Do not use "http://" in the api_address if the api_port is being used, as for some
	 * reason setting a port as a secondary value in the constructor of cpp-httplib gives an error
	 * if "http://" is present.
	 * 
	 */
	static std::string api_address;

	/**
	 * @brief This just stores the port for the backend.
	 * 
	 * @see Globals::api_address
	 * 
	 */
	static int api_port;

	/**
	 * @brief This bool matters in the context of the method that created a VideoDecoder object
	 * as it is monitored, ensuring the decoder is only kept alive while this is true. This is done
	 * so that any arbitrary method can disable the decoder.
	 * 
	 */
	static std::atomic<bool> keep_decoder_alive;
	/**
	 * @brief Mutex used for keep_decoder_alive.
	 * 
	 * @note I don't know if this is actually necessary.
	 * 
	 */
	static std::mutex Globals::keep_decoder_alive_mtx;
	/**
	 * @brief This bool is a crude way to monitor that the decoder is still running. This is done to block
	 * exiting.
	 * 
	 */
	static std::atomic<bool> decoder_is_running;
};

#endif