#pragma once

// Threading
#include <atomic>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>

// Sciter
#include <sciter/sciter-x.h>
#include <sciter/sciter-x-behavior.h>
#include <sciter/sciter-x-threads.h>
#include <sciter/sciter-x-video-api.h>

// ffmpeg/Libav
extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <libavutil/opt.h>
}

#include "Conversions.h"

/**
 * @brief This is the VideoDecoder class, and its current implementation decodes the given file from disk.
 * 
 */
class VideoDecoder
{

public:
	/**
	 * @brief Construct a new Video Decoder object
	 * 
	 * @param param_file_dir Value given from the VideoRenderingSite used for the
	 * custom-video-generator behaviour.
	 * @param param_dst This is the sciter rendering site used for rendering frames.
	 * @param param_frame_queue_max_frames This is the specified number of maximum frames
	 * that can be in the frame_queue.
	 */
	VideoDecoder(std::string param_file_dir,
				 sciter::video_destination *param_dst,
				 int param_frame_queue_max_frames);
	~VideoDecoder();

	/**
	 * @brief Frame queue which uses an std::queue of pointers to AVFrames.
	 * The decode thread will add frames to the back of the queue; meanwhile,
	 * the render thread will take from the front of the queue and pop it.
	 * 
	 * @note Use a mutex for accessing this on multiple threads
	 * 
	 */
	struct FrameQueue
	{
		std::queue<AVFrame *> v_frame_list;
		/**
		 * @brief defines the maximum number of frames that can be
		 * * placed into the queue. This is done to limit memory usage.
		 * 
		 */
		int max_frames;
	};

	/**
	 * @brief This takes the first frame from frame_queue and renders it to the screen using
	 * Sciter's rendering site.
	 * 
	 * @details This does the following:
	 * - Creates a new AVFrame called display_frame;
	 * - Creates a buffer;
	 * - Allocates the display_frame AVFrame;
	 * - Allocates memory to the buffer to fit an RGB24 frame based on the video context's width
	 * and height;
	 * - Fills display_frame to set up the pointers/linesizes or whatever;
	 * - Starts streaming to the sciter rendering_site;
	 * 
	 * Then, while not specified to abruptly end:
	 * - Sleeps (REPLACE TO SOME SORT OF TIMESTAMP SYNC);
	 * - Creates a mutex lock;
	 * - Quits if queue is empty and it either renderer_keep_waiting_for_decoder is set to false
	 * or abruptly_end_all is set to true;
	 * - If not quitting, waits for a signal on cond_frame_queue if frame_queue was empty;
	 * - Runs sws_scale on the first frame in frame_queue, this essentially gives display_frame all
	 * the contextual info like height, and stores the frame data in it.
	 * - Frees the first frame in the queue and pops (removes) it from the queue - this is safe as display_frame
	 * now holds the frame that will be displayed;
	 * - Notifies cond_frame_queue_too_large so that do_decode_video can continue if frame_queue exceeded the max
	 * and it was waiting;
	 * - Renders the frame
	 * 
	 * EXIT_do_render_video:
	 * - Frees the display buffer
	 * - Frees display_frame
	 * - Notifies cond_frame_queue_too_large so if that was stuck for some reason, or whatever, it can also finish.
	 * 
	 * @warning If abruptly_end_all is used, frames will amost certainly still exist in the frame_queue. This is cleared
	 * at the end of initialise_video().
	 * 
	 */
	void do_render_video();

	/**
	 * @brief This function handles the decoding of the video after av_read_frame has been called.
	 * The only reason this function is separate is to clean up the initialise_video function.
	 * 
	 * @details This function locks the mtx_frame_queue and spins if the amount of frames in the frame_queue
	 * is larger than the maximum specified. This is done to not use up GBs upon GBs of memory.
	 * 
	 * Once there is some space in the frame_queue:
	 * - it decodes the video to the existing v_frame;
	 * - makes and allocates a new AVFrame called enqueue_frame;
	 * - it locks mtx_frame_queue again as frame_queue needs to be accessed;
	 * - pushes enqueue_frame to the back of frame_queue;
	 * - moves the decoded v_frame to the position where enqueue_frame is on the frame_queue;
	 * - it unlocks mtx_frame_queue;
	 * - notifies cond_frame_queue as do_render_video will be waiting on cond_frame_queue while
	 * mtx_frame_queue is locked;
	 */
	void do_decode_video();

	/**
	 * 
	 * @brief Initialise the video, launch a render thread, and start decoding.
	 * 
	 * @details This does the following:
	 * - initialises the video by registering all potential audio/video codecs;
	 * - opens the file;
	 * - finds the stream info;
	 * - finds the correct AVMEDIA_TYPE_VIDEO stream (only video is being handled);
	 * - finds the decoder;
	 * - allocates the context of the decoder to AVCodecContext v_ctx_codec;
	 * - sets refcounted_frames to 1 so that they can be moved;
	 * - sets sws_context to be able to use sws_scale;
	 * - calculates the rgb24 buffer size;
	 * - allocates an AVFrame
	 * - spawns a do_render_video;
	 * - reads the video frame;
	 * - decodes the video frame;
	 * - and finally attaches do_render_video and cleans up libav stuff after itself.
	 * 
	 * @note av_packet_unref() is called here, after do_decode_video() call, for legibility
	 * @note as refcounted_frames 1 is being used, do_render_video holds the responsibility
	 * of freeing/unreferencing the frame
	 * 
	 * @see https://ffmpeg.org/doxygen/trunk/group__lavc__decoding.html#ga3ac51525b7ad8bca4ced9f3446e96532 in regards to refcounted_frames 1
	 * 
	 */
	void initialise_video();

	/**
	 * @brief Start the VideoDecoder by calling initialise_video with "this" context so that it runs
	 * in the same context (?) as what the constructor did.
	 * 
	 * @note The thread.join here was used when the VideoDecoder object was called
	 * and when cdr.start was ran normally; however, the implementation now uses
	 * an std::thread call to &VideoDecoder::start with a reference to the VideoDecoder
	 * object.
	 * 
	 */
	void start();

	/**
	 * @brief This is used to stop the decoder and renderer and completely
	 * wipe the queue by stopping both, then notifying do_render_video
	 * in case it was waiting.
	 * 
	 */
	void early_stop_all();

	/**
	 * @brief Sciter's rendering site.
	 * This is where frame data is passed to to render.
	 * 
	 */
	sciter::om::hasset<sciter::video_destination> rendering_site;

	FrameQueue frame_queue;

	std::atomic<bool> playback_paused_by_user;
	/**
	 * @brief Bool to signal if do_render_video should keep trying
	 * to wait if the queue is empty.
	 * 
	 * Usage: set to false once do_decode_video() is finished
	 * 
	 */
	std::atomic<bool> renderer_keep_waiting_for_decoder;

	/**
	 * @brief This is used to signal the decoder to stop, and 
	 * to signal do_render_video() to stop trying to render frames,
	 * regardless of whether the queue is empty or not.
	 * 
	 */
	std::atomic<bool> abruptly_end_all;
	/**
	 * @brief Mutex to inhibit race conditions between do_decode_video()
	 * and do_render_video() as they run on separate threads.
	 * 
	 */
	// std::mutex mtx_frame_queue;
	/**
	 * @brief Condition that do_render_video() waits and spins on to bring the thread
	 * back to life when do_decode_video() holds mtx_frame_queue.
	 * 
	 */
	std::condition_variable cond_frame_queue;
	/**
	 * @brief Condition that do_decode_video() waits and spins on if
	 * the current number of frames in the frame_queue exceeds that specified.
	 * do_render_video() notifies this condition to bring do_decode_video() back
	 * to life.
	 * 
	 */
	std::condition_variable cond_frame_queue_too_large;

	/**
	 * @brief Format context that is used to open/configure the codec and is used
	 * by av_read_frame. 
	 * 
	 * @note See the libavformat documentation.
	 * 
	 */
	AVFormatContext *v_ctx_format;
	/**
	 * @brief Used to store the codec info from the original input stream
	 * after the correct video_stream has been identified.
	 * 
	 */
	AVCodecContext *v_ctx_codec_orig;
	/**
	 * @brief Used for the codec context to configure sws_ctx and a bunch of
	 * other things. It is also used in do_decode_video().
	 * 
	 * @note See the libavcodec documentation.
	 * 
	 */
	AVCodecContext *v_ctx_codec;

	/**
	 * @brief Holds the decoder from v_ctx_orig.
	 * 
	 */
	AVCodec *v_codec;

	/**
	 * @brief AVFrame used explicitly for the decoding phase.
	 * Once this ends up in the frame_queue, do_render_video() creates a new frame
	 * accordingly so it can hold onto it after popping the frame_queue.
	 * 
	 */
	AVFrame *v_frame;

	/**
	 * @brief Packet used for decoding.
	 * 
	 */
	AVPacket v_pkt;

	/**
	 * @brief Used to indicate that decoding of a frame has finished.
	 * 
	 */
	int frame_decode_finished;

	/**
	 * @brief Context created by sws_getContext so that the context
	 * can be apploied to the display_frame in do_render_video()
	 * 
	 */
	struct SwsContext *sws_ctx;

	/**
	 * @brief redundant.
	 * 
	 * @todo don't use int i here and instead declare it in the one for loop
	 * that uses it
	 * 
	 * 
	 */
	int i;
	/**
	 * @brief Identifier used to point to the correct video stream as a file can
	 * have multiple audio and video streams.
	 * 
	 */
	int video_stream;
	/**
	 * @brief Mostly used for debugging, this stores the calculated
	 * value of what the FPS should be.
	 * 
	 */
	double frame_rate;
	/**
	 * @brief Mostly used for debugging, this stores the calculated
	 * value of what the frame time should be.
	 * 
	 */
	double frame_duration;
	/**
	 * @brief Used for debugging, this increments with every frame_decode_finished
	 * to calculate the average time taken for the decoding phase.
	 * 
	 */
	int v_frame_index;

	/**
	 * @brief Directory of the video input file.
	 * 
	 */
	std::string file_dir;

	/**
	 * @brief Frame buffer size of RGB24 images according to the
	 * video input width/height. As the rendering_size->start_streaming has
	 * been set to RGB24, the frame buffer is also done in RGB24.
	 * 
	 */
	int rgb24_frame_buffer_size;

	/**
	 * @brief Mostly used for debugging, this is the timestamp at which
	 * a frame has started being decoded.
	 * 
	 */
	std::chrono::system_clock::time_point decode_clock_start;
	/**
	 * @brief Mostly used for debugging, this is the total elapsed ms
	 * of the decoding process.
	 * 
	 */
	long long decoding_elapsed_total;
	/**
	 * @brief Mostly used for debugging, this is the calculated average
	 * of all decoded frames
	 * 
	 */
	int decode_time_avg;

private:
};