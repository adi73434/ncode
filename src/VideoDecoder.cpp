
#include "VideoDecoder.h"

#include "Globals.h"
#include "stdafx.h"

#include <Windows.h>
#include <iostream>
#include <sstream>
#include <string>

#include <array>
#include <time.h>
#include <chrono>

#define _WINDEBUGOUTAAAAAAAAAAAAAAAAAAA(s)     \
	{                                          \
		std::wostringstream os_;               \
		os_ << s;                              \
		OutputDebugStringW(os_.str().c_str()); \
	}

#define _WINDEBUGOUT(S) \
	{                   \
	}

std::mutex mtx_frame_queue;

VideoDecoder::VideoDecoder(std::string param_file_dir,
						   sciter::video_destination *param_dst,
						   int param_frame_queue_max_frames)
{
	this->rendering_site = param_dst;
	this->frame_queue.max_frames = param_frame_queue_max_frames;
	this->renderer_keep_waiting_for_decoder = true;
	this->abruptly_end_all = false;
	this->v_ctx_format = NULL;
	this->v_ctx_codec_orig = NULL;
	this->v_ctx_codec = NULL;
	this->v_codec = NULL;
	this->v_frame = NULL;
	this->sws_ctx = NULL;
	this->v_frame_index = 0;
	this->decoding_elapsed_total = 0;
	this->file_dir = param_file_dir;
};

VideoDecoder::~VideoDecoder()
{
}

void VideoDecoder::do_render_video()
{
	// Display frame will store the frame to-display, separate from the frame queue
	AVFrame *display_frame = NULL;
	bool is_first_frame = true;
	double synctime;
	long long last_rendered_time = 0;
	long long current_epoch;
	int actual_delay;

	// Initialising display_frame the same way it is done in the decode thread
	uint8_t *v_frame_display_buffer = NULL;
	display_frame = av_frame_alloc();
	v_frame_display_buffer = (uint8_t *)av_malloc(rgb24_frame_buffer_size * sizeof(uint8_t));
	avpicture_fill((AVPicture *)display_frame, v_frame_display_buffer, AV_PIX_FMT_RGB24,
				   v_ctx_codec->width, v_ctx_codec->height);

	rendering_site->start_streaming(v_ctx_codec->width, v_ctx_codec->height, sciter::COLOR_SPACE_RGB24);

	while (!abruptly_end_all)
	{
		// Skip if user paused video. This also sleeps so that this loop
		// doesn't spin for no reason
		if (playback_paused_by_user)
		{
			Sleep(50);
			continue;
		}

		std::unique_lock<std::mutex> mtx_lock(mtx_frame_queue);
		while (frame_queue.v_frame_list.empty())
		{
			// If renderer_keep_waiting_for_decoder is false, that means assume this thread will no longer receive
			// any more frames.
			// Also checks if it should abruptly end
			if (!renderer_keep_waiting_for_decoder || abruptly_end_all)
			{
				goto EXIT_do_render_video;
			}
			cond_frame_queue.wait(mtx_lock);
		}

		// Convert whatever the source video was to a AV_PIX_FMT_RGB24 frame
		// This also grabs the correct details from v_ctx_codec such as height, as otherwise display_frame
		// wouldn't have them
		sws_scale(sws_ctx, (uint8_t const *const *)frame_queue.v_frame_list.front(),
				  frame_queue.v_frame_list.front()->linesize, 0, v_ctx_codec->height, display_frame->data, display_frame->linesize);

		synctime = av_frame_get_best_effort_timestamp(frame_queue.v_frame_list.front());

		synctime *= av_q2d(v_ctx_codec->framerate);

		// Free the struct and buffers of the frame at the front of the queue and remove it from the queue
		// This can be done here as the frame that is rendered is display_frame, and thus the mutex can also be unlocked already
		av_frame_free(&frame_queue.v_frame_list.front());
		frame_queue.v_frame_list.pop();
		mtx_lock.unlock();
		cond_frame_queue_too_large.notify_one();

		// If not at first frame, do run the delay
		if (!is_first_frame)
		{
			long long wait_time_start = Conversions::get_ms_since_epoch().count();
			// Keep looping until enough time has passed according to the frame duration
			do
			{
				// Calculate the delta t between the last frame being rendered and reaching this point of ready
				current_epoch = Conversions::get_ms_since_epoch().count();
				actual_delay = current_epoch - last_rendered_time;
			} while (frame_duration > actual_delay + 1);
			_WINDEBUGOUT("Waiting: " << Conversions::get_ms_since_epoch().count() - wait_time_start << std::endl);
		}

		is_first_frame = false;
		rendering_site->render_frame((const BYTE *)display_frame->data[0], rgb24_frame_buffer_size);
		// Count time from after the frame is rendered
		last_rendered_time = Conversions::get_ms_since_epoch().count();
		//mtx_lock.unlock();
	}

	// This is done to break out of the nested while loops
EXIT_do_render_video:
	// Clear the current frame stuff and the display buffer; the queue is wiped at
	// the bottom of initialise_video
	av_free(v_frame_display_buffer);
	av_free(display_frame);
	cond_frame_queue_too_large.notify_one();
	return;
}

void VideoDecoder::do_decode_video()
{
	//_WINDEBUGOUT(__LINE__ << std::endl);
	std::unique_lock<std::mutex> mtx_lock(mtx_frame_queue);
	if (frame_queue.v_frame_list.size() > frame_queue.max_frames)
	{
		//_WINDEBUGOUT(__LINE__ << std::endl);
		cond_frame_queue_too_large.wait(mtx_lock);
		//_WINDEBUGOUT(__LINE__ << std::endl);
	}
	mtx_lock.unlock();
	//_WINDEBUGOUT(__LINE__ << std::endl);

	// Check if this is a packet from the video stream
	if (v_pkt.stream_index == video_stream)
	{

		decode_clock_start = std::chrono::system_clock::now();
		avcodec_decode_video2(v_ctx_codec, v_frame, &frame_decode_finished, &v_pkt);

		// // Send packet, and check no error
		// int avcodec_ret = avcodec_send_packet(v_ctx_codec, &v_pkt);

		// if (avcodec_ret < 0 || avcodec_ret == AVERROR(EAGAIN) || avcodec_ret == AVERROR_EOF)
		// {
		//// 	_WINDEBUGOUT("avcodec_send_packet: " << avcodec_re\nt);
		// 	break;
		// }

		// while (avcodec_ret >= 0)
		// {
		if (frame_decode_finished)
		{
			v_frame_index++;

			long long current_epoch = Conversions::get_ms_since_epoch().count();

			// _WINDEBUGOUT("decoding\n");
			AVFrame *enqueue_frame = NULL;
			enqueue_frame = av_frame_alloc();
			// Lock here as frame_queue is used in do_render_video. Do not lock earlier
			// as that would be pointless and as little as possible should be executed while locked
			_WINDEBUGOUT(__LINE__ << std::endl);
			mtx_lock.lock();
			_WINDEBUGOUT(__LINE__ << std::endl);
			frame_queue.v_frame_list.push(enqueue_frame);

			// Move v_frame to the back (last) element of v_frame_list
			av_frame_move_ref(frame_queue.v_frame_list.back(), v_frame);
			// _WINDEBUGOUT("decoding done\n");
			_WINDEBUGOUT(__LINE__ << std::endl);
			mtx_lock.unlock();
			cond_frame_queue.notify_one();
		}

		std::chrono::system_clock::time_point clk_end = std::chrono::system_clock::now();
		std::chrono::milliseconds elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(clk_end - decode_clock_start);

		decoding_elapsed_total += elapsed.count();
		if (v_frame_index != 0)
			decode_time_avg = decoding_elapsed_total / v_frame_index;

		// _WINDEBUGOUT("Elapsed: " << elapsed.count() << "\n\n");
		// _WINDEBUGOUT("Frametime: " << elapsed/ << "\n\n");
		// _WINDEBUGOUT("Frametime total: " << frame_time_avg << "\n\n");
		// _WINDEBUGOUT("Elapsed total: " << decoding_elapsed_total << "\n\n");
	}
}

void VideoDecoder::initialise_video()
{
	// ---------------------------------------------------------------------
	// SECTION Initial reading and setup
	// ---------------------------------------------------------------------

	// Register all formats and codecs; makes all codecs/muxers etc. supported
	// Can call them individually if want to
	av_register_all();

	_WINDEBUGOUT(__FILE__ << ">\n"
						  << __LINE__ << "\n: FILE DIR" << file_dir.c_str() << "\n\n");
	// Open video
	if (avformat_open_input(&v_ctx_format, file_dir.c_str(), NULL, NULL) != 0)
	{
		_WINDEBUGOUT(__FILE__ << ">\n"
							  << __LINE__ << "\n: Couldn't open file\n\n");
		// throw std::runtime_error("Couldn't open file");
		return;
	}

	// Retrieve stream information
	if (avformat_find_stream_info(v_ctx_format, NULL) < 0)
	{
		_WINDEBUGOUT(__FILE__ << ">\n"
							  << __LINE__ << "\n: Couldn't find stream info\n\n");
		// throw std::runtime_error("Couldn't find stream info");
	}

	// Find the first video stream
	video_stream = -1;
	for (i = 0; i < v_ctx_format->nb_streams; i++)
		if (v_ctx_format->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			video_stream = i;
			// Get framerate guess value from numerator/denominator
			frame_rate = (double)v_ctx_format->streams[i]->r_frame_rate.num / (double)v_ctx_format->streams[i]->r_frame_rate.den;
			frame_duration = 1000.0 / frame_rate;
			break;
		}
	if (video_stream == -1)
	{
		_WINDEBUGOUT(__FILE__ << ">\n"
							  << __LINE__ << "\nDidn't find supported stream\n\n");
		// throw std::runtime_error("Didn't find supported stream");
	}

	// Get a pointer to the codec context for the video stream
	v_ctx_codec_orig = v_ctx_format->streams[video_stream]->codec;
	// Find the decoder for the video stream
	v_codec = avcodec_find_decoder(v_ctx_codec_orig->codec_id);
	if (v_codec == NULL)
	{
		_WINDEBUGOUT(__FILE__ << ">\n"
							  << __LINE__ << "\nUnsupported codec\n\n");
		// throw std::runtime_error("Unsupported codec");
	}

	// ---------------------------------------------------------------------
	// !SECTION
	// ---------------------------------------------------------------------

	// ---------------------------------------------------------------------
	// SECTION  video context codec and sws context
	// ---------------------------------------------------------------------

	// Allocate codec context with matching codec
	v_ctx_codec = avcodec_alloc_context3(v_codec);
	// Copy AVCodecContext from pCodecCtxOrig to pCodecCtx
	// Supposedly this is deprecated; see documentation with F12
	// TODO: Fix deprecated
	if (avcodec_copy_context(v_ctx_codec, v_ctx_codec_orig) != 0)
	{
		_WINDEBUGOUT(__FILE__ << ">\n"
							  << __LINE__ << "\nCouldn't copy codec context\n\n");
		// throw std::runtime_error("Couldn't copy codec context");
	}

	av_opt_set_int(v_ctx_codec, "refcounted_frames", 1, 0);
	// Open codec with provided context and codec
	if (avcodec_open2(v_ctx_codec, v_codec, NULL) < 0)
	{
		_WINDEBUGOUT(__FILE__ << ">\n"
							  << __LINE__ << "\nCouldn't open codec\n\n");
		// throw std::runtime_error("Couldn't open codec");
		return;
	}

	// Initialize SWS context for software scaling
	// Convert to RGB24 format
	sws_ctx = sws_getContext(v_ctx_codec->width,
							 v_ctx_codec->height,
							 v_ctx_codec->pix_fmt,
							 v_ctx_codec->width,
							 v_ctx_codec->height,
							 AV_PIX_FMT_RGB24,
							 SWS_BILINEAR,
							 NULL,
							 NULL,
							 NULL);

	// ---------------------------------------------------------------------
	// !SECTION
	// ---------------------------------------------------------------------

	// Get buffer size for each frame
	rgb24_frame_buffer_size = av_image_get_buffer_size(AV_PIX_FMT_RGB24, v_ctx_codec->width, v_ctx_codec->height, 1);

	// -----------------------------------------------------------------
	// SECTION v_frame
	// -----------------------------------------------------------------

	// Allocate video frame.
	v_frame = av_frame_alloc();
	if (v_frame == NULL)
	{
		_WINDEBUGOUT(__FILE__ << ">\n"
							  << __LINE__ << "\nv_frame is null; could not allocate\n\n");
		// throw std::runtime_error("v_frame is null; could not allocate");
	}

	// v_pkt.pts = av_rescale_q(v_pkt.pts, v_ctx_format->streams[video_stream]->time_base, gStream->time_base);
	// v_pkt.dts = av_rescale_q(v_pkt.dts, gStream->codec->time_base, gStream->time_base);

	// ---------------------------------------------------------------------
	// !SECTION
	// ---------------------------------------------------------------------

	// -------------------------------------------------------------------------
	// SECTION Decoding
	// -------------------------------------------------------------------------

	std::thread renderthread(&VideoDecoder::do_render_video, this);
	// renderthread.detach();

	i = 0;
	while (av_read_frame(v_ctx_format, &v_pkt) >= 0 && !abruptly_end_all)
	{
		do_decode_video();
		// Free the packet that was allocated by av_read_frame
		av_frame_unref(v_frame);
		av_packet_unref(&v_pkt);
		// NOTE: I'm unsure if v_frame and v_pkt will need to be freed in some other way if
		// abruptly end all is set to true
	}
	// -----------------------------------------------------------------
	// !SECTION
	// -----------------------------------------------------------------

	// -------------------------------------------------------------------------
	// SECTION Cleanup
	// -------------------------------------------------------------------------
	// Set renderer_keep_waiting_for_decoder to false to stop do_render_video() from waiting
	// for a cond_frame_queue notification if there is nothing in the queue
	renderer_keep_waiting_for_decoder = false;
	cond_frame_queue.notify_one();
	renderthread.join();
	frame_queue.v_frame_list.size();
	//_WINDEBUGOUT("FINISHSSSSSSSSSSSSSSSSSS\n");
	//_WINDEBUGOUT("FINISHSSSSSSSSSSSSSSSSSS\n");
	//_WINDEBUGOUT("FINISHSSSSSSSSSSSSSSSSSS\n");
	//_WINDEBUGOUT("FINISHSSSSSSSSSSSSSSSSSS\n");
	//_WINDEBUGOUT("FINISHSSSSSSSSSSSSSSSSSS\n");
	//_WINDEBUGOUT("FINISHSSSSSSSSSSSSSSSSSS\n");
	while (!frame_queue.v_frame_list.empty())
	{
		//_WINDEBUGOUT("QUITTING -> FREEING FRAME" << std::endl);
		av_frame_free(&frame_queue.v_frame_list.front());
		frame_queue.v_frame_list.pop();
	}

	av_free(v_frame);
	// Free the YUV frame
	// av_frame_free(&v_frame);

	// Close the codec
	avcodec_close(v_ctx_codec);
	avcodec_close(v_ctx_codec_orig);

	// Close the video file
	avformat_close_input(&v_ctx_format);

	sws_freeContext(sws_ctx);
	this->rendering_site = NULL;
	// this->frame_queue = NULL;
	this->renderer_keep_waiting_for_decoder = NULL;
	this->abruptly_end_all = NULL;
	this->v_ctx_format = NULL;
	this->v_ctx_codec_orig = NULL;
	this->v_ctx_codec = NULL;
	this->v_codec = NULL;
	this->v_frame = NULL;
	this->sws_ctx = NULL;
	this->v_frame_index = NULL;
	this->decoding_elapsed_total = NULL;
	// this->file_dir = NULL;
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------
}

void VideoDecoder::start()
{
	initialise_video();
	// std::thread initmainthread(&VideoDecoder::initialise_video, this);
	// initmainthread.detach();
	// initmainthread.join();
	//_WINDEBUGOUT("aaaaaaaaaa");
	this;
}

void VideoDecoder::early_stop_all()
{
	cond_frame_queue_too_large.notify_one();
	renderer_keep_waiting_for_decoder = false;
	abruptly_end_all = true;
	cond_frame_queue.notify_one();
}