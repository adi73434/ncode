// This is an open source non-commercial project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "MainFrame.h"

#include "Globals.h"

#include <sciter/sciter-x-video-api.h>
#include <sciter/sciter-x-behavior.h>
#include <sciter/sciter-x-threads.h>

#include "HandleEvents.h"

// #include "CustomVideo.cpp"
#include "VideoDecoder.h"
#include "resources.cpp"



/**
 * @brief This is the function ran by sciter-win-main.cpp, as that is where the entry point is contained.
 * I assume the run parameter is a fuction to tell Sciter to run the window.
 *
 * @param run
 * @return int
 */
int uimain(std::function<int()> run)
{
	// enable features to be used from script
	// ALLOW_EVAL is required for the functionality presented by reactor.observable.tis
	SciterSetOption(NULL, SCITER_SET_SCRIPT_RUNTIME_FEATURES,
					ALLOW_FILE_IO | ALLOW_SOCKET_IO | ALLOW_EVAL | ALLOW_SYSINFO);

	// Whether or not to use debug mode
	SciterSetOption(NULL, SCITER_SET_DEBUG_MODE, TRUE);



	// -------------------------------------------------------------------------
	// Exposing Logic to UI
	// -------------------------------------------------------------------------
	// SciterSetGlobalAsset(new CrunchImage());
	SciterSetGlobalAsset(new HandleEvents());
	// -------------------------------------------------------------------------
	//
	// -------------------------------------------------------------------------



	// bind resources[] (defined in "resources.cpp") with the archive
	sciter::archive::instance().open(aux::elements_of(resources));

	// Create main window frame
	sciter::om::hasset<MainFrame> pwin = new MainFrame();

	// note: this:://app URL is dedicated to the sciter::archive content associated with the application
	pwin->load(WSTR("this://app/main.htm"));

	// or use this to load UI from
	//  pwin->load( WSTR("file:///home/andrew/Desktop/Project/res/main.htm") );

	pwin->expand();

	// Pass these so they can be used globally... hwnd is needed for calling TiScript from C++, among other things
	Globals::mainPwin = pwin;
	Globals::mainHwnd = pwin->get_hwnd();

	//aaaaaa
	//aaaaaaaaaaaaaaaaaaaaaaaa
	//aaAaaaaaaaaaaaaaaaaaaaaaaaaa
	//aaaaaaaaaaaaaaaaaaaaaaaaaa
	//aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
	//aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
	//aaaaaaaaaaaaaaaaaaaaaaaaaaa11aaaaa
	//aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
	//aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
	//aaaaaaaaaaaaaaaaaaaaaaaaaaaa
	//aaaaaaaaaaaaaaaaaa1aaaaaaaaaaaaaaa
	//aaaaaaaaaaaaaaaaaaaaaaaaaaaaa
	//aaaaaaaaaaaaaaaaaaaaaaaaaaaaa
	//aaaaaaaaaaaaaaaaaaaaaaaaaaaaa
	//aaaaaaaaaaaaaaaaaaaaa
	//aaaaaaaaaaaaaaaaaaaa
	//aaaaaaaaaaaa
	//aaaaaaaa
	//aaa


	return run();
}
