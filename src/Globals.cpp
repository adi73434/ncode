// This is an open source non-commercial project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "Globals.h"

HWND Globals::mainHwnd;
sciter::om::hasset<MainFrame> Globals::mainPwin;
int Globals::myglobalint = 78;

std::string Globals::video_file_dir = "";
std::string Globals::video_file_encoded_affix = "";
std::string Globals::video_file_extension = "";

std::vector<std::vector<std::string>> Globals::vector_2d_original_video_details;

int Globals::videos_content_comment_index = 0;
int Globals::videos_file_count = 0;
int Globals::videos_codec_varaints = 0;
int Globals::videos_content_count = 0;

int Globals::streams_content_comment_index = 0;
int Globals::streams_file_count = 0;
int Globals::streams_codec_varaints = 0;
int Globals::streams_content_count = 0;

std::atomic<int> Globals::live_encoder_quality_setting = 0;
std::atomic<bool> Globals::live_encoder_quality_setting_changed = false;

std::atomic<bool> Globals::speedtest_upload_speed_read = false;
std::atomic<bool> Globals::user_started_streamer_before_speed_read = false;

std::atomic<long long> Globals::max_encoder_bitrate_cap = 750000;
std::atomic<long long> Globals::configured_encoder_bitrate = 750000;

sciter::video_destination *Globals::global_video_destination = NULL;

// Default values... these shall change immediately via a TiScript
// function call
std::string Globals::api_address = "http://localhost";
int Globals::api_port = 80;

std::atomic<bool> Globals::keep_decoder_alive = true;
std::mutex Globals::keep_decoder_alive_mtx;
std::atomic<bool> Globals::decoder_is_running = false;