
#pragma once

#include "Globals.h"
#include "stdafx.h"

#include <Windows.h>
#include <iostream>
#include <sstream>
#include <string>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>

#include <array>
#include <time.h>
#include <chrono>

#include <sciter/sciter-x.h>
#include <sciter/sciter-x-behavior.h>
#include <sciter/sciter-x-threads.h>
#include <sciter/sciter-x-video-api.h>

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <libavutil/opt.h>
}

#include "Conversions.h"

std::queue<AVFrame *> frame_queue;
std::vector<uint8_t> frame_vector;

#define _WINDEBUGOUT(s)                        \
	{                                          \
		std::wostringstream os_;               \
		os_ << s;                              \
		OutputDebugStringW(os_.str().c_str()); \
	}

/*
	BEHAVIOR: video_generated_stream
		- provides synthetic video frames.
		- this code is here solely for the demo purposes - how
			to connect your own video frame stream with the rendering site

	COMMENTS:
		 <video style="behavior:video-generator video" />
	SAMPLE:
		 See: samples/video/video-generator-behavior.htm
	*/


const int MAX_DECODED_FRAMES = 64;

	struct render_params
	{
		sciter::om::hasset<sciter::fragmented_video_destination> rendering_site;
		int frame_buffer_size;
		AVCodecContext *v_ctx_codec;
		struct SwsContext *sws_ctx;
	};

	struct FrameQueue
	{
		std::queue<AVFrame*> v_frame_list;
		// FrameQueue *next_frame;
		int total_num_frames;
		int current_frame;
	};

	static FrameQueue global_frame_queue;

	static std::mutex mtx_frame_queue;
	static std::condition_variable cond_frame_queue;
	static std::condition_variable cond_frame_queue_too_large;



struct custom_video_stream : public sciter::event_handler
{

	static void SaveFrame(AVFrame *v_frame, int width, int height, int iFrame)
	{
		FILE *pFile;
		char szFilename[32];
		int y;

		// Open file
		sprintf(szFilename, "frame%d.ppm", iFrame);
		pFile = fopen(szFilename, "wb");
		if (pFile == NULL)
			return;

		// Write header
		fprintf(pFile, "P6\n%d %d\n255\n", width, height);

		// Write pixel data
		for (y = 0; y < height; y++)
			fwrite(v_frame->data[0] + y * v_frame->linesize[0], 1, width * 3, pFile);

		// Close file
		fclose(pFile);
	}

	sciter::om::hasset<sciter::video_destination> rendering_site;
	// ctor
	custom_video_stream() {}
	virtual ~custom_video_stream() {}

	virtual bool subscription(HELEMENT he, UINT &event_groups)
	{
		event_groups = HANDLE_BEHAVIOR_EVENT; // we only handle VIDEO_BIND_RQ here
		return true;
	}

	virtual void attached(HELEMENT he)
	{
		he = he;
	}

	virtual void detached(HELEMENT he) { asset_release(); }
	virtual bool on_event(HELEMENT he, HELEMENT target, BEHAVIOR_EVENTS type, UINT_PTR reason)
	{
		if (type != VIDEO_BIND_RQ)
			return false;
		// we handle only VIDEO_BIND_RQ requests here

		//printf("VIDEO_BIND_RQ %d\n",reason);

		if (!reason)
			return true; // first phase, consume the event to mark as we will provide frames

		rendering_site = (sciter::video_destination *)reason;
		sciter::om::hasset<sciter::fragmented_video_destination> fsite;

		if (rendering_site->asset_get_interface(FRAGMENTED_VIDEO_DESTINATION_INAME, fsite.target()))
		{
			// sciter::thread(generation_thread, "lol");
			sciter::thread(generation_thread, fsite);
		}

		return true;
	}

	// -------------------------------------------------------------------------
	// NOTE: stuff for when I was trying to put the rendering in a queue
	// -------------------------------------------------------------------------



	static void render_thread(render_params params)
	{
		// Display frame will store the frame to-display, separate from the frame queue
		AVFrame *display_frame = NULL;

		// Initialising display_frame the same way it is done in the decode thread
		uint8_t *v_frame_display_buffer = NULL;
		display_frame = av_frame_alloc();
		v_frame_display_buffer = (uint8_t *)av_malloc(params.frame_buffer_size * sizeof(uint8_t));
		avpicture_fill((AVPicture *)display_frame, v_frame_display_buffer, AV_PIX_FMT_RGB24,
				params.v_ctx_codec->width, params.v_ctx_codec->height);

		bool is_first_frame = true;
		params.rendering_site->start_streaming(params.v_ctx_codec->width, params.v_ctx_codec->height, sciter::COLOR_SPACE_RGB24);
		while (true)
		{
			Sleep(5);
			std::unique_lock<std::mutex> mtx_lock(mtx_frame_queue);
			_WINDEBUGOUT("displaying\n");
			if (!global_frame_queue.v_frame_list.empty())
			{
				// Move the frame from the front of the queue out of the queue so it can be popped
				// while the frame is still being displayed
				// AVFrame *next_in_queue = ;
				// av_frame_move_ref(display_frame, global_frame_queue.v_frame_list.front());

				sws_scale(params.sws_ctx, (uint8_t const *const *)global_frame_queue.v_frame_list.front(),
							  global_frame_queue.v_frame_list.front()->linesize, 0, params.v_ctx_codec->height, display_frame->data, display_frame->linesize);
				display_frame;
				_WINDEBUGOUT("displaying now\n");

				params.rendering_site->render_frame((const BYTE *)display_frame->data[0], params.frame_buffer_size);

				// Free the frame struct and buffers
				av_frame_free(&global_frame_queue.v_frame_list.front());

				global_frame_queue.v_frame_list.pop();
				// Unref the frame at the front and remove it from queue

			}
			else
			{
			_WINDEBUGOUT("displaying can't\n");
			}
			cond_frame_queue_too_large.notify_one();
			//mtx_lock.unlock();
		}
	}

	static void generation_thread(sciter::fragmented_video_destination *dst)
	{
		// Create sciter rendering site thing
		sciter::om::hasset<sciter::fragmented_video_destination> rendering_site = dst;
		AVFormatContext *v_ctx_format = NULL;
		AVCodecContext *v_ctx_codec_orig = NULL;
		AVCodecContext *v_ctx_codec = NULL;
		AVCodec *v_codec = NULL;
		AVFrame *v_frame = NULL;
		AVFrame *v_frame_test = NULL;
		AVFrame *v_frame_rgb = NULL;
		uint8_t *v_frame_rgb_buffer = NULL;
		AVPacket v_pkt;
		int frame_finished;
		struct SwsContext *sws_ctx = NULL;
		int i;
		int video_stream;
		bool first_frame = true;
		double frame_rate;
		double frame_duration;
		int v_frame_index = 0;
		// std::chrono::system_clock::time_point last_rendered_time;
		// std::chrono::milliseconds last_rendered_time;
		long long last_rendered_time;
		std::string file_dir = "d.mp4";

		// ---------------------------------------------------------------------
		// SECTION Initial reading and setup
		// ---------------------------------------------------------------------

		// Register all formats and codecs; makes all codecs/muxers etc. supported
		// Can call them individually if want to
		av_register_all();

		// Open video
		if (avformat_open_input(&v_ctx_format, file_dir.c_str(), NULL, NULL) != 0)
		{
			_WINDEBUGOUT("Couldn't open file\n");
			// throw std::runtime_error("Couldn't open file");
			return;
		}

		// Retrieve stream information
		if (avformat_find_stream_info(v_ctx_format, NULL) < 0)
		{
			_WINDEBUGOUT("Couldn't find stream info\n");
			// throw std::runtime_error("Couldn't find stream info");
		}

		// Find the first video stream
		video_stream = -1;
		for (i = 0; i < v_ctx_format->nb_streams; i++)
			if (v_ctx_format->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
			{
				video_stream = i;
				// Get framerate guess value from numerator/denominator
				frame_rate = (double)v_ctx_format->streams[i]->r_frame_rate.num / (double)v_ctx_format->streams[i]->r_frame_rate.den;
				frame_duration = 1000.0 / frame_rate;
				break;
			}
		if (video_stream == -1)
		{
			_WINDEBUGOUT("Didn't find supported stream\n");
			// throw std::runtime_error("Didn't find supported stream");
		}

		// Get a pointer to the codec context for the video stream
		v_ctx_codec_orig = v_ctx_format->streams[video_stream]->codec;
		// Find the decoder for the video stream
		v_codec = avcodec_find_decoder(v_ctx_codec_orig->codec_id);
		if (v_codec == NULL)
		{
			_WINDEBUGOUT("Unsupported codec\n");
			// throw std::runtime_error("Unsupported codec");
		}

		// ---------------------------------------------------------------------
		// !SECTION
		// ---------------------------------------------------------------------

		// ---------------------------------------------------------------------
		// SECTION  video context codec and sws context
		// ---------------------------------------------------------------------

		// Allocate codec context with matching codec
		v_ctx_codec = avcodec_alloc_context3(v_codec);
		// Copy AVCodecContext from pCodecCtxOrig to pCodecCtx
		// Supposedly this is deprecated; see documentation with F12
		// TODO: Fix deprecated
		if (avcodec_copy_context(v_ctx_codec, v_ctx_codec_orig) != 0)
		{
			_WINDEBUGOUT("Couldn't copy codec context\n");
			// throw std::runtime_error("Couldn't copy codec context");
		}

		av_opt_set_int(v_ctx_codec, "refcounted_frames", 1, 0);
		// Open codec with provided context and codec
		if (avcodec_open2(v_ctx_codec, v_codec, NULL) < 0)
		{
			_WINDEBUGOUT("Couldn't open codec\n");
			// throw std::runtime_error("Couldn't open codec");
			return;
		}

		// Initialize SWS context for software scaling
		// Convert to RGB24 format
		sws_ctx = sws_getContext(v_ctx_codec->width,
								 v_ctx_codec->height,
								 v_ctx_codec->pix_fmt,
								 v_ctx_codec->width,
								 v_ctx_codec->height,
								 AV_PIX_FMT_RGB24,
								 SWS_BILINEAR,
								 NULL,
								 NULL,
								 NULL);

		// I was having issues with stuff playing at 30fps, but that issue is gone now
		// but this was one of the things I tried and I'm keeping here for now, even though
		// it's completely redundant
		// v_ctx_codec->time_base.num = v_ctx_codec_orig->time_base.num;
		// v_ctx_codec->time_base.den = v_ctx_codec_orig->time_base.den;
		// ---------------------------------------------------------------------
		// !SECTION
		// ---------------------------------------------------------------------

		// Get buffer size for each frame
		int frame_buffer_size = av_image_get_buffer_size(AV_PIX_FMT_RGB24, v_ctx_codec->width, v_ctx_codec->height, 1);

		// -----------------------------------------------------------------
		// SECTION v_frame and v_frame_rgb
		// -----------------------------------------------------------------

		// Allocate video frame.
		v_frame = av_frame_alloc();
		v_frame_test = av_frame_alloc();
		v_frame_rgb = av_frame_alloc();
		// if (v_frame == NULL)
		if (v_frame == NULL || v_frame_rgb == NULL)
		{
			_WINDEBUGOUT("v_frame is null; could not allocate\n");
			// throw std::runtime_error("v_frame is null; could not allocate");
		}

		// v_pkt.pts = av_rescale_q(v_pkt.pts, v_ctx_format->streams[video_stream]->time_base, gStream->time_base);
		// v_pkt.dts = av_rescale_q(v_pkt.dts, gStream->codec->time_base, gStream->time_base);

		// Determine required buffer size and allocate buffer
		v_frame_rgb_buffer = (uint8_t *)av_malloc(frame_buffer_size * sizeof(uint8_t));

		// Assign appropriate parts of buffer to image planes in v_frame_rgb
		// Note that v_frame_rgb is an AVFrame, but AVFrame is a superset
		// of AVPicture
		avpicture_fill((AVPicture *)v_frame_rgb, v_frame_rgb_buffer, AV_PIX_FMT_RGB24,
					   v_ctx_codec->width, v_ctx_codec->height);

		// ---------------------------------------------------------------------
		// !SECTION
		// ---------------------------------------------------------------------

		// -----------------------------------------------------------------
		// SECTION Rendering
		// Read frames and save first five frames to disk
		// Packet passed as reference; it'll store frame data
		// -----------------------------------------------------------------
		// https://stackoverflow.com/questions/61498044/how-do-i-set-output-frame-time-using-libavcodec
		// std::unique_ptr<AVFormatContext, void(*)(AVFormatContext*)> octx(v_ctx_format, [](AVFormatContext* p){ if(p) avformat_close_input(&p); });
		// AVStream *strm = avformat_new_stream(octx.get(), 0);

		long long rendering_delay = 0;

		render_params args;

		args.rendering_site = dst;
		args.frame_buffer_size = frame_buffer_size;
		args.v_ctx_codec = v_ctx_codec;
		args.sws_ctx = sws_ctx;

		std::thread renderthread(&custom_video_stream::render_thread, args);
		renderthread.detach();


		long long elapsed_total = 0;
		std::chrono::system_clock::time_point clk_start;
		int frame_time_avg = 0;

		i = 0;
		while (av_read_frame(v_ctx_format, &v_pkt) >= 0)
		{
			std::unique_lock<std::mutex> mtx_lock(mtx_frame_queue);
			if (global_frame_queue.v_frame_list.size() > MAX_DECODED_FRAMES)
			{
				cond_frame_queue_too_large.wait(mtx_lock);
			}
			mtx_lock.unlock();

			clk_start = std::chrono::system_clock::now();

			// ? Check if this is a packet from the video stream
			if (v_pkt.stream_index == video_stream)
			{

				avcodec_decode_video2(v_ctx_codec, v_frame, &frame_finished, &v_pkt);

				// // Send packet, and check no error
				// int avcodec_ret = avcodec_send_packet(v_ctx_codec, &v_pkt);

				// if (avcodec_ret < 0 || avcodec_ret == AVERROR(EAGAIN) || avcodec_ret == AVERROR_EOF)
				// {
				// 	_WINDEBUGOUT("avcodec_send_packet: " << avcodec_ret);
				// 	break;
				// }

				// while (avcodec_ret >= 0)
				// {
				if (frame_finished)
				{
					// It seems that if you Sleep here for, for example, 3 or 8ms manually
					// to offset based off of frame_time_avg
					// the frame_time_avg and elapsed will jump up more than the specified number

					// For example, 5 is the frame_time_avg when not using any sleep
					// therefore to adjust for a 120 fps video, you should sleep for 5+5
					// but that makes the frame_time_avg jump to 17

					// Based on 120fps video
					// No Sleep() frame_time_avg = ~5
					// Sleep(100) // resulting frame_time_avg = 108
					// Sleep(50) // resulting frame_time_avg = 62
					// Sleep(20) // resulting frame_time_avg = 30
					// Sleep(10) // resulting frame_time_avg = 22
					// Sleep(5) // resulting frame_time_avg = 17
					// Sleep(3) // resulting frame_time_avg = 15
					// Sleep(1) // resulting frame_time_avg = 15

					v_frame_index++;
					// 	avcodec_ret = avcodec_receive_frame(v_ctx_codec, v_frame);
					// 	if (avcodec_ret == AVERROR(EAGAIN) || avcodec_ret == AVERROR_EOF)
					// 	{
					// 		_WINDEBUGOUT("avcodec_receive_frame: " << avcodec_ret << "\n");
					// 		break;
					// 	}

					if (!first_frame)
					{
						// std::chrono::milliseconds current_epoch = Conversions::get_ms_since_epoch();
						long long current_epoch = Conversions::get_ms_since_epoch().count();
						// std::chrono::system_clock::duration rendering_delay = current_epoch - last_rendered_time;
						rendering_delay = current_epoch - last_rendered_time;
						// _WINDEBUGOUT("The value of current_epoch is" << current_epoch << "\n");
						// _WINDEBUGOUT("The value of last_rendered_time is" << last_rendered_time << "\n");
						// _WINDEBUGOUT("The value of rendering_delay is" << rendering_delay << "\n");
						// _WINDEBUGOUT("The value of frame_duration is" << frame_duration << "\n");

						// Wait until enough time has passed to play the correct frame
						if (frame_duration > rendering_delay)
						{
							// Sleep(frame_duration - rendering_delay);
						}
					}
					else
					{
						first_frame = false;
					}
					// Convert whatever the source video was to a AV_PIX_FMT_RGB24 frame

					mtx_lock.lock();
					_WINDEBUGOUT("decoding\n");
					AVFrame *enqueue_frame = NULL;
					enqueue_frame = av_frame_alloc();
					global_frame_queue.v_frame_list.push(enqueue_frame);

					// Move v_frame_rgb to the back (last) element of v_frame_list
					av_frame_move_ref(global_frame_queue.v_frame_list.back(), v_frame);
					_WINDEBUGOUT("decoding done\n\n");
					mtx_lock.unlock();
					cond_frame_queue.notify_one();
					
					// Needed to unreference each frame as it is ref counted
					av_frame_unref(v_frame);
					last_rendered_time = Conversions::get_ms_since_epoch().count();
				}

				std::chrono::system_clock::time_point clk_end = std::chrono::system_clock::now();
				std::chrono::milliseconds elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(clk_end - clk_start);

				elapsed_total += elapsed.count();
				if (v_frame_index != 0)
					frame_time_avg = elapsed_total / v_frame_index;

				// _WINDEBUGOUT("Elapsed: " << elapsed.count() << "\n");
				// _WINDEBUGOUT("Frametime: " << elapsed/ << "\n");
				// _WINDEBUGOUT("Frametime total: " << frame_time_avg << "\n");
				// _WINDEBUGOUT("Elapsed total: " << elapsed_total << "\n");
			}

			// Free the packet that was allocated by av_read_frame
			av_packet_unref(&v_pkt);

			// Sleep(std::round(frame_duration-rendering_delay));
		}

		// -----------------------------------------------------------------
		// !SECTION
		// -----------------------------------------------------------------

		// Free the YUV frame
		// av_frame_free(&v_frame);

		// Close the codec
		// avcodec_close(v_ctx_codec);
		// avcodec_close(v_ctx_codec_orig);

		// Close the video file
		// avformat_close_input(&v_ctx_format);
	}
};

struct custom_video_stream_factory : public sciter::behavior_factory
{

	custom_video_stream_factory() : sciter::behavior_factory("custom-video-generator")
	{
	}

	// the only behavior_factory method:
	virtual sciter::event_handler *create(HELEMENT he)
	{
		return new custom_video_stream();
	}
};

// instantiating and attaching it to the global list
custom_video_stream_factory custom_video_stream_factory_instance;
