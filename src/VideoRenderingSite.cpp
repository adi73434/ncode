
#pragma once

#include "Globals.h"
#include "stdafx.h"

#include <Windows.h>
#include <iostream>
#include <sstream>
#include <string>

#include <sciter/sciter-x.h>
#include <sciter/sciter-x-behavior.h>
#include <sciter/sciter-x-threads.h>
#include <sciter/sciter-x-video-api.h>

#include "Conversions.h"
#include "HandleEvents.h"

#include "VideoDecoder.h"

/*
	BEHAVIOR: video_generated_stream
		- provides synthetic video frames.
		- this code is here solely for the demo purposes - how
			to connect your own video frame stream with the rendering site

	COMMENTS:
		 <video style="behavior:video-generator video" />
	SAMPLE:
		 See: samples/video/video-generator-behavior.htm
	*/

/**
 * @brief This is the custom behaviour implementation for the `custom-video-generator` behaviour
 * used for the <video> tag for the video player.
 * 
 */
struct custom_video_stream : public sciter::event_handler
{

	sciter::om::hasset<sciter::video_destination> rendering_site;
	// ctor
	custom_video_stream() {}
	virtual ~custom_video_stream() {}

	virtual bool subscription(HELEMENT he, UINT &event_groups)
	{
		event_groups = HANDLE_BEHAVIOR_EVENT; // we only handle VIDEO_BIND_RQ here
		return true;
	}

	virtual void attached(HELEMENT he)
	{
		he = he;
	}

	/**
	 * @brief When the element gets detached, try to stop the decoder and wait until it
	 * has stopped.
	 * 
	 * @param he 
	 */
	virtual void detached(HELEMENT he)
	{
		// When the element gets detached, tell the decoder to stop
		std::unique_lock<std::mutex> mtx_lock(Globals::keep_decoder_alive_mtx);
		Globals::keep_decoder_alive = false;
		while (Globals::decoder_is_running)
		{
			Sleep(10);
		}
		Globals::global_video_destination = NULL;
		asset_release();
		mtx_lock.unlock();
	}

	/**
	 * @brief I'm not entirely sure what on_event is, but it fires several times after attaching.
	 * The implementation here is only concerned with creating a Sciter rendering site and passing
	 * that to to the Globals::global_vide_destination so that it can be accessed anywhere in the code.
	 * 
	 * @param he 
	 * @param target 
	 * @param type 
	 * @param reason 
	 * @return true 
	 * @return false 
	 */
	virtual bool on_event(HELEMENT he, HELEMENT target, BEHAVIOR_EVENTS type, UINT_PTR reason)
	{
		if (type != VIDEO_BIND_RQ)
			return false;
		// we handle only VIDEO_BIND_RQ requests here

		//printf("VIDEO_BIND_RQ %d\n",reason);

		if (!reason)
			return true; // first phase, consume the event to mark as we will provide frames

		rendering_site = (sciter::video_destination *)reason;
		sciter::om::hasset<sciter::video_destination> fsite;

		if (rendering_site->asset_get_interface(VIDEO_DESTINATION_INAME, fsite.target()))
		{
			// Pass the video rendering site to the Globals class so it can be be accessed in the 
			// methods that trigger the video playback which exist outside of this struct.
			Globals::global_video_destination = fsite;
			// HandleEvents hevnt;
			// hevnt.play_video_thread("d.mp4", fsite);
		}

		return true;
	}
};

/**
 * @brief This creates the behaviour based on custom_video_stream.
 * 
 */
struct custom_video_stream_factory : public sciter::behavior_factory
{

	custom_video_stream_factory() : sciter::behavior_factory("custom-video-generator")
	{
	}

	// the only behavior_factory method:
	virtual sciter::event_handler *create(HELEMENT he)
	{
		return new custom_video_stream();
	}
};

// instantiating and attaching it to the global list
custom_video_stream_factory custom_video_stream_factory_instance;
