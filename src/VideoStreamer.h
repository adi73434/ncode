#pragma once

// Threading
#include <atomic>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>

// Sciter
#include <sciter/sciter-x.h>
#include <sciter/sciter-x-behavior.h>
#include <sciter/sciter-x-threads.h>
#include <sciter/sciter-x-video-api.h>

// ffmpeg/Libav
extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavformat/avio.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <libavutil/opt.h>
#include <libavutil/file.h>
}

#include "Conversions.h"

/**
 * @brief This is the VideoStreamer class which handles the decoding->encoding->redecoding->rendering process.
 * 
 * @details The primary decoding process is supposed to replace the stage in a live stream where the streamer
 * captures their desktop, or some sort of device.
 * The encoding process emulates the stage where something like OBS (or NVIDIA ShadowPlay/AMD ReLive) would
 * encode the input. Instead, this encodes the decoded output.
 * The redecoding process acts as a viewer of the stream, where normally this would be a viewer of a live stream
 * connecting to a UDP/RTMP-or-whatever video stream - think YouTube/Twitch/Discord, where you download video data
 * and decode that. This obviously differs because the whole encode->decode process is done on one computer, and
 * within a memory buffer, rather than with one creator and multiple viewers and through a server.
 * The rendering process finally renders the redecoded video, same as how YouTube/Twitch/YouTube etc. would render
 * the decoded stream.
 * 
 */
class VideoStreamer
{

public:
	/**
	 * @brief Construct a new Video Streamer object
	 * 
	 * @param param_dst This is the sciter rendering site used for rendering frames.
	 * @param param_file_dir File which will be decoded -> encoded -> redecoded -> rendered
	 * @param encoder_name The name of the encoder with which the encoding will be done
	 * @param param_desired_quality This is the desired quality based on the selection in the UI.
	 * @param param_max_prepared_input This is the specified number of maximum frames
	 * that can be in any of the frame queues.
	 * @param param_max_output This is the specified number of maximum packets/redecoded
	 * frames, limiting the amount that the encoder will catch up.
	 * 
	 */
	VideoStreamer(sciter::video_destination *param_dst,
				  std::string param_file_dir,
				  std::string encoder_name,
				  std::string param_desired_quality,
				  int param_max_prepared_input,
				  int param_max_output,
				  sciter::value param_ui_callback);
	~VideoStreamer();

	/**
	 * @brief Start the VideoStreamer by calling initialise_video.
	 * 
	 */
	void start();

	/**
	 * @brief This is used to stop the decoder->encoder->redecoder->renderer process by changing the variables that those
	 * threads rely on to make them behave as if they no longer are receiving anything, hence stopping the thread. This
	 * requires the notify_one in case any of the threads were waiting.
	 * 
	 */
	void early_stop_all();

	std::atomic<bool> playback_paused_by_user;

private:
	/**
	 * @brief Frame queue which uses an std::queue of pointers to AVFrames.
	 * The decode thread will add frames to the back of the queue; meanwhile,
	 * the render thread will take from the front of the queue and pop it.
	 * 
	 * @note Use a mutex for accessing this on multiple threads
	 * 
	 */
	struct FrameQueue
	{
		std::queue<AVFrame *> v_frame_list;
		/**
		 * @brief defines the maximum number of frames that can be
		 * * placed into the queue. This is done to limit memory usage.
		 * 
		 */
		int max_frames;
	};

	struct PacketQueue
	{
		std::queue<AVPacket *> v_pkt_list;
		/**
		 * @brief defines the maximum number of frames that can be
		 * * placed into the queue. This is done to limit memory usage.
		 * 
		 */
		int max_packets;
	};

	void do_emulate_frame_skip(int time_dif_ms_count);

	/**
	 * @brief This takes the first frame from redecoder_frame_queue and renders it to the screen using
	 * Sciter's rendering site.
	 * 
	 * @details This is similar to the VideoDecoder implementation, but it renders IYUV instead.
	 * 
	 * @warning If abruptly_end_all is used, frames will amost certainly still exist in the redecoder_frame_queue.
	 * This is cleared at the end of initialise_video().
	 * 
	 */
	void do_render_emulated_stream();

	/**
	 * @brief This decodes the video from the memory buffer written to by do_encode_input_file(), and then it queues up the
	 * decoded frame to be rendered on screen with do_render_video()
	 * 
	 * @details This is supposed to mimick the stage where a viewer of a livestream would receive a stream of packets from a server,
	 * which would then be decoded and displayed. Of course, this implementation does not use a server, but simply relies on the
	 * bitrate limit in do_encode_input_file().
	 * 
	 */
	void do_redecode_stream();

	/**
	 * @brief This initialises the required bits for do_redecode_stream.
	 * 
	 * @details This uses the encoder ID from the encoder context, and it copies its parameters.
	 */
	void init_redecode_stream();

	/**
	 * @brief This takes the frames that were decoded by do_decode_input_file(), and it then re-encodes the video based on the chosen
	 * codec from the UI, and this will be what impacts the quality.
	 * 
	 * @details The encoding here will be done with the provided codec and quality from the UI, and the user will be able to update
	 * the quality, which will restart the entire process with the new quality setting. The implementation of this should "emulate"
	 * encoding in OBS, except instead of using screen capture it uses a decoded video file. The additional bottleneck implementation
	 * here will be limiting the maximum output bitrate to be 50-80% of the user's maximum upload speed, maximising at 100Mbps. This
	 * is done to imitate the limitation of home broadband and the maximum allowed ingest bitrate of a server. The reason 100Mbps
	 * is the limit here is becase YouTube can technically allow 50Mbps, and I don't really care about the ingest side as much as I
	 * do the user-upload-speed side, so the 100Mbps limit is just for some semblance of sanity in case I give this test app to someone
	 * with 1Gbps symmetrical internet or whatever.
	 * 
	 */
	void do_encode_input_file();

	/**
	 * @brief This initialises the required bits for do_encode_input_file.
	 * 
	 * @details This decides the upload speed based on a percentage of the user's tested upload speed, with a specified default and
	 * a maximum. It also finds the specified encoder, and based on the encoder, it convert's the user's specified "quality" setting
	 * to a somewhat representative quality setting in the encoder. When the user changes these settings, the stream will be restarted
	 * by the parent which called VideoStreamer.
	 * 
	 */
	void init_encode_input_file();

	/**
	 * @brief This decodes the input file into a memory buffer, replacing the step in something like OBS which would otherwise be
	 * desktop capture. But because this needs standardised videos for quality rating, it's going to be based off of a video file.
	 * 
	 * @details The input file is decoded into a memory buffer, up to a certain number of maximum frames, and it is then encoded
	 * into another memory buffer. That encoded memory buffer is what would normally be sent to a RTMP/UDP server or whatever, and
	 * then transmitted to viewers of the live stream. However, because that's not something I'm implemeneting, the encoded memory
	 * buffer will simply be re-decoded in do_decode_emulated_stream()
	 * 
	 */
	void do_decode_input_file();

	/**
	 * @brief This initialises the decoder based on the provided file directory input, it finds the fps etc.
	 * 
	 * @warning The current implementation assumed YUV input files.
	 * 
	 */
	void init_decode_input_file();

	/**
	 * @brief Initialise the video, launch a source-decoder thread, source-re-encoder thread, and a re-encoded decoder.
	 * 
	 * @details This spawns the threads, then waits for them all to finish. Once they all finish, it ensures that the
	 * queues are empty (wiping them if they weren't empty), and it then closes the formats/contextx/frames/buffers etc.
	 * once everything is done.
	 * 
	 */
	void initialise_video();

	/**
	 * @brief Sciter's rendering site.
	 * This is where frame data is passed to to render.
	 * 
	 */
	sciter::om::hasset<sciter::video_destination> rendering_site;

	/**
	 * @brief Directory of the video input file.
	 * 
	 */
	std::string file_dir;

	/**
	 * @brief This will hold the desired codec name, based off of the values in the stream array in main.tis.
	 * 
	 */
	std::string desired_encoder_name;

	/**
	 * @brief This is the desired quality from the UI, and in the current implementation (as of writing), it
	 * is basically just "quality-1", "quality-2" etc., and the VideoStreamer implementation will de-abstract
	 * that into something useful based on each codec.
	 * 
	 */
	std::string desired_quality;

	/**
	 * @brief This is the callback to the function which alerts the user in the UI.
	 * 
	 */
	sciter::value ui_callback;

	/**
	 * @brief This is the frame queue of frames once they have been decoded->encoded->redecoded. These will be used for
	 * display.
	 * 
	 */
	FrameQueue redecoder_frame_queue;

	/**
	 * @brief This is a, depending on if it's implemented or not, queue for the output of encoded frames. This was/is
	 * supposed to limit the amount of encoded frames at once.
	 * 
	 * @details If this is implemented (check if do_encode_input_file uses it), that means most likely that I decided
	 * to re-implement the encoder packet queue, but the case where it is/was not implemented is because the queue can
	 * be avoided by directly sending the packet into the re-decoder. 
	 * 
	 */
	PacketQueue encoder_output_packet_queue;

	/**
	 * @brief This is the frame queue of frames output by the initial decoder, and this will be used by the encoder.
	 * 
	 */
	FrameQueue source_frame_queue;

	/**
	 * @brief This is used to signal the decoder to stop, and 
	 * to signal do_render_emulated_stream() to stop trying to render frames,
	 * regardless of whether the queue is empty or not.
	 * 
	 */
	std::atomic<bool> abruptly_end_all;

	/**
	 * @brief Condition that is waited on/notified when the redecoder frame queue is too small,
	 * aka the redecoder has no input.
	 * 
	 */
	std::condition_variable cond_redecoder_frame_queue_too_small;

	/**
	 * @brief Condition that is waited on/notified when the redecoder frame queue is too large,
	 * aka the output of the redecoder is too large.
	 * 
	 */
	std::condition_variable cond_redecoder_frame_queue_too_large;

	std::condition_variable cond_encoder_packet_queue_too_small;
	std::condition_variable cond_encoder_packet_queue_too_large;

	/**
	 * @brief Condition that is waited on/notified when the source decoder frame queue is too small,
	 * aka the encoder has no input.
	 * 
	 */
	std::condition_variable cond_source_frame_queue_too_small;

	/**
	 * @brief Condition that is waited on/notified when the source decoder frame queue is too large,
	 * aka the output of the source decoder is too large.
	 * 
	 */
	std::condition_variable cond_source_frame_queue_too_large;

	std::condition_variable_any cond_emulate_frame_skip;

	/**
	 * @brief Format context that is used to open/configure the codec for the initial decoder.
	 * 
	 */
	AVFormatContext *v_source_ctx_format;

	/**
	 * @brief Context used for the codec for the redecoder stage, which gets configured partially using the
	 * v_encoder_ctx_codec.
	 * 
	 */
	AVCodecContext *v_redecoder_ctx_codec;

	/**
	 * @brief Context used for the encoder stage, which gets configured via the v_encoder_codec
	 * which is based on the user input.
	 * 
	 */
	AVCodecContext *v_encoder_ctx_codec;
	/**
	 * @brief Context used for the codec initial decoder stage, configured via v_source_codec which is
	 * based on the input file.
	 * 
	 */

	AVCodecContext *v_source_ctx_codec;

	/**
	 * @brief Used to store the codec info from the original input codec stream after the correct
	 * video_stream has been identified in v_source_ctx_format.
	 * 
	 */
	AVCodecContext *v_source_ctx_codec_orig;

	/**
	 * @brief Decoder codec which is based on v_encoder_ctx_codec's codec, which is based on v_encoder_codec.
	 * Essentially this just loads the decoder codec for whatever the encoded video was encoded with.
	 * 
	 */
	AVCodec *v_redecoder_codec;

	/**
	 * @brief Encoder codec which will be used to encode the video, varies based on what codec ("version 1/2/3/4")
	 * the user selected in the UI.
	 * 
	 */
	AVCodec *v_encoder_codec;

	/**
	 * @brief This is the codec of the original video, which is found via v_source_ctx_codec_orig's codec.
	 * 
	 */
	AVCodec *v_source_codec;

	/**
	 * @brief Frame which will hold whatever shall be rendered on the screen, and it allows the frame to be held
	 * onto after popping the redecoder frame queue.
	 * 
	 */
	AVFrame *v_display_frame;

	/**
	 * @brief This is the frame buffer used for v_display_frame.
	 * 
	 */
	AVBufferRef *v_display_frame_buffer;

	/**
	 * @brief This holds the frame which is received by do_redecode_stream, aka the output decoded frame. This is
	 * then pushed into the redecoder_frame_queue, ready to be displayed.
	 * 
	 */
	AVFrame *v_redecoder_frame;

	/**
	 * @brief This is the frame which is used to grab the output from the initial decoder, which is then passed into
	 * the encoder
	 * 
	 */
	AVFrame *v_encoder_frame;

	/**
	 * @brief This holds the output frame from the initial decoder, which is then passed into the source_frame_queue,
	 * making it ready to be encoded.
	 * 
	 */
	AVFrame *v_source_frame;

	AVPacket *v_redecoded_pkt;

	/**
	 * @brief This holds the output packet from the encoder, which is then passed directly into the redecoder IF
	 * encoder_output_packet_queue is not being used. Otherwise, it is pushed into encoder_output_packet_queue, and
	 * then the redecoder shall read from that.
	 * 
	 */
	AVPacket *v_encoder_pkt;

	/**
	 * @brief This holds the packet fetched by av_read_frame in the initial decoder, which is passed into
	 * avcodec_send_packet to decode the packet into a frame.
	 * 
	 */
	AVPacket v_source_pkt;

	std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds> renderer_start_time;
	
	/**
	 * @brief This basically stores the "representation", if you will, for getting the timebase in milliseconds from pts.
	 * 
	 */
	AVRational timebase_ms;

	AVRational timebase_set;

	/**
	 * @brief Keeps track of whether the renderer is still running, and if it's set to false manually, the renderer
	 * will exit.
	 * 
	 * @details This will naturally become false under normal execution once the renderer is finished, but that first requires
	 * the redecoder to be finished, which requires the encoder to be finished, which requires the decoder to be finished.
	 * 
	 * @note Because the renderer might be waiting on a condition, also notify on the potential conditions:
	 * - cond_redecoder_frame_queue_too_small
	 * 
	 */
	std::atomic<bool> renderer_still_running;

	/**
	 * @brief Keeps track of whether the renderer is still running, and if it's set to false manually, the redecoder
	 * will exit.
	 * 
	 * @details This will naturally become false under normal execution once the redecoder is finished, but that first requires
	 * the encoder to be finished, which requires the decoder to be finished.
	 * 
	 * @note Because the redecoder might be waiting on a condition, also notify on the potential conditions:
	 * - cond_redecoder_frame_queue_too_large
	 * - cond_encoder_packet_queue_too_small (ONLY IF USING encoder_output_packet_queue)
	 * 
	 */
	std::atomic<bool> redecoder_still_running;

	/**
	 * @brief Keeps track of whether the encoder is still running, and if it's set to false manually, the encoder
	 * will exit.
	 * 
	 * @details This will naturally become false under normal execution once the encoder is finished, which requires
	 * the decoder to be finished.
	 * 
	 * @note Because the encoder might be waiting on a condition, also notify on the potential conditions:
	 * - cond_source_frame_queue_too_small
	 * - cond_encoder_packet_queue_too_large (ONLY IF USING encoder_output_packet_queue)
	 * 
	 */
	std::atomic<bool> encoder_still_running;

	/**
	 * @brief Keeps track of whether the decoder is still running, and if it's set to false manually, the decoder
	 * will exit.
	 * 
	 * @details This will naturally become false under normal execution once the decoder is finished.
	 * 
	 * @note Because the decoder might be waiting on a condition, also notify on the potential conditions:
	 * - cond_source_frame_queue_too_large
	 * 
	 */
	std::atomic<bool> decoder_still_running;

	/**
	 * @brief This is used to track the return of the redecoder bits, and if something failed this should halt redecoding.
	 * 
	 * @note AVERROR(EAGAIN) and AVERROR_EOF are excused; the buffers may need filling or whatever so EAGAIN just
	 * triggers a re-run. I don't know if AVERROR_EOF is actually applicable here.
	 * 
	 */
	int redecoder_ret;

	/**
	 * @brief This is used to track the return of the encoder bits, and if something failed this should halt redecoding.
	 * 
	 * @note AVERROR(EAGAIN) and AVERROR_EOF are excused; the buffers may need filling or whatever so EAGAIN just
	 * triggers a re-run. I don't know if AVERROR_EOF is actually applicable here.
	 * 
	 */
	int encoder_ret;

	/**
	 * @brief This is used to track the return of the decoder bits, and if something failed this should halt decoding.
	 * 
	 * @note AVERROR(EAGAIN) and AVERROR_EOF are excused; the buffers may need filling or whatever so EAGAIN just
	 * triggers a re-run. I don't know if AVERROR_EOF is actually applicable here.
	 * 
	 */
	int decoder_ret;

	/**
	 * @brief Context used for sws_scale, currently only used in the renderer to create/format the output frames properly.
	 * 
	 */
	struct SwsContext *sws_yuv_from_redecoder;

	/**
	 * @brief Identifier used to point to the correct video stream as a file can
	 * have multiple audio and video streams.
	 * 
	 */
	int video_source_stream;

	/**
	 * @brief Used for debugging, this stores the calculated value of what the FPS should be.
	 * 
	 */
	double frame_rate;

	/**
	 * @brief Partially used for debuggin, but actually necessary for the renderer wait threshold so that the next frame
	 * is only rendered after, more or less, the needed duration has passed.
	 * 
	 * @note The current implementation of keeping frames on screen for the required duration is hacky and isn't properly
	 * in sync.
	 * 
	 */
	double frame_duration;

	/**
	 * @brief Frame buffer size of RGB24 images according to the video input width/height.
	 * 
	 * @note This is what I intended to use for the sciter rendering site output, but I'm currently outputting IYUV
	 * images, so unless that changes, this is useless. But I'm still keeping it here.
	 * 
	 */
	int rgb24_frame_buffer_size;

	/**
	 * @brief Frame buffer size, which should be arbitrary but actually isn't, based on the input file type.
	 * 
	 * @note This isn't "arbitrary" because the input files are controlled by me, and the decoders/encoders used
	 * in this whole implementation assume YUV. This could technically be arbitrary, and it *is* based on the input file,
	 * but if you check init_encode_input_file's implementation, the pix_fmt has to match whatever the encoder accepts,
	 * so if the encoder is RGB and the input is YUV, or vice versa, I believe an additional conversion step would be
	 * necessary.
	 * 
	 */
	int input_frame_buffer_size;

	std::atomic<bool> should_emulate_frame_skip;

	/**
	 * @brief This is how many frames, in terms of frame duration, we can be behind before a frame skip
	 * gets emulated.
	 * 
	 */
	int frame_skip_frame_count_tolerance;

	/**
	 * @brief This is used in combination with frame_skip_frame_count_tolerance. This basically is used as a multiplier
	 * for when the frame skip should be ignored. This means that if a frame is behind more than frame_duration * 
	 * frame_skip_frame_count_tolerance * frame_skip_ignore_multiplier, then the frame skip does not occur so as to give
	 * the encoder/redecoder a chance to actually output video with really slow codecs.
	 * 
	 */
	int frame_skip_ignore_multiplier;

	/**
	 * @brief This stores the present time of the frame in the renderer, which is based off of its timestamp.
	 * 
	 */
	int64_t time_rendered_frame;

	/**
	 * @brief This stores the millisecond offset for redecoded frames. This gets applied in the redecoding process,
	 * and it allows the renderer to check the timestamp of new frames after a frame skip.
	 * 
	 */
	int64_t pts_frame_skip_offset;
};