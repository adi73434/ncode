
// This is an open source non-commercial project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "HandleEvents.h"

// This globals.h files gives the global variable that is needed to call TiScript functions from C++
#include "Globals.h"

#include <Windows.h>
#include <shellapi.h>
#include <iostream>
#include <sstream>
#include <string>
#include <filesystem>

#include "VideoDecoder.h"
#include "VideoStreamer.h"
#include "Conversions.h"

#define CPPHTTPLIB_OPENSSL_SUPPORT

#include <ffmpegcpp.h>
#include <httplib.h>

// using namespace std;
using namespace ffmpegcpp;

#define _WINDEBUGOUT(s)                        \
	{                                          \
		std::wostringstream os_;               \
		os_ << s;                              \
		OutputDebugStringW(os_.str().c_str()); \
	}

std::mutex mtx_encoder_queue;

sciter::value HandleEvents::set_global_video_file_concat_values(
	sciter::value video_file_dir,
	sciter::value video_file_encoded_affix,
	sciter::value video_file_extension)
{
	Globals::video_file_dir = Conversions::sciter_value_to_std_string(video_file_dir);
	Globals::video_file_encoded_affix = Conversions::sciter_value_to_std_string(video_file_encoded_affix);
	Globals::video_file_extension = Conversions::sciter_value_to_std_string(video_file_extension);
	return sciter::value();
}

sciter::value HandleEvents::set_global_api_location(sciter::value ui_api_address, sciter::value ui_api_port)
{
	Globals::api_address = Conversions::sciter_value_to_std_string(ui_api_address);
	Globals::api_port = Conversions::sciter_value_to_int(ui_api_port);

	return sciter::value();
}

sciter::value HandleEvents::set_3d_video_and_stream_array_details(sciter::value videos_settings,
																  sciter::value streams_settings,
																  sciter::value video_array_2d)
{
	assert(video_array_2d.is_array() || video_array_2d.is_object_array());

	Globals::videos_content_comment_index = Conversions::sciter_value_to_int(videos_settings[0]);
	Globals::videos_file_count = Conversions::sciter_value_to_int(videos_settings[1]);
	Globals::videos_codec_varaints = Conversions::sciter_value_to_int(videos_settings[2]);
	Globals::videos_content_count = Conversions::sciter_value_to_int(videos_settings[3]);
	Globals::streams_content_comment_index = Conversions::sciter_value_to_int(streams_settings[0]);
	Globals::streams_file_count = Conversions::sciter_value_to_int(streams_settings[1]);
	Globals::streams_codec_varaints = Conversions::sciter_value_to_int(streams_settings[2]);
	Globals::streams_content_count = Conversions::sciter_value_to_int(streams_settings[3]);

	// Using a global? Just for this?
	// Me to future me: Cry me a river; I wanted this as a global so I can re-use it in the cleanup
	// NOTE: This needs the details above first to be set
	Globals::vector_2d_original_video_details = convert_2d_ui_array_to_2d_vector(video_array_2d);

	return sciter::value();
}

// -----------------------------------------------------------------------------
// SECTION Decoding and Playback
// -----------------------------------------------------------------------------
void HandleEvents::play_video_thread(std::string file_dest, sciter::value ui_callback)
{
	// std::unique_lock<std::mutex> mtx_lock(Globals::keep_decoder_alive_mtx);
	Globals::keep_decoder_alive = true;
	// mtx_lock.unlock();

	// Specify the max number of frames in the frame queue
	int max_frames = 4;

	// Keep looping if the global_video_destination hasn't been set
	// I don't know why this happens, but when HandleEvents.play_video(); is ran
	// on the attached() event in Reactor, this runs before the global_video_destination
	// actually receives the Sciter rendering site from VideoRenderingSite
	int vidDestUnsetLoops = 0;
	while (Globals::global_video_destination == NULL)
	{
		Sleep(100);
		vidDestUnsetLoops++;
		// If, for some reason, this has looped over enough times, give a callback to the UI
		// instead of continuing to loop.
		if (vidDestUnsetLoops == 10)
		{
			ui_callback.call("For some reason the video player could not load, please try starting a video manually.");
			return;
		}
	}

	// Create a VideoDecoder
	VideoDecoder cdr(file_dest, Globals::global_video_destination, max_frames);

	// Spawn a thread that starts the video decoder, passing it the context of the newly
	// spanwed VideoDecoder object. The other implementation was to do cdr.start(), where
	// the start method spawns a new thread, but I was having some issues and this is what
	// I ended up with.
	std::thread thread_video_decoder(&VideoDecoder::start, &cdr);

	// decoder_is_running is used to block exiting the app until it is finished
	Globals::decoder_is_running = true;

	// NOTE: keep_decoder_alive is set to false either upon detaching of the section
	// with the video player, or upon a user-triggered app exit event.
	// This loop also handles events from the UI such as pausing the video
	while (Globals::keep_decoder_alive)
	{
		// Toggle pause state if event set to toggle
		if (try_toggle_pause_video)
		{
			cdr.playback_paused_by_user = !cdr.playback_paused_by_user;
			try_toggle_pause_video = false;
		}
		Sleep(50);
	}

	// This triggers once keep_decoder_alive is set to false. See the comment in the header file
	// of VideoDecoder for what that does.
	cdr.early_stop_all();
	thread_video_decoder.join();
	// Allow app to close
	Globals::decoder_is_running = false;
	// I think this destructor is useless but whatever
	cdr.~VideoDecoder();
}

void HandleEvents::play_video_stream_thread(std::string file_dest,
											std::string encoder_name,
											std::string desired_quality,
											sciter::value ui_callback)
{
	// std::unique_lock<std::mutex> mtx_lock(Globals::keep_decoder_alive_mtx);
	Globals::keep_decoder_alive = true;
	// mtx_lock.unlock();

	// Specify the max number of primary-decode frames
	// This is intentionally high so that the encode always has
	// a frame to work on
	// This is also used for the redecoder frame queue, but that
	// doesn't matter there
	int max_decoded_frames = 32;
	// This'll likely never be reached, but intentionally
	// limit the number of max encoded packets so that
	// a user can't pause the video and have the encoder rush ahead
	int max_encoded_packets = 2;

	// Keep looping if the global_video_destination hasn't been set
	// I don't know why this happens, but when HandleEvents.play_video(); is ran
	// on the attached() event in Reactor, this runs before the global_video_destination
	// actually receives the Sciter rendering site from VideoRenderingSite
	int vidDestUnsetLoops = 0;
	while (Globals::global_video_destination == NULL)
	{
		Sleep(100);
		vidDestUnsetLoops++;
		// If, for some reason, this has looped over enough times, give a callback to the UI
		// instead of continuing to loop.
		if (vidDestUnsetLoops == 10)
		{
			ui_callback.call("For some reason the video player could not load, please try starting a video manually.");
			return;
		}
	}

	// Create a VideoStreamer
	VideoStreamer strmr(
		Globals::global_video_destination,
		file_dest,
		encoder_name,
		desired_quality,
		max_decoded_frames,
		max_encoded_packets,
		ui_callback);

	// Spawn a thread that starts the video decoder, passing it the context of the newly
	// spanwed VideoDecoder object. The other implementation was to do cdr.start(), where
	// the start method spawns a new thread, but I was having some issues and this is what
	// I ended up with.
	std::thread thread_video_decoder(&VideoStreamer::start, &strmr);

	// decoder_is_running is used to block exiting the app until it is finished
	Globals::decoder_is_running = true;

	// NOTE: keep_decoder_alive is set to false either upon detaching of the section
	// with the video player, or upon a user-triggered app exit event.
	// This loop also handles events from the UI such as pausing the video
	while (Globals::keep_decoder_alive)
	{
		// Toggle pause state if event set to toggle
		if (try_toggle_pause_video)
		{
			strmr.playback_paused_by_user = !strmr.playback_paused_by_user;
			try_toggle_pause_video = false;
		}
		// This makes the video decoder/encoder stop if the settings changed
		if (Globals::live_encoder_quality_setting_changed)
		{
			// HandleEvents::stop_video();
			Globals::keep_decoder_alive = false;
		}
		Sleep(50);
	}

	// This triggers once keep_decoder_alive is set to false. See the comment in the header file
	// of VideoDecoder for what that does.
	strmr.early_stop_all();
	thread_video_decoder.join();
	// Allow app to close
	Globals::decoder_is_running = false;
	// I think this destructor is useless but whatever
	// strmr.~VideoStreamer();

	// This finally handles re-creating the stream if it should be
	if (Globals::live_encoder_quality_setting_changed)
	{
		Globals::live_encoder_quality_setting_changed = false,
		HandleEvents::play_video_stream_thread(file_dest, encoder_name, desired_quality, ui_callback);
	}
}

sciter::value HandleEvents::play_video(sciter::value ui_play_type,
									   sciter::value ui_file_dest,
									   sciter::value ui_encoder_name,
									   sciter::value ui_desired_quality,
									   sciter::value ui_callback)
{
	// -------------------------------------------------------------------------
	// TODO: CONVERT FILE NAME FROM toEncode/toEncodee to whatever the result should be based on the encoder
	// -------------------------------------------------------------------------
	// If a video is already playing, close that video before re-opening a new one
	// This will wait until the video is stopped
	if (Globals::decoder_is_running)
	{
		HandleEvents::stop_video();
	}

	std::string play_type = Conversions::sciter_value_to_std_string(ui_play_type);
	std::string encoder_name = Conversions::sciter_value_to_std_string(ui_encoder_name);
	std::string file_dest = Conversions::sciter_value_to_std_string(ui_file_dest);

	if (play_type == "video_on_demand")
	{
		std::string full_file_dest;
		if (encoder_name == "ORIGINAL")
		{
			// Concatenate the file directory without the encoder name as we're targeting the original
			full_file_dest = Globals::video_file_dir + file_dest + Globals::video_file_extension;
		}
		else
		{
			// Concatenate the file directory with the file encoder name etc.
			full_file_dest = Globals::video_file_dir +
							 file_dest +
							 Globals::video_file_encoded_affix +
							 encoder_name +
							 Globals::video_file_extension;
		}

		// Run the method that actually spawns the video player on a new thread so that the GUI doesn't block
		std::thread thread_video_player(&HandleEvents::play_video_thread, this, full_file_dest, ui_callback);
		thread_video_player.detach();
	}
	else if (play_type == "live_encoding")
	{
		// Concatenate the file directory without the encoder name as we'll be encoding live
		std::string full_file_dest = Globals::video_file_dir + file_dest + Globals::video_file_extension;

		// If the video is an original, just decode it
		if (encoder_name == "ORIGINAL")
		{
			std::thread thread_video_player(&HandleEvents::play_video_thread, this, full_file_dest, ui_callback);
			thread_video_player.detach();
		}
		else
		{
			std::string desired_quality = Conversions::sciter_value_to_std_string(ui_desired_quality);
			// Start the encoder-decoder combo, and also pass the encoder name since we're not using it in the file path
			std::thread thread_video_player(&HandleEvents::play_video_stream_thread,
											this,
											full_file_dest,
											encoder_name,
											desired_quality,
											ui_callback);

			thread_video_player.detach();
		}
	}

	return sciter::value();
}

sciter::value HandleEvents::stop_video()
{
	// Tell video player to stop. This essentially makes play_video_thread trigger the stop,
	// join the thread, and set decoder_is_running to false
	Globals::keep_decoder_alive = false;

	// Wait until decoder is finished
	while (Globals::decoder_is_running)
	{
		Sleep(50);
	}

	return sciter::value();
}

sciter::value HandleEvents::toggle_pause_video()
{
	try_toggle_pause_video = true;
	return sciter::value();
}
// -----------------------------------------------------------------------------
// !SECTION
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// SECTION Encoding
// -----------------------------------------------------------------------------

void HandleEvents::encode_file(std::string file_to_encode, std::string encoder_to_use)
{
	// original videos are not getting encoded so if that's the "encoder" specified, skip
	if (encoder_to_use == "ORIGINAL")
	{
		return;
	}

	// This example will take a raw audio file and encode it into as MP3.
	try
	{
		std::string encoded_file_output = Globals::video_file_dir +
										  file_to_encode +
										  Globals::video_file_encoded_affix +
										  encoder_to_use +
										  Globals::video_file_extension;

		// Create a muxer that will output the video as MKV.
		Muxer *muxer = new Muxer(encoded_file_output.c_str());

		VideoCodec *codec;

		if (encoder_to_use == "libx264")
		{
			codec = new VideoCodec("libx264");
			codec->SetQualityScale(20);
			codec->SetOption("preset", "veryfast");
		}
		else if (encoder_to_use == "libx265")
		{
			codec = new VideoCodec("libx265");
			codec->SetQualityScale(20);
			codec->SetOption("preset", "veryfast");
		}
		else if (encoder_to_use == "libvpx-vp9")
		{
			codec = new VideoCodec("libvpx-vp9");
			codec->SetOption("crf", 60);
			codec->SetGenericOption("b", "0");
			// CPU utilisation target: 100*(16-cpu-used)/16)%
			codec->SetOption("cpu-used", 8);
			codec->SetOption("deadline", "realtime");
			codec->SetOption("row-mt", 1);
			// The CRF value can be from 0–63. Lower values mean better quality. Recommended values range from 15–35, with 31 being recommended for 1080p HD video.
		}
		else if (encoder_to_use == "libaom-av1")
		{
			return;
			// codec = new VideoCodec("AV1");
			// Quality must be 0 for Constant Quality
			// codec->SetGenericOption("b", "0");
			//  The CRF value can be from 0–63. Lower values mean better quality and greater file size.
			// codec->SetOption("crf", 30);
		}

		// Create an encoder that will encode the raw audio data as MP3.
		// Tie it to the muxer so it will be written to the file.
		VideoEncoder *encoder = new VideoEncoder(codec, muxer);

		// Load the raw video file so we can process it.
		// FFmpeg is very good at deducing the file format, even from raw video files,
		// but if we have something weird, we can specify the properties of the format
		// in the constructor as commented out below.
		std::string encoded_file_input = Globals::video_file_dir + file_to_encode + Globals::video_file_extension;
		RawVideoFileSource *video_file = new RawVideoFileSource(encoded_file_input.c_str(), encoder);

		// Prepare the output pipeline. This will push a small amount of frames to the file sink until it IsPrimed returns true.
		video_file->PreparePipeline();
		int i = 0;

		// Push all the remaining frames through.
		while (!video_file->IsDone())
		{
			//
			i++;
			video_file->Step();
		}
		_WINDEBUGOUT(i);
		_WINDEBUGOUT(i);
		_WINDEBUGOUT(i);
		_WINDEBUGOUT(i);
		// Save everything to disk by closing the muxer.
		muxer->Close();

		// Delete everything so we don't leak memory because this uses the new keyword
		delete muxer;
		delete codec;
		delete encoder;
		delete video_file;
		_WINDEBUGOUT(encoded_file_output.c_str());
	}
	catch (FFmpegException e)
	{
		std::cerr << "Exception caught!" << std::endl;
		std::cerr << e.what() << std::endl;
		throw e;
	}

	std::cout << "Encoding complete!" << std::endl;
	std::cout << "Press any key to continue..." << std::endl;

	getchar();

	return;
}

sciter::value HandleEvents::cpp_enqueue_encoder(sciter::value ui_video_group, sciter::value ui_video_in_group_id, sciter::value ui_file_to_encode, sciter::value ui_encoder_to_use)
{
	std::vector<std::string> add_vec;
	add_vec.push_back(Conversions::sciter_value_to_std_string(ui_video_group));
	add_vec.push_back(Conversions::sciter_value_to_std_string(ui_video_in_group_id));
	add_vec.push_back(Conversions::sciter_value_to_std_string(ui_file_to_encode));
	add_vec.push_back(Conversions::sciter_value_to_std_string(ui_encoder_to_use));
	std::unique_lock<std::mutex> mtx_lock(mtx_encoder_queue);
	encoder_queue.push_back(add_vec);
	mtx_lock.unlock();
	return sciter::value();
}

void HandleEvents::watch_encoder_queue_thread()
{
	// Since this is synchronous, this loop will have to wait for encode_file to finish
	// so only one at a time will run. That's intentional.
	while (keep_watching_queue_thread)
	{
		// Slow this loop down because it has no reason to go faster
		Sleep(100);

		// Lock mutex
		std::unique_lock<std::mutex> mtx_lock(mtx_encoder_queue);
		// Only bother if there's things in the queue
		if (encoder_queue.size() > 0)
		{
			int video_group = std::stoi(encoder_queue.front()[0]);
			int video_in_group_id = std::stoi(encoder_queue.front()[1]);
			std::string file_to_encode = encoder_queue.front()[2];
			std::string encoder_to_use = encoder_queue.front()[3];
			Conversions::pop_front(encoder_queue);
			// Unlock the mutex so it doesn't stay locked for a stupid amount of time while encoding
			// Then trigger the encoder
			mtx_lock.unlock();
			encode_file(file_to_encode, encoder_to_use);

			// Tell the UI which file has been encoded
			encode_file_callback.call(video_group, video_in_group_id);
		}
	}
}

sciter::value HandleEvents::cpp_trigger_watch_encoder_queue(sciter::value ui_callback)
{
	// Assign the callback function to this object so that watch_encoder_queue_thread
	// can trigger a callback to the UI to inform it which files are done.
	encode_file_callback = ui_callback;
	keep_watching_queue_thread = true;
	// Start and detach the thread so the UI won't block
	std::thread queue_watcher(&HandleEvents::watch_encoder_queue_thread, this);
	queue_watcher.detach();
	return sciter::value();
}
// -----------------------------------------------------------------------------
// !SECTION
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// SECTION Server requests
// -----------------------------------------------------------------------------

sciter::value HandleEvents::cpp_check_user_code(sciter::value ui_user_code)
{
	std::string unique_code = Conversions::sciter_value_to_std_string(ui_user_code);

	httplib::Client cli(Globals::api_address.c_str());
	httplib::Params post_params{
		{"unique_code", unique_code}};
	auto res = cli.Post("/ncode/php/check_code.php", post_params);
	_WINDEBUGOUT(res->body.c_str());
	return sciter::value(res->body);
}

std::vector<std::vector<std::string>> HandleEvents::convert_2d_ui_array_to_2d_vector(
	sciter::value array_2d)
{
	int first_layer = Globals::videos_file_count * Globals::videos_codec_varaints;
	int second_layer = Globals::videos_content_count;

	std::vector<std::vector<std::string>> vector_2d(first_layer);
	assert(array_2d.is_array() || array_2d.is_object_array());
	for (int i = 0; i < first_layer; i++)
	{
		sciter::value first_dimension = array_2d[i];
		assert(first_dimension.is_array() || first_dimension.is_object_array());
		for (int j = 0; j < second_layer; j++)
		{
			vector_2d[i].push_back(Conversions::sciter_value_to_std_string(first_dimension[j]));
		}
	}
	return vector_2d;
}

Json::Value HandleEvents::convert_3d_ui_array_to_json(
	sciter::value file_codec_array,
	int file_count,
	int codecs_per_file,
	int comment_start_index,
	int content_count,
	bool has_quality_rating)
{
	Json::Value json_video_data;

	assert(file_codec_array.is_array() || file_codec_array.is_object_array());
	// -------------------------------------------------------------------------
	// For every file group
	// -------------------------------------------------------------------------
	for (int i = 0; i < file_count; i++)
	{
		sciter::value codecs_in_one_file = file_codec_array[i];
		assert(codecs_in_one_file.is_array() || codecs_in_one_file.is_object_array());
		// ---------------------------------------------------------------------
		// For every codec in a file
		// ---------------------------------------------------------------------
		for (int j = 0; j < codecs_per_file; j++)
		{
			sciter::value ratings_and_comment_for_codec = codecs_in_one_file[j];
			assert(ratings_and_comment_for_codec.is_array() || ratings_and_comment_for_codec.is_object_array());


			// Here we grab the comment
			std::string comment = Conversions::sciter_value_to_std_string(ratings_and_comment_for_codec[comment_start_index]);
			json_video_data["file" + std::to_string(i)]["codec" + std::to_string(j)]["comment"] = comment;
			if (has_quality_rating)
			{
				// Here we grab the quality rating
				std::string qual_r = Conversions::sciter_value_to_std_string(ratings_and_comment_for_codec[comment_start_index-1]);
				json_video_data["file" + std::to_string(i)]["codec" + std::to_string(j)]["quality_rating"] = qual_r;
			}

			// -----------------------------------------------------------------
			// For every rating in a file (which are 1 *after* the comment index)
			// -----------------------------------------------------------------
			for (int k = comment_start_index + 1; k <= content_count; k++)
			{
				std::string rating = Conversions::sciter_value_to_std_string(ratings_and_comment_for_codec[k]);
				json_video_data["file" + std::to_string(i)]["codec" + std::to_string(j)]["ratings"].append(rating);
			}
		}
	}
	return json_video_data;
}

std::string HandleEvents::grab_comments_from_vector(std::vector<std::string> video_file_details_single, int start_pos, int end_pos)
{
	std::string concat_ratings = "";

	for (int i = start_pos; i < end_pos; i++)
	{
		concat_ratings += video_file_details_single[i] + ",";
	}
	return concat_ratings;
}

sciter::value HandleEvents::submit_survey_thread(
	sciter::value ui_submit_array,
	sciter::value ui_callback,
	sciter::value ui_component_context)
{
	Json::Value json_post_data;

	// -------------------------------------------------------------------------
	// SECTION Input data from Sciter
	// -------------------------------------------------------------------------
	// These are all essentially strings so this works.
	json_post_data["unique_code"] = Conversions::sciter_value_to_std_string(ui_submit_array[0]);
	json_post_data["intro_location_type"] = Conversions::sciter_value_to_std_string(ui_submit_array[1]);
	json_post_data["intro_internet_satisfaction"] = Conversions::sciter_value_to_std_string(ui_submit_array[2]);
	json_post_data["intro_stream_upload_quality"] = Conversions::sciter_value_to_std_string(ui_submit_array[3]);
	json_post_data["intro_stream_download_quality_youtube"] = Conversions::sciter_value_to_std_string(ui_submit_array[4]);
	json_post_data["intro_stream_download_quality_twitch"] = Conversions::sciter_value_to_std_string(ui_submit_array[5]);
	json_post_data["intro_stream_download_quality_discord"] = Conversions::sciter_value_to_std_string(ui_submit_array[6]);
	json_post_data["intro_age_range"] = Conversions::sciter_value_to_std_string(ui_submit_array[7]);
	json_post_data["outro_employment"] = Conversions::sciter_value_to_std_string(ui_submit_array[8]);
	json_post_data["outro_industry"] = Conversions::sciter_value_to_std_string(ui_submit_array[9]);
	json_post_data["outro_tech_hobbies"] = Conversions::sciter_value_to_std_string(ui_submit_array[10]);
	json_post_data["outro_selected_rating1"] = Conversions::sciter_value_to_std_string(ui_submit_array[11]);
	json_post_data["outro_selected_rating2"] = Conversions::sciter_value_to_std_string(ui_submit_array[12]);
	json_post_data["outro_selected_rating3"] = Conversions::sciter_value_to_std_string(ui_submit_array[13]);
	json_post_data["outro_selected_rating4"] = Conversions::sciter_value_to_std_string(ui_submit_array[14]);

	// This corresponds to videoFilesAndDetails and streamFilesAndDetails in main.tis
	Json::Value json_video_ratings = convert_3d_ui_array_to_json(
		ui_submit_array[15],
		Globals::videos_file_count,
		Globals::videos_codec_varaints,
		Globals::videos_content_comment_index,
		Globals::videos_content_count,
		false);
	Json::Value json_stream_ratings = convert_3d_ui_array_to_json(
		ui_submit_array[16],
		Globals::streams_file_count,
		Globals::streams_codec_varaints,
		Globals::streams_content_comment_index,
		Globals::streams_content_count,
		true);

	json_post_data["videos_data"] = json_video_ratings;
	json_post_data["streams_data"] = json_stream_ratings;
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------

	// -------------------------------------------------------------------------
	// Get the bitrate the encoders were using
	// This will either be the default bitrate, or the user's upload speed
	// based on the read_speedtest_result_thread() method
	long long btrt = Globals::configured_encoder_bitrate;
	json_post_data["configured_encoder_bitrate"] = btrt;
	// -------------------------------------------------------------------------

	// -------------------------------------------------------------------------
	// SECTION Data from speedtest
	// -------------------------------------------------------------------------
	json_post_data["upload_speed"] = "untested";
	json_post_data["download_speed"] = "untested";

	// Read the file if it exists
	if (std::filesystem::exists("speedtest_result.json"))
	{
		std::ifstream json_file("speedtest_result.json");
		Json::Reader reader;
		Json::Value speed_res;
		reader.parse(json_file, speed_res);

		// Check the JSON file has upload and download properties, if it doesn't we just
		// don't bother reading them and upload defaults
		if (speed_res.isMember("download") && speed_res.isMember("upload"))
		{
			if (speed_res["download"].isMember("bandwidth") && speed_res["upload"].isMember("bandwidth"))
			{
				// NOTE: These are stored in Bytes
				json_post_data["download_speed"] = speed_res["download"]["bandwidth"].asString();
				json_post_data["upload_speed"] = speed_res["upload"]["bandwidth"].asString();
			}
		}
	}
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------

	httplib::Client cli(Globals::api_address.c_str());

	httplib::Params post_params{
		{"json_data", json_post_data.toStyledString()}};

	auto res = cli.Post("/ncode/php/submit.php", post_params);

	// -------------------------------------------------------------------------
	// TODO: Handle the responses
	// -------------------------------------------------------------------------
	_WINDEBUGOUT(res->body.c_str());

	// Trigger callback function in UI, passing it the "context" of the component
	// so that we can use the "this" context in TiScript,
	// and passing it the body of the response text
	ui_callback.call(ui_component_context, res->body);
	return sciter::value();
}

sciter::value HandleEvents::cpp_submit_survey(sciter::value ui_submit_array, sciter::value ui_callback, sciter::value ui_component_context)
{
	std::thread thread_submit(&HandleEvents::submit_survey_thread, this, ui_submit_array, ui_callback, ui_component_context);
	thread_submit.detach();
	return sciter::value();
}

void HandleEvents::read_speedtest_result_thread()
{
	bool file_exists = false;

	// Keep checking until a result is found, or until the user
	// starts a stream.
	// This way, if a user starts a stream before the upload speed was read,
	// we give up and just use the defaults for all VideoStreamers
	while (!Globals::speedtest_upload_speed_read && !Globals::user_started_streamer_before_speed_read)
	{
		Sleep(50);
		// Check if file exists, and try to read if it does
		if (FILE *file = fopen("speedtest_result.json", "r"))
		{
			fclose(file);
			// reader.parse didn't want to accept the FILE, but it does work with ifstream
			std::ifstream json_file("speedtest_result.json");
			Json::Reader reader;
			Json::Value speed_res;
			reader.parse(json_file, speed_res);

			// Check the result actually exists
			if (speed_res.isMember("download") && speed_res.isMember("upload"))
			{
				if (speed_res["upload"].isMember("bandwidth"))
				{
					long long read_byterate = speed_res["upload"]["bandwidth"].asInt();

					// If user's upload speed is higher than the cap, use the cap
					if (read_byterate * 8 > Globals::max_encoder_bitrate_cap)
					{
						long long btrt = Globals::max_encoder_bitrate_cap;
						Globals::configured_encoder_bitrate = btrt;
					}
					else
					{
						Globals::configured_encoder_bitrate = (read_byterate * 8) * 0.5;
					}
					// Success, so stop the loop
					Globals::speedtest_upload_speed_read = true;
				}
			}
		}
	}
}

sciter::value HandleEvents::cpp_trigger_ookla_speedtest()
{
	ShellExecute(nullptr, L"open", L"cmd.exe", L"/C speedtest.exe -f json > speedtest_result.json", nullptr, SW_SHOW);
	std::thread thr_reader(&HandleEvents::read_speedtest_result_thread, this);
	thr_reader.detach();
	return sciter::value();
}

sciter::value HandleEvents::make_exit_safe()
{
	HandleEvents::stop_video();

	// Return captured in TiScript
	return sciter::value(WSTR("good_to_exit"));
}

sciter::value HandleEvents::cpp_cleanup_files()
{
	for (int i = 0; i < Globals::vector_2d_original_video_details.size(); i++)
	{
		if (Globals::vector_2d_original_video_details[i][2] != "ORIGINAL")
		{
			// Concatenate the file directory with the file encoder name etc.
			std::string file_dest = Globals::video_file_dir +
									Globals::vector_2d_original_video_details[i][0] +
									Globals::video_file_encoded_affix +
									Globals::vector_2d_original_video_details[i][1] +
									Globals::video_file_extension;
			_WINDEBUGOUT("Deleting file: " << file_dest.c_str() << std::endl);
			std::filesystem::remove(file_dest);
		}
	}
	std::filesystem::remove("speedtest_result.json");
	return sciter::value();
}