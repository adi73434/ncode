<?php

require("_dbconnect.php");

if (!$_POST["unique_code"]) {
	echo "data_not_received";
	die();
}

// Select row where the unique code matches
$sql_code_match = $dbconn->prepare('SELECT code_id FROM code WHERE code_value = ?');
$sql_code_match->execute([$_POST["unique_code"]]);
$sql_code_match_result = $sql_code_match->fetch(PDO::FETCH_ASSOC);

if (!$sql_code_match_result) {
	echo "code_not_exist";
	die();
}

// Try to select row where the unique code id matches in the submissions
// This is trying to make sure the code hasn't been used before
$sql_code_used = $dbconn->prepare('SELECT * FROM submission WHERE code_id = ?');
$sql_code_used->execute([$sql_code_match_result["code_id"]]);

// If there is a non-zero amount of rows, that means someone already used that code
if ($sql_code_used->rowCount() > 0) {
	echo "code_already_used";
	die();
} else {
	echo "code_good";
	die();
}
