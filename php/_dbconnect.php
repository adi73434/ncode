<?php

// This file is used to connect to the database for easy modification

header("Access-Control-Allow-Origin: *");

$serverName = "localhost";
$username = "root";
$password = "";
$databaseName = "ncode";

try {
	$dbconn = new PDO("mysql:host=$serverName;dbname=$databaseName", $username, $password);

	// Set the PDO error mode to exception
	$dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
	die();
}
