<?php

require("_dbconnect.php");

$in_data = json_decode($_POST["json_data"]);


// Check none of the required fields are blank
if (!$in_data->unique_code) {
	echo "data_not_received";
	die();
}
if (!$in_data->download_speed) {
	echo "missing_download_speed";
	die();
}
if (!$in_data->upload_speed) {
	echo "missing_upload_speed";
	die();
}
if (!$in_data->videos_data) {
	echo "missing_videos_data";
	die();
}
if (!$in_data->streams_data) {
	echo "missing_streams_data";
	die();
}
if (!$in_data->intro_location_type) {
	echo "missing_intro_location_type";
	die();
}
if (!$in_data->intro_internet_satisfaction) {
	echo "missing_intro_internet_satisfaction";
	die();
}
if (!$in_data->intro_stream_upload_quality) {
	echo "missing_intro_stream_upload_quality";
	die();
}
if (!$in_data->intro_stream_download_quality_youtube) {
	echo "missing_intro_stream_download_quality_youtube";
	die();
}
if (!$in_data->intro_stream_download_quality_twitch) {
	echo "missing_intro_stream_download_quality_twitch";
	die();
}
if (!$in_data->intro_stream_download_quality_discord) {
	echo "missing_intro_stream_download_quality_discord";
	die();
}
if (!$in_data->intro_age_range) {
	echo "missing_intro_age_range";
	die();
}
if (!$in_data->outro_employment) {
	echo "missing_outro_employment";
	die();
}
if (!$in_data->outro_industry) {
	echo "missing_outro_industry";
	die();
}
// if (!$in_data->outro_tech_hobbies) {
// 	echo "missing_outro_tech_hobbies";
// 	die();
// }
if (!$in_data->outro_selected_rating1) {
	echo "missing_outro_selected_rating1";
	die();
}
if (!$in_data->outro_selected_rating2) {
	echo "missing_outro_selected_rating2";
	die();
}
if (!$in_data->outro_selected_rating3) {
	echo "missing_outro_selected_rating3";
	die();
}
if (!$in_data->outro_selected_rating4) {
	echo "missing_outro_selected_rating4";
	die();
}



// Select row where the unique code matches
$sql_code_match = $dbconn->prepare('SELECT code_id FROM code WHERE code_value = ?');
$sql_code_match->execute([$in_data->unique_code]);
$sql_code_match_result = $sql_code_match->fetch(PDO::FETCH_ASSOC);

if (!$sql_code_match_result) {
	echo "code_not_exist";
	die();
}



// Try to select row where the unique code id matches in the submissions
// This is trying to make sure the code hasn't been used before
$sql_code_used = $dbconn->prepare('SELECT submission_id FROM submission WHERE code_id = ?');
$sql_code_used->execute([$sql_code_match_result["code_id"]]);
$sql_code_used_result = $sql_code_used->fetch(PDO::FETCH_ASSOC);

// If there is a match, that means someone already use that code
if ($sql_code_used_result) {
	echo "code_already_used";
	die();
}



$sql_insert = $dbconn->prepare('INSERT INTO
	submission
	(
		submission_id,
		code_id,
		download_speed,
		upload_speed,
		videos_data,
		streams_data,
		intro_location_type,
		intro_internet_satisfaction,
		intro_stream_upload_quality,
		intro_stream_download_quality_youtube,
		intro_stream_download_quality_twitch,
		intro_stream_download_quality_discord,
		intro_age_range,
		outro_employment,
		outro_industry,
		outro_tech_hobbies,
		outro_selected_rating1,
		outro_selected_rating2,
		outro_selected_rating3,
		outro_selected_rating4
	)
	VALUES (
		NULL,
		:code_id,
		:download_speed,
		:upload_speed,
		:videos_data,
		:streams_data,
		:intro_location_type,
		:intro_internet_satisfaction,
		:intro_stream_upload_quality,
		:intro_stream_download_quality_youtube,
		:intro_stream_download_quality_twitch,
		:intro_stream_download_quality_discord,
		:intro_age_range,
		:outro_employment,
		:outro_industry,
		:outro_tech_hobbies,
		:outro_selected_rating1,
		:outro_selected_rating2,
		:outro_selected_rating3,
		:outro_selected_rating4
	)');

// Insert the unique_code_id primary key here as a foreign key alongside the rest of the data
$sql_insert->execute([
	":code_id" => $sql_code_match_result["code_id"],
	":download_speed" => $in_data->download_speed,
	":upload_speed" => $in_data->upload_speed,
	":videos_data" => json_encode($in_data->videos_data),
	":streams_data" => json_encode($in_data->streams_data),
	":intro_location_type" => $in_data->intro_location_type,
	":intro_internet_satisfaction" => $in_data->intro_internet_satisfaction,
	":intro_stream_upload_quality" => $in_data->intro_stream_upload_quality,
	":intro_stream_download_quality_youtube" => $in_data->intro_stream_download_quality_youtube,
	":intro_stream_download_quality_twitch" => $in_data->intro_stream_download_quality_twitch,
	":intro_stream_download_quality_discord" => $in_data->intro_stream_download_quality_discord,
	":intro_age_range" => $in_data->intro_age_range,
	":outro_employment" => $in_data->outro_employment,
	":outro_industry" => $in_data->outro_industry,
	":outro_tech_hobbies" => $in_data->outro_tech_hobbies,
	":outro_selected_rating1" => $in_data->outro_selected_rating1,
	":outro_selected_rating2" => $in_data->outro_selected_rating2,
	":outro_selected_rating3" => $in_data->outro_selected_rating3,
	":outro_selected_rating4" => $in_data->outro_selected_rating4
]);



// Check register was successful
if (!$sql_insert) {
	echo "issue";
} else {
	echo "good";
}
