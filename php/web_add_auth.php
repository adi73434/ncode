<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>temp add auth</title>
</head>
<body>


	<form id="authForm"  action="add_auth_code.php"">
		<input id="data-username" type="text" name="username" placeholder="username" />
		<input id="data-password" type="password" name="password" type="password" placeholder="password" />
		<input id="data-authcode" type="text" name="authcode" placeholder="authcode" />
		<input type="submit" value="Insert Auth Code" />
	</form>

	<script type="text/javascript">

		let formEl = document.getElementById("authForm");
		
		formEl.onsubmit = (evnt) => {
			
			evnt.preventDefault();

			let dataUsername = document.getElementById("data-username").value;
			let dataPassword = document.getElementById("data-password").value;
			let dataAuthcode = document.getElementById("data-authcode").value;

			fetch("add_auth_code.php", {
				method: "POST",
				// credentials: "omit",
				body: JSON.stringify({
					username: dataUsername,
					password: dataPassword,
					authcode: dataAuthcode,
				}),
			})
			.then(response => response.text())
			.then(data => {
				alert(data);
			});
		}
	</script>

</body>
</html>

