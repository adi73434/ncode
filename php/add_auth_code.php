<?php

require("_dbconnect.php");

$in_data = json_decode(file_get_contents('php://input'));

// Check data sent
if (!$in_data) {
	echo "data_not_received";
	die();
}
if (strlen($in_data->username) < 1 ||
	strlen($in_data->password) < 1 ||
	strlen($in_data->authcode) < 1
	) {
	echo "data_not_received";
	die();
}
// Try find admin by username
$sql_user_check = $dbconn->prepare('SELECT password FROM admin WHERE username = ?');
$sql_user_check->execute([$in_data->username]);
$sql_user_check_result = $sql_user_check->fetch(PDO::FETCH_ASSOC);

if (!$sql_user_check_result) {
	echo "login_no_match";
	die();
}

// Since we got the password that matches the username, see if the hashed pw matches
if (!password_verify($in_data->password, $sql_user_check_result["password"])) {
	echo "login_no_match";
	die();
}

// -----------------------------------------------------------------------------
// admin pw is verified by here
// -----------------------------------------------------------------------------

// Select row where the unique code matches
$sql_code_match = $dbconn->prepare('SELECT code_id FROM code WHERE code_value = ?');
$sql_code_match->execute([$in_data->authcode]);
$sql_code_match_result = $sql_code_match->fetch(PDO::FETCH_ASSOC);

if ($sql_code_match_result) {
	echo "code_already_exists";
	die();
}

// Insert code
$sql_insert_code = $dbconn->prepare('INSERT into code (code_id, code_value) VALUES (NULL, ?)');
$sql_insert_code->execute([$in_data->authcode]);

// Check add was successful
if (!$sql_insert_code) {
	echo "arbitrary_insert_issue";
} else {
	echo "code_inserted";
}
